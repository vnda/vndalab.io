---
layout: default
title: Listas
parent: Guia de documentação
nav_order: 5
---

# Lists
{: .no_toc }

## ## Conteúdos
{: .no_toc .text-delta }

1. TOC
{:toc}

---

Most lists can be rendered with pure Markdown.

## list

<div class="code-example" markdown="1">
- Item 1
- Item 2
- Item 3

_or_

1. Item 1
1. Item 2
1. Item 3
</div>
```markdown
- Item 1
- Item 2
- Item 3

_or_

1. Item 1
1. Item 2
1. Item 3
```

## Task list

<div class="code-example" markdown="1">
- [ ] hello, this is a todo item
- [ ] hello, this is another todo item
- [x] goodbye, this item is done
</div>
```markdown
- [ ] hello, this is a todo item
- [ ] hello, this is another todo item
- [x] goodbye, this item is done
```

