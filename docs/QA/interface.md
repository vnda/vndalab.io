---
layout: default
title: Interface
parent: QA
nav_order: 2
---

# Interface
{: .no_toc }

## Conteúdos
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## QA de Interface

* Etapa	Check
* Home	Home em HTML: <inserir link layout>
* Home	Menu principal dinâmico
* Home	CNPJ e assinatura VNDA no rodapé
* Home	Dinamizar
* Home	Adaptar mobile
* Home	Direcionar ícones redes sociais: <insirir links>
* Home	Registrar orientações de cadastro
* Transacional	Tela de produto: <inserir link layout>
* Transacional	Adicionar produto no carrinho
* Transacional	Tela de carrinho
* Transacional	Tela de Checkout
* Transacional	Registrar orientações de cadastro
* Transacional	Adaptar mobile
* Transacional	Preço promocional na página de produto
* Listas	Listagem de produtos: <inserir link layout>
* Listas	Paginação
* Listas	Busca
* Listas	Paginação na Busca
* Listas	Login / Minha conta
* Listas	Registrar orientações de cadastro
* Listas	Adaptar mobile
* Listas	Preço promocional na página de produto
* Listas	Tag flag
* Listas	Padronizar Aspect Ratio das imagens dos produtos
* Checkpoint	Priorizar visualização acima da dobra das páginas
* Checkpoint	Registrar relatórios de performance da home/tag/produto
* Checkpoint	Melhorar performance home
* Checkpoint	Melhorar performance tag
* Checkpoint	Melhorar performance produto
* Checkpoint	Lazy load imagens
* Páginas	Sobre a marca: <link layout>
* Páginas	Tela atendimento: <link layout>
* Páginas	Termos de uso: <link layout>
* Revisões	Checklist final
* Revisões	CNPJ no rodapé
* Revisões	Paginação na tela de busca e tag
* Revisões	Indicação de esgotado na vitrine e página do produto
* Revisões	Assinatura Vnda: sempre escrito dessa forma: Vnda - Tecnologia em Ecommerce, com site abrindo em outra aba.
* Revisões	de/por na página do produto e vitrine
* Revisões	[listagens] Tag flag
* Revisões	Número de parcelas dinâmico na página de produtos e em vitrine (verificar se aplica ao design em questão)
* Revisões	Dados do rodapé dinâmicos com o admin -> configurações
* Revisões	Ordenamento dos tamanhos na vitrine e no produto (quando aplicável)
* Revisões	Ordenamento dos filtros (quando aplicável)
* Revisões	Filtros de tag funcionais e em ordem crescente
* Revisões	Mobile tem que estar Excelente/Bom/Razoável -> https://testmysite.thinkwithgoogle.com/intl/pt-br
* Revisões	Favicon
* Revisões	Verificar CSS above the fold na home/tag/search/product/login
* Revisões	Avaliar performance -> http://www.webpagetest.org/
* Revisões	Avaliar performance -> Lighthouse (Aba audits do dev tools)
