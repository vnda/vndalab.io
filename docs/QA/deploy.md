---
layout: default
title: Deploy
parent: QA
nav_order: 3
---

# Deploy
{: .no_toc }

## Conteúdos
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Checklist

* Setup Google Analytics e Tag Manager
* Atualizar cadastro de shipping
* Atualizar cadastro de notifications
* Atualizar cadastro Antifraude
* Atualizar integração com ERP
* Cadastro do tracker
* Cadastro do automation
* Hooks da oms (marketplace)
* Google Webmaster Tools
* Verificar Favicon e Tag capa
* Verificar cadastro de menus, página e banners
* Verificar direcionamentos para loja em staging
* Verificar infos do rodapé
* Conferir se url está com https verdinho
* Testar formulários
* Conferir dados de pagamento
* Enviar dados de produção para a adquirente (Stone)
* Conferir se direct está ativo
* Colocar pedidos teste
* Criar user de staging em produção
* E-mail lancado@vnda.com.br
* Redirects 301
* Robots.txt ([link](https://trello.com/c/x4ifH9T3/7368-gerar-robotstxt-modelo))
* Cadastro no Letsmonitor
* Ativar Dashboard
