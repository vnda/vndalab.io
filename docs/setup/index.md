---
layout: default
title: Setup
nav_order: 2
has_children: true
---

# Setup
{: .no_toc }

No momento em que o cliente entra em contato com a Vnda, uma série de agendas são marcadas. Os primeiros contatos buscam entender definir alguns pontos sobre o cliente, como informações sobre a marca, valores, perfil do cliente. Subsequente a isso, o contratado de serviço com um escopo definido é realizado. A partir de então o cliente fornece todo material que ira compor a identidade visual utilizada na criação da loja, e a equipe da Vnda inicia o processo de criação do ecommerce, dividido em uma série de etapas de desenvolvimento até a sua concepção, chamamos o processo de  SETUP.

## Conteúdos
{: .no_toc .text-delta }

1. TOC
{:toc}

***

## Processo de Setup
As lojas são desenvolvidas no ambiente de desenvolvimento Vnda. 
Todos os projetos seguem o mesmo fluxo de setup e entrega.
As entregas são divididas em etapas, conforme fluxograma abaixo:

![Fluxo de setup](/assets/images/Fluxo_de_setup_2019_-_Integradores__1_.jpg)

| # | Etapa        | Objetivos                                                                | Arquivos associados                                                                                                                                                                                                                                                                   |
|---|--------------|--------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1 | Briefing     | Definições de escopo de projeto e alinhamento de layout.                 |                                                                                                                                                                                                                                                                                       |
| 2 | Home         | Aprovação da tela de home do ecommerce dinâmica e responsiva.            | [home.liquid](https://docs.vnda.com.br/docs/liquid4/templates/home/) [layout.liquid](https://docs.vnda.com.br/docs/liquid4/templates/layout/)                                                                                                                               |
| 3 | Transacional | Aprovação do fluxo transacional do ecommerce, exceto navegação.          | [product.liquid](https://docs.vnda.com.br/docs/liquid4/templates/product/) custom.css                                                                                                                                                                                                   |
| 4 | Listas       | Aprovação do fluxo de navegação do ecommerce em listagens e buscas.      | [tag.liquid](https://docs.vnda.com.br/docs/liquid4/templates/tag/) [search.liquid](https://docs.vnda.com.br/docs/liquid4/templates/search/) [login.liquid](https://docs.vnda.com.br/docs/liquid4/templates/login/) [account.liquid](https://docs.vnda.com.br/docs/liquid4/templates/account/) |
| 5 | Conceitual   | Aprovação das páginas conceituais do ecommerce.                          | [page.liquid](https://docs.vnda.com.br/docs/liquid4/templates/page/)                                                                                                                                                                                                                    |
| 6 | Revisões     | Aprovação das revisões finais acumuladas ao longo das etapas anteriores. |                                                                                                                                                                                                                                                                                       |

Detalhamento:
  
- **Etapa 1 - Briefing**: Definição do escopo, referências/direcionamentos, materiais de marca (logo aplicação principal em .CSV, paleta de cores, fontes).
- **Etapa 2 - Home**: desenvolvimento em duas entregas, sendo a primeira, **Home 1.0**, entrega da home em HTML. A segunda, **Home 2.0**, entrega da home dinâmica e com o responsivo.
- **Etapa 3 - Transacional**: desenvolvimento da página interna do produto, adicionar ao carrinho funcional, estilizar página default de carrinho e de checkout. Dinâmicas e responsivas. 
- **Etapa 4 - Listas**: desenvolvimento da página de listagem de produtos (tag), busca e Login. Estilizar página default de minha conta dinâmicas e responsivas.
- **Etapa 5 - Conceitual**: desenvolvimento das páginas institucionais e de atendimento dinâmicas e responsivas.
- **Revisões**: revisões gerais, testes de performance e usabilidade.

Cada etapa possui um checklist específico com o wireframe, as informações e necessidades de cada tela.

Conforme o projeto for avançando as etapas, as orientações, demandas e informações necessárias vão sendo complementadas.

### Navegadores suportados

As lojas devem suportar as seguintes versões de cada navegador:
  
| Navegador             | Versão suportada | Versão atual[^1] | Ciclo de atualização |
| --------------------- | ----------------------- | ------------- |
| **Internet Explorer** | Sem suporte             | 11            | Descontinuado |
| **Microsoft Edge**    | 16                      | 17            | 6 semanas[^2] |
| **Firefox**           | 44                      | 70            | 6 semanas |
| **Safari**            | 10.1                    | 13            | Anual[^3]     |
| **Opera**             | 40                      | 65            | 6 semanas |
| **Chrome**            | 49                      | 78            | 6 semanas |

## Documentação
A orientação técnica está reunida em [Setup](https://docs.vnda.com.br/docs/setup/setup/). 
O registro das informações do cliente e do projeto estão no painel do Trello que você recebeu acesso.
Todos os guias de uso do painel administrativo Vnda estão nesse link: <http://ajuda.vnda.com.br>.

## Materiais
Todos os materiais do cliente e wireframes ficam salvos da pasta do google drive do cliente e no card do projeto.

## Acessos
- Trello: você receberá por email o convite para acesso ao painel.
- Pasta de materiais e arquivos do projeto: você receberá o link no card do projeto.
- Painel administrativo da loja: você receberá um email com o assunto Senha do Admin Vnda com link, login e senha temporária.
- Gitlab: você receberá por email o acesso ao repositório da Vnda.
- Dropbox: você receberá por email o acesso às pastas da Vnda.

## Contato
- O contato deve acontecer pelo email ajuda@vnda.com.br.
- Enviar para esse email a indicação de quem são as pessoas que devemos interagir ao longo do projeto e da área/responsabilidade de cada um.
- Dúvidas ou considerações sobre o projeto podem ser registradas como comentário no card do projeto do Trello.

***

[^1]: Atualizado em 2019-11-25.
[^2]: A partir de janeiro de 2020, quando o navegador utilizará o Chromium.
[^3]: Atualizações menores de performance e segurança são trimestrais. Novos recursos são apresentados anualmente com as atualizações do macOS e iOS.
