---
layout: default
title: Organização dos Squads
parent: Setup
nav_order: 1
---

# Organização dos Squads

Para organizar o processo de setup, contamos com um modelo de ambiente de trabalho em setores divididos, todos operando com a utilização da metodologia ágil `SCRUM`, em ciclos semanais de entrega.
Em específico, o setor de Setup possui times especializados, que chamamos de Squads, compostos por Desenvolvedores Fron-End e Projet Owners.
O Front e o Back são bem separados, com equipes diferentes para cada especialização.

***

## Composição dos SQUADs

### Project Owner (PO)

Cada squad possui dois ou mais Project Owners, os quais são atribuidos tarefas de atender e trocar informações com o clientes, organizar as informações que serão repassadas aos desenvolvedores e gerênciar as pautas de projeto, como as pautas que serão tocadas pelos fronts.

#### Pautas gerenciadas pelos POs

Dentre as muitas que o Project Owner gerenciam e podem enderessar aos desenvolvedores Front-end, as mais comuns são:

- Criação de novas lojas, que são pautas de SETUP
- Ajustes em lojas já lançadas, que são as pautas de ATENDIMENTO e ONBOARDING

### Desenvolvedores Front-End

Na Vnda o front participa ativamente da criação do layout. Mesmo com a criação de um wireframe com a estrutura da loja através de um designer, o Front-end ainda deverá exercer um papel de criador, cuidando para que a identidade da marca, as referências e style guides  sejam seguidas na concepção do layout do ecommerce.

#### Pautas gerenciadas pelos POs

- Cada front gerencia o estado atual de sua própria pauta e recebe direto as tarefas dos projetos que ele é responsável
- Em dados momentos, um front pode possuir muitos projetos simultâneos, cabe ao front gerenciar o seu tempo e informar as datas para conclusão de cada tarefa de projeto.

#### Tecnologias que o Front deve sempre buscar se aperfeiçoar

- A parte nativa da web: html, css, javascript, jquery
- Sass como pré processador css
- Gulp (utilizado para automatizar tarefas)
- Liquid 2 e 4 (linguagem de template utilizada para integrar o front com o backend)
- Git (para versionamento do código, mas sem a utilização de branchs - já que não tem mais de uma pessoa trabalhando no mesmo projeto)

***

## Métodologia ágil SCRUM

Para atender e organizar as demandas de Setup, os Squads utilizam a metodologia ágil de desenvolvimento e planejamento chamada SCRUM.

### SCRUM

Scrum é um conjunto de boas práticas empregado no gerenciamento de projetos complexos, em que não se conhece todas as etapas ou necessidades.
Focado nos membros da equipe, o Scrum torna os processos mais simples e claros, pois mantém registros visíveis sobre o andamento de todas as etapas.
Assim, os participantes sabem em que fase o projeto está, o que já foi concluído e o que falta ser feito para a sua entrega.
Ela é aplicada a partir de ciclos rápidos, chamados sprints, nos quais há um tempo determinado para que as atividades sejam concluídas. Na Vnda, esse ciclo tem a duração de uma semana.

#### Ciclo semanal

O ciclo semanal dos squads de Setup é constituido pela Reunião Geral, pela Reunião de Setup e por reuniões internas(de squad).

#### Reunião de Setup

A Reunião de Setup ocorre no primeiro horário de Segunda-feita, onde é feito uma retrospectiva da última semana e um projeção breve da semana atual.

#### Reunião Geral

A Reunião Geral ocorre no primeiro horário do período da tarde, onde são apresentados todos os indicadores semanais da Vnda, bem como são falados sobre as novidades, novos clientes, novos colaboradores e anuncios em geral.

#### Reuniões internas

As reuniões internas são organizadas pelo Squad, e registras em agenda.
Cada time tem a liberdade de escolher quais os melhores horários para agendamento de reuniões internas, porem sempre teremos dois tipos de reuniões existentes em todos os Squads, a Daily e a Sprint Review.

#### Daily

Consiste em uma reunião diária rápida, que dura cerca de 20 minutos, onde cada integrante do Squad deve falar sobre seu trabalho de forma breve, buscando responder três perguntas:

- O que eu fiz desde a última Daily?
- O que eu pretendo fazer?
- Preciso de alguma ajuda?

#### Sprint Review

Sprint Review é realizado ao final de cada semana. Nesse rito o time fala sobre tudo que foi desenvolvido, registrando os resultados em planilha, para que posteriormente sejam transformados em indicadores para a reunião de Setup.

Neste rito é o momento ideal para os front's consultarem os POs sobre quais projetos são prioridade e, no Airtable, movidos do status **pautar** para **to do**, com os campos **Início** e **Due** preenchidos.
