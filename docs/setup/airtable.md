---
layout: default
title: Airtable
parent: Setup
nav_order: 2
---

# Airtable para o Front-end
{: .no_toc }

## Conteúdos
{: .no_toc .text-delta }

1. TOC
{:toc}

***

## O que é o Airtable

Airtable é o sistema onde a equipe de Setup organiza os projetos/pautas que estão trabalhando.

Ele é organizado em tabelas com linhas e colunas, onde as linhas são partes dos projetos e as colunas são dados sobre eles.

Nos próximos capítulos você confere mais informações sobre as linhas e colunas.

A equipe de PO's cria os cards e direciona para a pauta dos front's. Existem duas exceções a isso:
​

- Cards de code review: os próprios front's criam o card e colocam na pauta do colega
- Cards de deploy: o próprio front cria e executa a tarefa

***

## Partes do Airtable

***

### Abas

Na parte superior do Airtable você se depara com algumas abas. As principais para o seu trabalho são:

- **Atividades**: aqui ficam os cards da pauta dos fronts
- **Sprints**: organização semanal das pautas. Mais informações no capítulo de sprint.

***

### Views

​As views são modelos de tela que você pode escolher para visualizar suas pautas.​

Acesse a aba **atividades** e todas as views estão disponíveis no botão ao lado de **Pauta front**.

Existem views específicas para cada front, como por exemplo **Pauta front Paolla**.

Você pode criar novas views para sua pauta em formato de kanban ou calendário, por exemplo.

***

## Linhas e colunas

Cada linha do Airtable representa uma parte de um projeto. Você pode clicar nas flechas posicionadas antes do nome do projeto para ver todas as informações do card dentro do modal. Cada projeto tem mais de uma linha/card ao longo do seu desenvolvimento.

As colunas são as mesmas informações presentes dentro do modal, porém de forma mais resumida.

As principais colunas são:

***

### Coluna `Atividade`

Nome da atividade a ser desenvolvida naquele card.

<sub>exemplo: nomes de atividades</sub>

- **Home 1.0** e **Produto 1.0** são cards para criar a página home e a página produto, respectivamente
- Cards que começam com **onboarding** são executados depois que uma loja é lançada
- **Publicar cards** é um card que representa uma atividade de deploy

***

### Coluna `Projeto`

Nome da loja a qual aquele card se refere.

***

### Coluna `Time`

Nome do front que está executando aquele card.

***

### Coluna `Pts`

Quantidade de pontos do card. Cards de deploy, que o próprio front cria, devem ser preenchidos com o valor de **1** ponto.

Os demais cards já virão com esse campo preenchido.

***

### Coluna `Etapa atividade`

Se refere à etapa do projeto dentro da empresa como um todo (e não apenas em front).

Vai desde o fechamento do contrato com cliente até a loja ir ao ar e terminar o processo de onboarding.
​
Geralmente não é preenchida pelo front.

***

### Coluna `Área`

Sempre preencher com a opção **front**.

***

### Coluna `Checklist`

É a parte mais importante do card, esse campo diz quais itens especificamente devem ser realizados na loja.

Você possui libedade para ajustar itens da sua pauta e fazer comentários no meio dos checklists para a equipe de PO. Caso seu comentário seja algo urgente, é preferível fazer essa comunicação pelo Slack.

***

### Coluna `Inicio`

Data em que o front iniciou o card.

***

### Coluna `Due`

Data em que o front planeja finalizar o card.

Esse campo deve ser preenchido assim que o front iniciar a atividade, informando uma previsão de término. Essa informação pode ser atualizada conforme o front percebe que vai terminar o card antes ou depois do previsto.

***

### Coluna `Prioridade`

As prioridades são informações que obtemos dos PO's.

Serve para organizar quais cards deve ser tocados primeiro.

Você pode organizar as prioridades simplesmente digitando números nesse campo.

***

### Coluna `Status`

Essa coluna é muito importante para entender como está a pauta dos front's.

Veja abaixo os principais status e seus significados:

- **Aguardando**: o PO está preparando o card para enviar ao front (ainda não deve ser pego)
- **Pautar**: cards prontos para o front tocar, porém ele ainda não definiu data ou não está no seu sprint atual
- **To Do**: o front já definiu uma data para iniciar e terminar de fazer o card (geralmente esses cards são os únicos que recebem algum dado na coluna **prioridades**)
- **Doing**: card que o front está executando no momento, o ideal é manter o mínimo de cards aqui, de preferência 1
- **Review**: cards terminados pelo front e aguardando revisão do PO
- **Aguardando ok para subir**: cards que o PO já revisou e está aguardando revisão do cliente
- **Checked**: card revisado pelo PO (e em alguns casos pelo cliente também) e que o front já pode fazer deploy
- **Deployed**: cards deployados
- **Done**: cards finalizados e que o deploy já foi conferido em produção.

Cards de **code review** e **deploy** são movidos diretamente para **done**, e não para **review**.

***

## Filtros de cards

Logo abaixo das abas do Airtable, você encontra a opção **filters**, onde é possível escolher quais cards essa view irá exibir.

É aconselhável não mexer nessa configuração na view **Pauta front**.

Caso você tenha uma view apenas sua, você pode personalizar isso.

A recomendação é usar o seguinte filtro:

<sub>exemplo: filtro recomendado</sub>

```liquid

[Where] - [Status] - [is any of] - [preencha com todos os status necessários]
​
[And] - [Time] - [contains] - [preencha com seu nome]

```

Assim você verá todos os cards dos status selecionados e com o seu nome.

***

## Agrupamentos de cards

Todos os cards exibidos na tela, após a filtragem, podem ser agrupados para uma melhor visualização.

<sub>exemplo: agrupamento de cards</sub>

```liquid

[Group by] - [Status]

```

Assim você verá suas pautas agrupadas de acordo com o status, e os grupos podem ser minimizados.

***

## Ordenação de cards (sort)

Critérios para ordenar os cards na tela. Os mais usados são:

- Status
- Due
- Prioridade
