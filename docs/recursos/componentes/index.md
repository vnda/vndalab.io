---
layout: default
title: Componentes
nav_order: 1
parent: Recursos
has_children: true
---

# Componentes
{: .no_toc }

Confira nessa seção Componentes da plataforma que podem ser integrados nas lojas.
