---
layout: default
title: Popup de Newsletter
parent: Componentes
grand_parent: Recursos
nav_order: 2
---

# Componente Popup de Newsletter

Essa documentação foi migrada e reestruturada para nosso novo portal de documentações. Você pode acessá-la [aqui](https://developers.vnda.com.br/docs/pop-up-de-newsletter).