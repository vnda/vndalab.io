---
layout: default
title: Login e Registrar
parent: Componentes
grand_parent: Recursos
nav_order: 2
---


# Componente Login e Registrar

Essa documentação foi migrada e reestruturada para nosso novo portal de documentações. Você pode acessá-la [aqui](https://developers.vnda.com.br/docs/login-e-registrar).