---
layout: default
title: Instagram
parent: Componentes
grand_parent: Recursos
nav_order: 2
---

# Componente Instagram
{: .d-inline-block}

Liquid v4
{: .label .label-blue }

Um demo do componente pode ser visto [nesse link de teste](https://template1.vnda.dev/components/instagram-carousel.v4.html)

Para esse componente, no parametro `posts` deve ser colocado o retorno do `load_posts`: `loaded_posts`. Ver documentação dessa tag [aqui](https://docs.vnda.com.br/docs/liquid2/tags/loja/#load_posts-e-load_post)

## Assets

{% raw %}

```liquid
  <link rel="stylesheet" href="{{ 'instagram-carousel.v4.css' | component_path }}">
  <script src="{{ 'instagram-carousel.v4.js' | component_path }}"></script>
```

{% endraw %}

## Javascript

{% raw %}
```javascript
document.addEventListener("DOMContentLoaded", function(event) {
  var widgetSlide = new Vnda.Component.InstagramCarousel({
    mode: 'swiper',
    title: 'instagram @vndaecommerce',
    link: 'https://www.instagram.com/vndaecommerce',
    swiperProps: {
      spaceBetween: 10,
      slidesPerView: 1.3,
      breakpoints: {
        450: {
          slidesPerView: 2
        },
        767: {
          slidesPerView: 4
        },
        1200: {
          slidesPerView: 6
        }
      }
    },
    maxImages: 10,
    posts:          
    [
      {
        "title": "E finalmente chegou o dia que é motivo de alegria para os varejistas! 25 de dezembro é uma das datas comerciais mais importantes, mas não apenas pelo movimento das lojas (físicas ou virtuais) e pelas vendas aquecidas, mas por todo sentimento envolvido. \n\nVocê já parou para pensar no papel que sua marca exerce quando ela faz parte daquela troca de presentes cheia de carinho e cuidado entre amigos e familiares? \n\nNão é à toa que acreditamos e somos parceiros de marcas com propósito, pois estas têm o poder de conectar as pessoas. Esse é o verdadeiro espírito natalino!\n\nHoje não só desejamos um Feliz Natal para você e para sua família, mas te convidamos a refletir sobre a importância que seu negócio tem neste momento sensível e belo de cada família.\n\nUm Natal muito especial a todos! 🎄🎁✨",
        "image": "https://scontent-iad3-2.cdninstagram.com/v/t51.29350-15/269888953_2205023762971609_8355196618090062613_n.jpg?_nc_cat=109&ccb=1-5&_nc_sid=8ae9d6&_nc_ohc=6fSBeRa_zzYAX_MZ80q&_nc_ht=scontent-iad3-2.cdninstagram.com&edm=ANo9K5cEAAAA&oh=00_AT9mCBe2oZoyTlvyv401N2poVIxVyRQnURPzau50DR__rg&oe=61D21724",
        "timestamp": "2021-12-25T14:39:28+0000",
        "url": "/m/instagram/17946966664654621/e-finalmente-chegou-o-dia-que-e-motivo-de-alegria-para-os-varejistas-25-de-dezembro-e-uma-das-datas-comerciais-mais-importantes-mas-nao-apenas-pelo-movimento-das-lojas-fisicas-ou-virtuais-e-pelas-vendas-aquecidas-mas-por-todo-sentimento-envolvido-voce-ja-parou-para-pensar-no-papel-que-sua-marca-exerce-quando-ela-faz-parte-daquela-troca-de-presentes-cheia-de-carinho-e-cuidado-entre-amigos-e-familiares-nao-e-a-toa-que-acreditamos-e-somos-parceiros-de-marcas-com-proposito-pois-estas-tem-o-poder-de-conectar-as-pessoas-esse-e-o-verdadeiro-espirito-natalino-hoje-nao-so-desejamos-um-feliz-natal-para-voce-e-para-sua-familia-mas-te-convidamos-a-refletir-sobre-a-importancia-que-seu-negocio-tem-neste-momento-sensivel-e-belo-de-cada-familia-um-natal-muito-especial-a-todos",
        "permalink": "https://www.instagram.com/p/CX6NRd9ucNa/"
      },
      {
        "title": "Em novembro deste ano, a Vnda se juntou ao Olist com a missão de acelerar o crescimento dos negócios de varejo. \n\nAgora somos um só @teamolist fazendo acontecer diariamente e juntos, @olist.br, Vnda e @tinyerpoficial, ficamos muito felizes em anunciar uma nova conquista. \nHoje nos tornamos o novo #unicórnio brasileiro! \n\nQue venham os próximos desafios!🦄💙🧡",
        "image": "https://scontent-iad3-2.cdninstagram.com/v/t51.29350-15/267121765_2879291072375144_1155139968833950710_n.jpg?_nc_cat=103&ccb=1-5&_nc_sid=8ae9d6&_nc_ohc=l35Hhn9sJSYAX9MAZ5H&_nc_ht=scontent-iad3-2.cdninstagram.com&edm=ANo9K5cEAAAA&oh=00_AT8CBuzskQTNudrni08qsRE86jXHjodDrY6d-V8KuhhN6A&oe=61D15954",
        "timestamp": "2021-12-16T12:50:59+0000",
        "url": "/m/instagram/17902379174298332/em-novembro-deste-ano-a-vnda-se-juntou-ao-olist-com-a-missao-de-acelerar-o-crescimento-dos-negocios-de-varejo-agora-somos-um-so-teamolist-fazendo-acontecer-diariamente-e-juntos-olist-br-vnda-e-tinyerpoficial-ficamos-muito-felizes-em-anunciar-uma-nova-conquista-hoje-nos-tornamos-o-novo-unicornio-brasileiro-que-venham-os-proximos-desafios",
        "permalink": "https://www.instagram.com/p/CXi1tGNOZ_U/"
      },
      {
        "title": "O papo agora é sobre 2022! Você sabe o que está por vir no cenário do varejo? 🔜\n \nLá se foram dois anos com a maior aceleração que o comércio digital já enfrentou na história, e com esse movimento de mercado, vieram também novidades e adaptações relevantes no que diz respeito ao comportamento dos consumidores dos mais diversos perfis.\n \nO online está cada vez mais forte, ao mesmo tempo que o fluxo do varejo físico está voltando à \"normalidade\". Com esse novo cenário, os lojistas devem estar preparados para atender novos hábitos que irão surgir, além de outros já conhecidos que devem se estabelecer de forma definitiva. Para você varejista, 2022 já é agora. Seu negócio está preparado? 📳\n \nArrasta para o lado e confere as principais tendências de consumo que podemos esperar para 2022! 👉🏼\n\nQuer saber mais? Você pode conferir o artigo completo em nosso blog. Link na bio!",
        "image": "https://scontent-iad3-2.cdninstagram.com/v/t51.29350-15/265960335_976857692900357_3310242202628036528_n.jpg?_nc_cat=111&ccb=1-5&_nc_sid=8ae9d6&_nc_ohc=CXg_gVx0uCAAX9Xlwz8&_nc_ht=scontent-iad3-2.cdninstagram.com&edm=ANo9K5cEAAAA&oh=00_AT88n4RWZP8m710-pyLwpmDM0_g1ZMJuLfu1SPcNMOwXAw&oe=61D13CF2",
        "timestamp": "2021-12-10T19:36:34+0000",
        "url": "/m/instagram/17904965066350162/o-papo-agora-e-sobre-2022-voce-sabe-o-que-esta-por-vir-no-cenario-do-varejo-la-se-foram-dois-anos-com-a-maior-aceleracao-que-o-comercio-digital-ja-enfrentou-na-historia-e-com-esse-movimento-de-mercado-vieram-tambem-novidades-e-adaptacoes-relevantes-no-que-diz-respeito-ao-comportamento-dos-consumidores-dos-mais-diversos-perfis-o-online-esta-cada-vez-mais-forte-ao-mesmo-tempo-que-o-fluxo-do-varejo-fisico-esta-voltando-a-normalidade-com-esse-novo-cenario-os-lojistas-devem-estar-preparados-para-atender-novos-habitos-que-irao-surgir-alem-de-outros-ja-conhecidos-que-devem-se-estabelecer-de-forma-definitiva-para-voce-varejista-2022-ja-e-agora-seu-negocio-esta-preparado-arrasta-para-o-lado-e-confere-as-principais-tendencias-de-consumo-que-podemos-esperar-para-2022-quer-saber-mais-voce-pode-conferir-o-artigo-completo-em-nosso-blog-link-na-bio",
        "permalink": "https://www.instagram.com/p/CXUHWYTOcvv/"
      }"permalink": "https://www.instagram.com/p/CT21vtYley9/"
    ]  
  });
  widgetSlide.render(document.getElementById('component-instagram-carousel-slider-root'));

  var widgetGrid = new Vnda.Component.InstagramCarousel({
    mode: 'grid',
    title: 'instagram @vndaecommerce',
    link: 'https://www.instagram.com/vndaecommerce',
    gridProps: {
      xs: 6,
      sm: 4,
      md: 4,
      lg: 2,
      spaceBetween: 10,
    },
    maxImages: 6,
    posts:
    [
      {
        "title": "E finalmente chegou o dia que é motivo de alegria para os varejistas! 25 de dezembro é uma das datas comerciais mais importantes, mas não apenas pelo movimento das lojas (físicas ou virtuais) e pelas vendas aquecidas, mas por todo sentimento envolvido. \n\nVocê já parou para pensar no papel que sua marca exerce quando ela faz parte daquela troca de presentes cheia de carinho e cuidado entre amigos e familiares? \n\nNão é à toa que acreditamos e somos parceiros de marcas com propósito, pois estas têm o poder de conectar as pessoas. Esse é o verdadeiro espírito natalino!\n\nHoje não só desejamos um Feliz Natal para você e para sua família, mas te convidamos a refletir sobre a importância que seu negócio tem neste momento sensível e belo de cada família.\n\nUm Natal muito especial a todos! 🎄🎁✨",
        "image": "https://scontent-iad3-2.cdninstagram.com/v/t51.29350-15/269888953_2205023762971609_8355196618090062613_n.jpg?_nc_cat=109&ccb=1-5&_nc_sid=8ae9d6&_nc_ohc=6fSBeRa_zzYAX_MZ80q&_nc_ht=scontent-iad3-2.cdninstagram.com&edm=ANo9K5cEAAAA&oh=00_AT9mCBe2oZoyTlvyv401N2poVIxVyRQnURPzau50DR__rg&oe=61D21724",
        "timestamp": "2021-12-25T14:39:28+0000",
        "url": "/m/instagram/17946966664654621/e-finalmente-chegou-o-dia-que-e-motivo-de-alegria-para-os-varejistas-25-de-dezembro-e-uma-das-datas-comerciais-mais-importantes-mas-nao-apenas-pelo-movimento-das-lojas-fisicas-ou-virtuais-e-pelas-vendas-aquecidas-mas-por-todo-sentimento-envolvido-voce-ja-parou-para-pensar-no-papel-que-sua-marca-exerce-quando-ela-faz-parte-daquela-troca-de-presentes-cheia-de-carinho-e-cuidado-entre-amigos-e-familiares-nao-e-a-toa-que-acreditamos-e-somos-parceiros-de-marcas-com-proposito-pois-estas-tem-o-poder-de-conectar-as-pessoas-esse-e-o-verdadeiro-espirito-natalino-hoje-nao-so-desejamos-um-feliz-natal-para-voce-e-para-sua-familia-mas-te-convidamos-a-refletir-sobre-a-importancia-que-seu-negocio-tem-neste-momento-sensivel-e-belo-de-cada-familia-um-natal-muito-especial-a-todos",
        "permalink": "https://www.instagram.com/p/CX6NRd9ucNa/"
      },
      {
        "title": "Em novembro deste ano, a Vnda se juntou ao Olist com a missão de acelerar o crescimento dos negócios de varejo. \n\nAgora somos um só @teamolist fazendo acontecer diariamente e juntos, @olist.br, Vnda e @tinyerpoficial, ficamos muito felizes em anunciar uma nova conquista. \nHoje nos tornamos o novo #unicórnio brasileiro! \n\nQue venham os próximos desafios!🦄💙🧡",
        "image": "https://scontent-iad3-2.cdninstagram.com/v/t51.29350-15/267121765_2879291072375144_1155139968833950710_n.jpg?_nc_cat=103&ccb=1-5&_nc_sid=8ae9d6&_nc_ohc=l35Hhn9sJSYAX9MAZ5H&_nc_ht=scontent-iad3-2.cdninstagram.com&edm=ANo9K5cEAAAA&oh=00_AT8CBuzskQTNudrni08qsRE86jXHjodDrY6d-V8KuhhN6A&oe=61D15954",
        "timestamp": "2021-12-16T12:50:59+0000",
        "url": "/m/instagram/17902379174298332/em-novembro-deste-ano-a-vnda-se-juntou-ao-olist-com-a-missao-de-acelerar-o-crescimento-dos-negocios-de-varejo-agora-somos-um-so-teamolist-fazendo-acontecer-diariamente-e-juntos-olist-br-vnda-e-tinyerpoficial-ficamos-muito-felizes-em-anunciar-uma-nova-conquista-hoje-nos-tornamos-o-novo-unicornio-brasileiro-que-venham-os-proximos-desafios",
        "permalink": "https://www.instagram.com/p/CXi1tGNOZ_U/"
      },
      {
        "title": "O papo agora é sobre 2022! Você sabe o que está por vir no cenário do varejo? 🔜\n \nLá se foram dois anos com a maior aceleração que o comércio digital já enfrentou na história, e com esse movimento de mercado, vieram também novidades e adaptações relevantes no que diz respeito ao comportamento dos consumidores dos mais diversos perfis.\n \nO online está cada vez mais forte, ao mesmo tempo que o fluxo do varejo físico está voltando à \"normalidade\". Com esse novo cenário, os lojistas devem estar preparados para atender novos hábitos que irão surgir, além de outros já conhecidos que devem se estabelecer de forma definitiva. Para você varejista, 2022 já é agora. Seu negócio está preparado? 📳\n \nArrasta para o lado e confere as principais tendências de consumo que podemos esperar para 2022! 👉🏼\n\nQuer saber mais? Você pode conferir o artigo completo em nosso blog. Link na bio!",
        "image": "https://scontent-iad3-2.cdninstagram.com/v/t51.29350-15/265960335_976857692900357_3310242202628036528_n.jpg?_nc_cat=111&ccb=1-5&_nc_sid=8ae9d6&_nc_ohc=CXg_gVx0uCAAX9Xlwz8&_nc_ht=scontent-iad3-2.cdninstagram.com&edm=ANo9K5cEAAAA&oh=00_AT88n4RWZP8m710-pyLwpmDM0_g1ZMJuLfu1SPcNMOwXAw&oe=61D13CF2",
        "timestamp": "2021-12-10T19:36:34+0000",
        "url": "/m/instagram/17904965066350162/o-papo-agora-e-sobre-2022-voce-sabe-o-que-esta-por-vir-no-cenario-do-varejo-la-se-foram-dois-anos-com-a-maior-aceleracao-que-o-comercio-digital-ja-enfrentou-na-historia-e-com-esse-movimento-de-mercado-vieram-tambem-novidades-e-adaptacoes-relevantes-no-que-diz-respeito-ao-comportamento-dos-consumidores-dos-mais-diversos-perfis-o-online-esta-cada-vez-mais-forte-ao-mesmo-tempo-que-o-fluxo-do-varejo-fisico-esta-voltando-a-normalidade-com-esse-novo-cenario-os-lojistas-devem-estar-preparados-para-atender-novos-habitos-que-irao-surgir-alem-de-outros-ja-conhecidos-que-devem-se-estabelecer-de-forma-definitiva-para-voce-varejista-2022-ja-e-agora-seu-negocio-esta-preparado-arrasta-para-o-lado-e-confere-as-principais-tendencias-de-consumo-que-podemos-esperar-para-2022-quer-saber-mais-voce-pode-conferir-o-artigo-completo-em-nosso-blog-link-na-bio",
        "permalink": "https://www.instagram.com/p/CXUHWYTOcvv/"
      },
      {
        "title": "Essa foi uma semana super especial para o time da Vnda e queremos compartilhar aqui alguns dos porquês:\n\n• Depois de dois anos, voltamos a participar presencialmente de eventos do ecossistema de e-commerce 🙌🏼\n\n• Essa foi a 1ª vez (de muitas que estão por vir) que representantes das soluções Vnda, @olist.br e @tinyerpoficial marcaram presença em um evento de mercado juntos, pois agora somos um só time 🚀\n\nEssa foi a edição 2021 do Congresso E-commerce Brasil RS, que aconteceu no formato híbrido, nos dias 08 e 09 de dezembro, com a estrutura física em Porto Alegre. \n\nForam dois dias de muito conteúdo relevante sobre varejo e tecnologia, muito aprendizado e, principalmente um networking super importante e necessário entre alguns dos principais players do mundo do e-commerce.\n\nObrigada @ecommerce_br e demais envolvidos. Que venham os próximos! 🧡💙",
        "image": "https://scontent-iad3-2.cdninstagram.com/v/t51.29350-15/265070493_1856921621183034_2607968130131049886_n.jpg?_nc_cat=104&ccb=1-5&_nc_sid=8ae9d6&_nc_ohc=SiVPvtaZgy4AX8mxI9U&_nc_ht=scontent-iad3-2.cdninstagram.com&edm=ANo9K5cEAAAA&oh=00_AT8YJdNceM5Jkh5icmNoGR6cQA6CRhUCV5-3EtcmpmKWrw&oe=61D1CE14",
        "timestamp": "2021-12-09T20:32:46+0000",
        "url": "/m/instagram/18243014020073823/essa-foi-uma-semana-super-especial-para-o-time-da-vnda-e-queremos-compartilhar-aqui-alguns-dos-porques-depois-de-dois-anos-voltamos-a-participar-presencialmente-de-eventos-do-ecossistema-de-e-commerce-essa-foi-a-1-vez-de-muitas-que-estao-por-vir-que-representantes-das-solucoes-vnda-olist-br-e-tinyerpoficial-marcaram-presenca-em-um-evento-de-mercado-juntos-pois-agora-somos-um-so-time-essa-foi-a-edicao-2021-do-congresso-e-commerce-brasil-rs-que-aconteceu-no-formato-hibrido-nos-dias-08-e-09-de-dezembro-com-a-estrutura-fisica-em-porto-alegre-foram-dois-dias-de-muito-conteudo-relevante-sobre-varejo-e-tecnologia-muito-aprendizado-e-principalmente-um-networking-super-importante-e-necessario-entre-alguns-dos-principais-players-do-mundo-do-e-commerce-obrigada-ecommerce_br-e-demais-envolvidos-que-venham-os-proximos",
        "permalink": "https://www.instagram.com/p/CXRo_FRu79k/"
      }
    ]          
  });
  widgetGrid.render(document.getElementById('component-instagram-carousel-grid-root'));
});
```
{% endraw %}

{% include tag_parameters.html params=site.data.instagram_parameters %}
<!-- {% include component_methods.html methods=site.data.instagram_methods %} -->

### Exemplo de configurações do swiperProps

Pode ser usado todas as configurações do Swiper, ver documentação [aqui](https://swiperjs.com/)

{% raw %}
```javascript
swiperProps: {
  spaceBetween: 10, // espaçamento entre as imagens
  slidesPerView: 1.3, // quantidade de imagens para o mobile
  breakpoints: {
    450: {
      slidesPerView: 2 // quantidade de imagens acima de 450px
    },
    767: {
      slidesPerView: 4 // quantidade de imagens acima de 767px
    },
    1200: {
      slidesPerView: 6 // quantidade de imagens acima de 1200px
    }
  }
},
```
{% endraw %}

### Exemplo de configurações do gridProps

O valor informado em xs, sm, md e lg deve ser o número de colunas (em um grid de 12 colunas) que cada imagem deve ocupar

{% raw %}
```javascript
gridProps: {
  xs: 6,
  sm: 4,
  md: 4,
  lg: 2,
  spaceBetween: 10, // espaçamento entre as imagens
},
```
{% endraw %}
