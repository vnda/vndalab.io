---
layout: default
title: Minha conta
parent: Componentes
grand_parent: Recursos
nav_order: 2
---

# Componente de minha conta

Essa documentação foi migrada e reestruturada para nosso novo portal de documentações. Você pode acessá-la [aqui](https://developers.vnda.com.br/docs/minha-conta).
