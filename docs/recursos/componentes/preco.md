---
layout: default
title: Preço de produto
parent: Componentes
grand_parent: Recursos
nav_order: 2
---

# Componente Preço do Produto

Essa documentação foi migrada e reestruturada para nosso novo portal de documentações. Você pode acessá-la [aqui](https://developers.vnda.com.br/docs/preco-de-produto).