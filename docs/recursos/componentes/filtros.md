---
layout: default
title: Filtros de produto
parent: Componentes
grand_parent: Recursos
nav_order: 2
---

# Componente Filtro de Produtos

Essa documentação foi migrada e reestruturada para nosso novo portal de documentações. Você pode acessá-la [aqui](https://developers.vnda.com.br/docs/filtros-de-produto).