---
layout: default
title: Cart Drawer
parent: Componentes
grand_parent: Recursos
nav_order: 2
---

# Componente cart drawer

Essa documentação foi migrada e reestruturada para nosso novo portal de documentações. Você pode acessá-la [aqui](https://developers.vnda.com.br/docs/cart-drawer).