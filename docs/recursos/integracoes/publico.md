---
layout: default
title: Público
parent: Integrações
grand_parent: Recursos
---

# Público
{: .no_toc }

Envia uma audiência para integrações com a plataforma que utilizam e-mail.

## Request

| Method   | URL        | Content type       | Response |
|:--------:|:----------:|:------------------:|:--------:|
| **POST** | `/publico` | `application/json` | **200**  |


### Body

```json
{
  "first_name": "Nick", // string ou null
  "last_name": "Sobrenome", // string ou null
  "email": "nick@exemplo.com", // obrigatório
  "phone_area": "51", // string ou null
  "phone": "999999999", // string ou null
  "tags": ["tag1", "tag2"] // array de strings ou null
}
```
