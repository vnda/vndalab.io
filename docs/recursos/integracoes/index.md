---
layout: default
title: Integrações
nav_order: 2
parent: Recursos
has_children: true
---

# Integrações
{: .no_toc }

Confira nessa seção recursos da plataforma que podem ser integrados nas lojas.
