---
layout: page
title: Active Campaign
nav_order: 7
parent: Integrações
grand_parent: Recursos
---

# Active Campaign
{: .no_toc}

O Active Campaign é uma ferramenta de acompanhamento de eventos e campanhas para criar relatórios de marketing. Para integrá-lo com a Plataforma Vnda, é preciso criar os eventos necessários no Active Campaign e, então, adicioná-los na loja através do Google Tag Manager.

Esse guia indicará o passo-a-passo para realizar essa integração.

***

1. TOC
  {:toc}

***

## Criação do proxy

Para começar a integração do Active Campaign, é necessário solicitar a criação de um proxy CORS para o time de back-end.

| Caminho do proxy        | URL de destino         |
| ----------------------- | ---------------------- |
| `/activecampaign/event` | `//trackcmp.net/event` |


O proxy permitirá que o front envie os eventos para o Active Campaign através do Tag Manager. Note que o caminho do proxy deve ser usado para fazer as requisições `POST` nos passos seguintes.

***

## Configurando o Active Campaign

No painel do cliente no Active Campaign, vá em **Configurações » Rastreamento** e ative a opção **Rastreamento do Site**.

![Captura de tela da opção “Rastreamento do Site” no painel do Active Campaign]({% link assets/images/active-campaign/site-tracking.png %})

Na mesma página, adicione o domínio da loja em produção na lista de permissões.

![Captura de tela da lista de permissões de domínios no painel do Active Campaign]({% link assets/images/active-campaign/domain-management.png %})

Na mesma página, ative a opção **Monitoramento de Eventos** e adicione os seguintes eventos:

* Add to Cart
* Checkout Start
* Ecommerce Order
* Pageview
* Product View

Copie a **Chave do evento** exibida acima da lista de eventos que você adicionou. Ela será usada durante a integração no Tag Manager.

Clique em **API de monitoramento de evento**, como exibido na captura abaixo:

![Captura de tela das opções de monitoramento de eventos no painel do Active Campaign]({% link assets/images/active-campaign/event-tracking.png %}).

Uma caixa de diálogo será exibida. Copie o valor do campo `actid`. Esse é o ID da conta do Active Campaign, e será usado durante a integração do Tag Manager.

***

## Variáveis do Tag Manager

No Tag Manager da loja, crie as seguintes variáveis:

| Name                                                         | Type     | Value                                            |
| ------------------------------------------------------------ | -------- | ------------------------------------------------ |
| `Active Campaign - Event Key`                                | Constant | Chave do evento copiada no passo anterior        |
| `Active Campaign - ID`                                       | Constant | Valor do campo `actid` copiado no passo anterior |
| `Active Campaign - Event Tracking - Email - Checkout Start`  | Custom JavaScript | ```function getClientEmail() { return $('#client_email').val(); }``` |
| `Active Campaign - Event Tracking - Email - Ecommerce Order` | Data Layer Variable | `email`                               | 

***

## Triggers do Tag Manager

No Tag Manager da loja, crie (ou altere) os seguintes triggers:

| Name                | Type                 | This trigger fires on         |
| ------------------- | -------------------- | ----------------------------- |
| `Add to Cart Click` | Click - All Elements | `Click Classes contains add-to-cart-button` (note que a classe do botão pode mudar dependendo da loja) |
| `Checkout`          | Page View            | `Page URL contains /checkout` |
| `Produto`           | Page View            | `Page URL contains /produto/` | 

***

## Tags do Tag Manager

No Tag Manager da loja, crie uma tag para cada evento criado nas configurações do Active Campaign:

### Site Tracking

| Name                              | Type        | Trigger   |
| --------------------------------- | ----------- | --------- |
| `Active Campaign - Site Tracking` | Custom HTML | All Pages |

{% raw %}
```html
<script type="text/javascript">
    (function(e,t,o,n,p,r,i){e.visitorGlobalObjectAlias=n;e[e.visitorGlobalObjectAlias]=e[e.visitorGlobalObjectAlias]||function(){(e[e.visitorGlobalObjectAlias].q=e[e.visitorGlobalObjectAlias].q||[]).push(arguments)};e[e.visitorGlobalObjectAlias].l=(new Date).getTime();r=t.createElement("script");r.src=o;r.async=true;i=t.getElementsByTagName("script")[0];i.parentNode.insertBefore(r,i)})(window,document,"https://diffuser-cdn.app-us1.com/diffuser/diffuser.js","vgo");
    vgo('setAccount', '{{Active Campaign - ID}}');
    vgo('setTrackByDefault', true);
    vgo('process');
</script>
```
{% endraw %}

### Evento: Pageview

| Name                                          | Type        | Trigger   |
| --------------------------------------------- | ----------- | --------- |
| `Active Campaign - Event Tracking - Pageview` | Custom HTML | All Pages |

{% raw %}
```html
<script type="text/javascript">
  function acPageviewEvent() {
    // inicia a requisição
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        console.log("Recording Pageview Event");
        // console.debug(this.responseText);
      } else {
        // console.debug(this.responseText)
      }
    };
    // ID do usuário no ActiveCampaign (em /app/settings/tracking)
    var actid = "{{Active Campaign - ID}}";
    // Chave do evento (em /app/settings/tracking)
    var eventKey = "{{Active Campaign - Event Key}}";
    // Nome do evento pra rastrear
    var event = "Pageview"
    var visit = {
      email: "{{Active Campaign - Event Tracking - Email - Ecommerce Order}}" // o endereço de email do usuário
    }
    
    visit = new URLSearchParams(visit).toString()
    // URL da página para registrar no evento
    var eventData = window.location.href;
    // Construindo a string pra enviar para o AC.
    var eventString = "actid=" + actid
                      + "&key=" + eventKey
                      + "&event=" + event
                      + "&visit=" + visit
                      + "&eventdata=" + eventData;
    // Enviando a informação para o ActiveCampaign usando a API deles:
    xhttp.open("POST", "/activecampaign/event", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(eventString);
  }
  
  acPageviewEvent();
</script>
```
{% endraw %}

### Evento: Product View

| Name                                              | Type        | Trigger   |
| ------------------------------------------------- | ----------- | --------- |
| `Active Campaign - Event Tracking - Product View` | Custom HTML | Produto   |

{% raw %}
```html
<script type="text/javascript">
  function acProductViewEvent() {
    // inicia a requisição
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        console.log("Recording Product View Event");
        // console.debug(this.responseText);
      } else {
        // console.debug(this.responseText)
      }
    };
    // ID do usuário no ActiveCampaign (em /app/settings/tracking)
    var actid = "{{Active Campaign - ID}}";
    // Chave do evento (em /app/settings/tracking)
    var eventKey = "{{Active Campaign - Event Key}}";
    // Nome do evento pra rastrear
    var event = "Product View"
    var visit = {
      email: "{{Active Campaign - Event Tracking - Email - Ecommerce Order}}" // o endereço de email do usuário
    }
    
    visit = new URLSearchParams(visit).toString()
    // URL da página para registrar no evento
    var eventData = window.location.href;
    // Construindo a string pra enviar para o AC.
    var eventString = "actid=" + actid
                      + "&key=" + eventKey
                      + "&event=" + event
                      + "&visit=" + visit
                      + "&eventdata=" + eventData;
    // Enviando a informação para o ActiveCampaign usando a API deles:
    xhttp.open("POST", "/activecampaign/event", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(eventString);
  }
  
  acProductViewEvent();
</script>
```
{% endraw %}

### Evento: Add To Cart

| Name                                              | Type        | Trigger             |
| ------------------------------------------------- | ----------- | ------------------- |
| `Active Campaign - Event Tracking - Add To Cart`  | Custom HTML | Add to Cart Click   |

{% raw %}
```html
<script type="text/javascript">
  function acAddToCartEvent() {
    // inicia a requisição
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        console.log("Recording Add to Cart Event");
        // console.debug(this.responseText);
      } else {
        // console.debug(this.responseText)
      }
    };
    // ID do usuário no ActiveCampaign (em /app/settings/tracking)
    var actid = "{{Active Campaign - ID}}";
    // Chave do evento (em /app/settings/tracking)
    var eventKey = "{{Active Campaign - Event Key}}";
    // Nome do evento pra rastrear
    var event = "Add To Cart"
    var visit = {
      email: "{{Active Campaign - Event Tracking - Email - Ecommerce Order}}" // o endereço de email do usuário
    }
    
    visit = new URLSearchParams(visit).toString()
    // URL da página para registrar no evento
    var eventData = window.location.href;
    // Construindo a string pra enviar para o AC.
    var eventString = "actid=" + actid
                      + "&key=" + eventKey
                      + "&event=" + event
                      + "&visit=" + visit
                      + "&eventdata=" + eventData;
    // Enviando a informação para o ActiveCampaign usando a API deles:
    xhttp.open("POST", "/activecampaign/event", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(eventString);
  }
  
  acAddToCartEvent();
</script>
```
{% endraw %}

### Evento: Checkout Start

| Name                                                | Type        | Trigger    |
| --------------------------------------------------- | ----------- | ---------- |
| `Active Campaign - Event Tracking - Checkout Start` | Custom HTML | Checkout   |

{% raw %}
```html
<script type="text/javascript">
  function acCheckoutStart() {
    // inicia a requisição
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        console.log("Recording Checkout Start Event");
        console.debug(this.responseText);
      } else {
        console.debug(this.responseText)
      }
    };
    // ID do usuário no ActiveCampaign (em /app/settings/tracking)
    var actid = "{{Active Campaign - ID}}";
    // Chave do evento (em /app/settings/tracking)
    var eventKey = "{{Active Campaign - Event Key}}";
    // Nome do evento pra rastrear
    var event = "Checkout Start"
    var visit = {
      email: "{{Active Campaign - Event Tracking - Email - Checkout Start}}" // o endereço de email do usuário
    }
    visit = new URLSearchParams(visit).toString()
    // URL da página para registrar no evento
    var eventData = window.location.href;
    // Construindo a string pra enviar para o AC.
    var eventString = "actid=" + actid
                      + "&key=" + eventKey
                      + "&event=" + event
                      + "&visit=" + visit
                      + "&eventdata=" + eventData;
    // Enviando a informação para o ActiveCampaign usando a API deles:
    xhttp.open("POST", "/activecampaign/event", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(eventString);
  }
  
  acCheckoutStart();
</script>
```
{% endraw %}

### Evento: Ecommerce Order

| Name                                                 | Type        | Trigger     |
| ---------------------------------------------------- | ----------- | ----------- |
| `Active Campaign - Event Tracking - Ecommerce Order` | Custom HTML | Concluído   |

{% raw %}
```html
<script type="text/javascript">
  function acEcommerceOrder() {
    // inicia a requisição
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        console.log("Recording Ecommerce Order Event");
        // console.debug(this.responseText);
      } else {
        // console.debug(this.responseText)
      }
    };
    // ID do usuário no ActiveCampaign (em /app/settings/tracking)
    var actid = "{{Active Campaign - ID}}";
    // Chave do evento (em /app/settings/tracking)
    var eventKey = "{{Active Campaign - Event Key}}";
    // Nome do evento pra rastrear
    var event = "Ecommerce Order"
    var visit = {
      email: "{{Active Campaign - Event Tracking - Email - Ecommerce Order}}" // o endereço de email do usuário
    }
    
    visit = new URLSearchParams(visit).toString()
    // URL da página para registrar no evento
    var eventData = window.location.href;
    // Construindo a string pra enviar para o AC.
    var eventString = "actid=" + actid
                      + "&key=" + eventKey
                      + "&event=" + event
                      + "&visit=" + visit
                      + "&eventdata=" + eventData;
    // Enviando a informação para o ActiveCampaign usando a API deles:
    xhttp.open("POST", "/activecampaign/event", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(eventString);
  }
  
  acEcommerceOrder();
</script>
```
{% endraw %}
