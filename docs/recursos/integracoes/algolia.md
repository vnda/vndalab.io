---
layout: default
title: Algolia
parent: Integrações
grand_parent: Recursos
---

# Algolia
{: .no_toc }

Algolia é uma ferramenta que disponibiliza uma API para o desenvolvimento de buscas de produtos ou posts. O Algolia Search conta com diversas possibilidades de configuração diretamente no painel da conta.

O Algolia consiste em duas partes: implementação de pesquisa e análise de pesquisa. As ferramentas de implementação tornam mais fácil para seus desenvolvedores criar e manter ótimas experiências de pesquisa para seus usuários. As ferramentas analíticas permitem que suas equipes de negócios analisem o impacto dessas experiências e refinem-nas, para que possam abordar diretamente seus objetivos de negócios em evolução. [Confira mais detalhes na documentação oficial](https://www.algolia.com/doc/guides/getting-started/what-is-algolia/).


## Neste artigo:
{: .no_toc .text-delta}

1. TOC
{:toc}

***

## Widgets

InstantSearch.js é uma biblioteca JavaScript vanilla que permite criar uma experiência de resultado de pesquisa instantânea usando a API de pesquisa do Algolia. Ela possui diversos widgets prontos para utilização. [Confira mais detalhes na documentação oficial](https://github.com/algolia/instantsearch.js).

## Como indexar os produtos / posts

1. É necessário criar uma conta no Algolia, a ferramenta permite, atualmente, até 10k pesquisas de forma grátis por mês.
2. Não é necessário criar os *Index* previamente.
3. Os produtos sempre serão indexados no *Index* `products` e os posts com o nome da coleção do Content, que geralmente é `posts`.
4. Após criado o usuário será possivel verificar algumas informações importantes para a implementação como ***APLICATION_KEY*** e ***USAGE_API_KEY***. Essas informações podem ser verificadas no icone de settings na parte inferior esquerda.

## Ativando a integração no Admin

Em *Admin > Configurações > Integrações > Algolia* adicione o ID e a KEY que foram obtidas no painel do Algolia.

> Obs.: Será necessário atualizar a planilha de produtos e os posts para que eles sejam indexados.

## Implementação na loja

### Inicialmente instale os pacotes necessários para utilizar o Algolia

```bash
npm install instantsearch.js algoliasearch -D
```

### Crie um novo arquivo JavaScript para importar os módulos e desenvolver a funcionalidade

```javascript
import algoliasearch from 'algoliasearch/lite';
import instantsearch from 'instantsearch.js';
import { searchBox, hits, configure, pagination } from 'instantsearch.js/es/widgets';

const searchClient = algoliasearch('XXX', 'XXX');

const search = instantsearch({
  searchClient,
  indexName: 'products'
});

search.addWidgets([
  instantsearch.widgets.searchBox({
    container: '#search',
    placeholder: 'Busca'
  }),

  instantsearch.widgets.hits({
    container: '#hits-products',
    templates: {
      item(item) {
        return `
          <div class="product-block">
            <a href="${ item.url }">
              <p>Exemplo</p>
            </a>
          </div>
        `;
      },
      empty: stringResults + ' <q>{{ query }}</q>'
    }
  }),
]);

search.start();
```

### Cases

[EXS Eletrodomésticos](https://exseletrodomesticos.vnda.dev/)
