---
layout: default
title: Cockpit
parent: Integrações
grand_parent: Recursos
---

# Cockpit
{: .no_toc }

O Cockpit é um gerenciador de conteúdos que permite criarmos objetos com propriedades personalizadas onde podemos adicionar campos dos mais variados tipos de forma ilimitada (Textos, Imagens Únicas, Galerias, Checkboxes, Booleans, Colorpicker, wysiwyg, entre outros).

## Neste artigo:
{: .no_toc .text-delta }

1. TOC
{:toc}

***

## Estrutura de arquivos

- No diretório `liquids` criar uma pasta `media` com o arquivo `blog.liquid` para listagem e `blog_post.liquid` para o post.
- O acesso para a página será `https://[nome_da_loja].vnda.com.br/m/blog` e para o post `https://[nome_da_loja].vnda.com.br/m/blog/[url_gerada_automaticamente]`

Obs.: será necessário dar um `replace` na url do post na listagem para substituir `/m/cockpit` por `/m/blog`

---

## Acesso ao painel

Os acessos para o painel do Vnda Content são informados no card da tarefa.

A URL de acesso padrão é: `https://<nome_da_loja>.vnda.com.br/admin/conteudo/`

**Exemplo:**

Loja Referência: [Care Natural Beauty](https://carenb.vnda.com.br/m/blog)

[Documentação oficial do Cockpit](https://getcockpit.com/documentation/api/)

---

## Setup padrão do Cockpit

### Habilitar o Cockpit no setup desejado

Para ativar o cockpit na loja o atendimento deverá seguir esse procedimento:

[Como solicitar a implementação do Vnda Content](https://trello.com/c/HiDIu58P/102-documenta%C3%A7%C3%A3o-como-solicitar-implementa%C3%A7%C3%A3o-vnda-content "Como solicitar a implementação do Vnda Content")

[Card Modelo]("https://trello.com/c/s81sW0I2/6473-card-modelo-vnda-content")

### API Key

- Após efetuar o login clicar na logo do Cockipt no canto superior esquerdo e no menu que abrir clicar em `Settings`.
- Entrar em `API Access`.
- No ícone azul do lápis clicar para gerar o `Token`.
- No canto inferior esquerdo clicar em `Save`.
- Esse token é utilizado no campo `api_key` do `{% raw %}{% media %}{% endraw %}` para que seja possível buscar o conteúdo.

### Collection padrão para o Blog

- Na tela principal do Cockpit ao lado de `Collections` clicar no `+` (Create Collection).
- Na esquerda por padrão vamos utilizar o name `posts` (utilizado no parâmetro `collection` no {% raw %}`{% media %}`{% endraw %}).
- Habilitar a opção **Custom sortable entries**
- A label pode ser `Blog`.
- Group, Icon, Color e Description são opcionais.
- Habilitar possibilidade de ordenar os posts.

### Campos padrão

No centro da tela onde tem a mensagem `No fields add yet` clicar em `Add field`. Adicionar os seguintes campos: `title`, `thumb`, `description`, `content`, `date`, `publish` e `permalink`. Ao lado de cada campo tem um ícone de `Settings`, clique nele. Aqui é possível configurar o tipo do campo e o título que será mostrado pro cliente quando ele for postar, além de escolher se o campo será obrigatório e se terá tradução.

- Campo `title` deve ser do tipo `text`, com a label `Título` e `Required` marcado.
- Campo `thumb` deve ser do tipo `image` com a label `Thumb` e `Required` marcado.
- Campo `description` deve ser do tipo `textarea` com a label `Descrição` e `Required` marcado.
- Campo `content` deve ser do tipo `Wysiwyg` com a label `Conteúdo` e `Required` marcado.
- Campo `date` deve ser do tipo `date` com a label `Data` e `Required` marcado.
- Campo `publish` deve ser do tipo `boolean` com a label `Publicar` e `Required` marcado.
- Campo `permalink` deve ser do tipo `text` com a label `Permalink` e o `Required` não precisa estar marcado.
- Após configurar os campos não esquecer de clicar em `SAVE` na página da collection.

### Observações

- O campo `content` é onde será inserido todo o conteúdo do post como textos, imagens, vídeos, etc.
- O campo `publish` é pra ser utilizado caso o cliente por algum motivo queira esconder o post do site. Aí o front pode utilizar um `if`
  pra checar se deve ser mostrado ou não.
- O campo `permalink` deverá ser utilizado para a URL amigável do post no futuro, portanto ainda não está habilitado, mas por padrão vamos criar.

- Para campos personalizados que necessitam de configuração no json os exemplos podem ser conferidos neste link:

[Fieldtypes](https://getcockpit.com/documentation/reference/fieldtypes)

- Para utilizar categorias ou qualquer outro tipo de separação dos posts pode ser usado um campo do tipo `Select` seguindo o exemplo de JSON:

```json
{
  "cls": "",
  "options": "Option 1, Option 2, Option 3",
  "default": "Option 2"
}
```

- Também existe a opção de se criar uma outra `Colection` para fazer a separação.

---

## Listagem/Filtros

Os dados são retornados no objeto {% raw %}`{{ post }}`{% endraw %}.

### Listar posts

#### media
{: .d-inline-block}

Liquid v2
{: .label .label-yellow }

{% raw %}
```liquid
{% media from: "cockpit" collection: "nome" api_key: "xxx" page: params.page %}
```
{% endraw %}

{% raw %}
Para alterar ordenação: 

```liquid
{% assign sort = '{ "_created": -1 }' | from_json %} 

{% media ... sort: sort %}
```

Filtrar por algum campo: 

```liquid
{% assign filter = '{ "title": "bar"}' | from_json %} 

{% media ... filter: filter %}
```

Exibir um post: 

```liquid
{% media from: "cockpit" collection: "nome" api_key: "xxx" media_id: params.id %}
```
{% endraw %}

#### load_media
{: .d-inline-block}

Liquid v4
{: .label .label-blue }

`{% raw %}{% load_media from: "cockpit" %}{% endraw %}` (ver doc)

Na versão 4 do Liquid a tag `media` foi descontinuada e a tag `load_media` a substituiu.


---

## Criando a conta do usuário

- Esse código vai gerar o grupo de usuários `loja` que não terá acesso para editar a estrutura das collections.
- Novamente no canto superior esquerdo clique na logo do Cockpit e depois em `Accounts`.
- Na direita clique no botão azul `ADD ACCOUNT`.
- Insira os dados do cliente no formulário. Na coluna da direita altere o Group de `admin` para `loja`.
- Clique em `SAVE`.

### Configurando as permissões do usuário na Collection

- Na tela inicial do painel clique no ícone `Collections`.
- Onde aparece a collection Blog, coloque o mouse sobre o ícone de `Settings` e logo após em `Edit`.
- Logo acima de onde aparecem os campos da collection, ao lado da aba `Fields` clique na aba `Permissions`.
- Você irá visualizar dois grupos de usuários, o `Public` e o `loja`.
- Em `Public` todas as opções deverão ficar em `Vermelho`.
- Em `loja` com excessão da opção `Edit Collection`, todas as opções deverão ficar em `Verde`.
- Clique em `SAVE`.

---

## Cadastro dos Posts

- Na tela inicial do painel na aba `Collections` clique em `Blog`.
- Você irá para uma tela onde serão listados todos os post já cadastrados.
- Se quiser editar, duplicar ou deletar um post, na direita da linha do post clique no ícone de menu e logo após Edit, Duplicate ou Delete.
- Se quiser adicionar um novo post na direita clique no botão azul `ADD ENTRY`.
- Você irá visualizar uma estrutura com os campos criados para a collection blog. Basta preenchê-los.
- O campo `Título` é o título do post.
- O campo `Thumb` é o thumb do post para ser puxado nas listagens.
- O campo `Data` é a data de postagem do post.
- O campo `Publicar` deve ser verdadeiro ou falso, esse parâmetro pode ser utilizado para esconder o post caso seja necessário.
- O campo `Permalink` será utilizado no futuro para definir uma URL amigável e não precisa ser preenchido por enquanto.
- O campo `Conteúdo` é onde irá todo o conteúdo do post. Nele através do item de menu `Insert` é possível inserir links, imagens e vídeos.

No item `Format` é possível alterar a formatação do texto, assim como alinhamento e também a inserção de cabeçalhos.

- Após preencher os campos clique em `SAVE`.

### Observações

- Para inserir uma imagem no corpo do texto deve ser utilizada a opção `add assets` e não a opção `add image`.
- Tamanho máximo para upload de imagens 1mb.
- Para postar vídeos no corpo do post o cliente precisará criar uma conta no youtube.

---

## Posts relacionados

Se o blog possuir posts relacionados na página de post único, você pode criar um campo especial na coleção. Nas configurações da coleção de posts, crie um novo campo:

- Em `name` coloque o nome `related_posts`.
- Nas configurações do campo:
  - _FieldType_: `Collectionlink`
  - _Label_: `Posts relacionados`

Ainda na tela de configurações do campo, adicione o snippet abaixo no bloco _Options JSON_:

```json
{
  "link": "posts", // o nome da coleção que deve ser listada
  "multiple": true, // `true` se for possível selecionar mais de um post relacionado
  "display": "title" // o campo que será exibido para o usuário no editor
}
```

Ao selecionar posts relacionados, o objeto {% raw %}`{{ post }}`{% endraw %} do `blog_post.liquid` retorna um objeto `related_posts` com as IDs e os títulos dos posts relacionados. É possível percorrer esse objeto para exibir um link para eles:

### Exemplo
{: .d-inline-block}

Liquid v2
{: .label .label-yellow }

{% raw %}
```liquid
{% assign related_posts = post.related_posts %}

{% for related_post in related_posts %}
  {% media from: "cockpit" api_key: "API_KEY" media_id: related_post._id %}
    <a href="{{ post.url }}">
      <img src="{{ post.thumb.path }}" alt="{{ post.thumb.meta.title }}" />
      {{ post.title }}
    </a>
  {% endmedia %}
{% endfor %}
```
{% endraw %}

### Exemplo
{: .d-inline-block}

Liquid v4
{: .label .label-blue }

{% raw %}
```liquid
{% assign related_posts = post.related_posts %}

{% for related_post in related_posts %}
  {% load_media from: "cockpit" api_key: "API_KEY" media_id: related_post._id %}
  {% assign current_post = loaded_posts | first %}

  <a href="{{ current_post.url }}">
    <img src="{{ current_post.thumb.path }}" alt="{{ current_post.thumb.meta.title }}" />
    {{ current_post.title }}
  </a>
{% endfor %}
```
{% endraw %}
