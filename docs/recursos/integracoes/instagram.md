---
layout: default
title: Instagram
parent: Integrações
grand_parent: Recursos
---

# Instagram
{: .no_toc }

Esse recurso é desenvolvido utilizando as tags Liquid `load_posts` e `load_post`. Em staging utilizamos placeholder no desenvolvimento pois o Instagram geralmente é configurado apenas no ambiente de produção pela equipe de Atendimento/CS.
