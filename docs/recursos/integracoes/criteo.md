---
layout: default
title: Criteo
parent: Integrações
grand_parent: Recursos
---

# Criteo
{: .no_toc }

Antes de fazer a integração, deve-se acessar a página de integração do cliente para verificar se as primeiras etapas já foram feitas. Estando na etapa "TAGS", o front pode fazer a integração.

O Criteo tem um modo de depuração, esse modo irá ajudar a validar se as informações estão indo corretamente.

***

## Neste artigo:
{: .no_toc .text-delta}

1. TOC
{:toc}

***

## Passo 1: Prepare o Liquid


Para poder pegar certas informações através do Tag Manager, vai ser necessário fazer pequenas alterações nos arquivos Liquid da loja.

###### layout.liquid
{: .no_toc .text-delta }

{% raw %}
```liquid
<script>var pageTemplate = '{{ template }}'</script>

{% if logged_in %}
  <script>var userKey = '{{ client.email }}'</script>
{% else %}
  <script>var userKey = ''</script>
{% endif %}
```
{% endraw %}

###### _product_block.liquid
{: .no_toc .text-delta }

{% raw %}
```liquid
<div class="product-block" data-product-list-id="{{ product.id }}">
  […]
</div>
```
{% endraw %}

***

## Passo 4: Crie as variáveis

| Propriedade          | Valor                |
|----------------------|----------------------|
| Name                 | Vnda - Template page |
| Type                 | JavaScript Variable  |
| Global Variable Name | pageTemplate         |

| Propriedade | Valor                             |
|-------------|-----------------------------------|
| Name        | Vnda - Criteo Account id          |
| Type        | Constant                          |
| Value       | Código da conta Criteo do cliente |

| Propriedade          | Valor               |
|----------------------|---------------------|
| Name                 | Vnda - User key     |
| Type                 | JavaScript Variable |
| Global Variable Name | userKey             |

***

## Passo 3: Crie os eventos

| Propriedade  | Valor                    |
|--------------|--------------------------|
| Evento       | All Pages                |
| Trigger type | Page View                |
| Fires on     | All Pages View           |
| Tags         | _Vnda - Criteo - Loader_ |

| Propriedade  | Valor                                                    |
|--------------|----------------------------------------------------------|
| Evento       | Product page - Window loaded                             |
| Trigger type | Page View - Window Loaded                                |
| Fires on     | `when {% raw %}{{Template page}}{% endraw %} == 'product'` |
| Tags         | _Vnda - Criteo - Product Page_                           |

| Propriedade  | Valor                                                |
|--------------|------------------------------------------------------|
| Evento       | Tag page - Window loaded                             |
| Trigger type | Page View - Window Loaded                            |
| Fires on     | `when {% raw %}{{Template page}}{% endraw %} == 'tag'` |
| Tags         | _Vnda - Criteo - Products page_                      |

| Propriedade  | Valor                                                 |
|--------------|-------------------------------------------------------|
| Evento       | Home page - Window loaded                             |
| Trigger type | Page View - Window Loaded                             |
| Fires on     | `when {% raw %}{{Template page}}{% endraw %} == 'home'` |
| Tags         | _Vnda - Criteo - Homepage_                            |

| Propriedade  | Valor                                                     |
|--------------|-----------------------------------------------------------|
| Evento       | Carrinho page - Window load                               |
| Trigger type | Page View - Window Loaded                                 |
| Fires on     | `when url contains'carrinho'` |
| Tags         | _Vnda - Criteo - Cartpage_                                |

| Propriedade  | Valor                                                      |
|--------------|------------------------------------------------------------|
| Evento       | Concluido page - Window load                               |
| Trigger type | Page View - Window Loaded                                  |
| Fires on     | `when url contains 'concluido'` |
| Tags         | _Vnda - Criteo - Track Transaction_                        |


***

## Passo 4: Crie as tags

### Criteo Loader

| Propriedade | Valor                  |
|-------------|------------------------|
| Tag         | Vnda - Criteo - Loader |
| Event       | Page View              |
| Pages       | All Pages              |
| Event Name  | All Pages              |

**Conteúdo da tag:**

{% raw %}
```html
<script type="text/javascript">
  !function () {
    function insertScript(_src) {
      var criteoScript = document.createElement('script');
      criteoScript.src = _src;
      criteoScript.async = true;
      var elemReference = document.getElementsByTagName('script')[0];

      elemReference.parentNode.insertBefore(criteoScript, elemReference);
    }

    insertScript("//static.criteo.net/js/ld/ld.js");
    insertScript("//wurfl.io/wurfl.js");
  }();

  function getSiteType () {
    var result = 'd';
  
    try {
      if (WURFL.is_mobile) {
        result = 'm';
      if (WURFL.form_factor == 'Tablet') {
        result = 't';
      }
    }
    } catch (err) {
        console.info('err', err);
    }
  
      return result;
  }
</script>
```
{% endraw %}

***

### Home Page

| Propriedade | Valor                                                 |
|-------------|-------------------------------------------------------|
| Tag         | Vnda - Criteo - Homepage                              |
| Event       | Window Loaded                                         |
| Pages       | `when {% raw %}{{Template page}}{% endraw %} == 'home'` |
| Event Name  | Home page - Window loaded                             |

**Conteúdo da tag:**

{% raw %}
```html
<script type="text/javascript">
  window.criteo_q = window.criteo_q || [];
  window.criteo_q.push(
    { event: "setAccount", account: {{Vnda - Criteo Account id}} },
    { event: "setSiteType", type: getSiteType() },
    { event: "setEmail", email: {{Vnda - User key}} },
    { event: "viewHome" })
</script>
```
{% endraw %}

***

### Products Page

| Propriedade | Valor                                                |
|-------------|------------------------------------------------------|
| Tag         | Vnda - Criteo - Products page                        |
| Event       | Window Loaded                                        |
| Pages       | `when {% raw %}{{Template page}}{% endraw %} == 'tag'` |
| Event Name  | Product page - Window loaded                         |

**Conteúdo da tag:**

{% raw %}
```html
<script>
  function getProductIds() {
    var ids = [];
    var products = document.querySelectorAll('[data-product-list-id]');
    var maxQtdIds = 3;

    for (var i = 0; i < products.length; i++) {
      if ((i + 1) <= maxQtdIds) {
        ids.push(products[i].dataset['productListId']);
      }
    }

    return ids;
  }

  var productsIds = getProductIds();

  if (productsIds.length > 0) {
    window.criteo_q = window.criteo_q || [];
    window.criteo_q.push(
      { event: "setAccount", account: {{Vnda - Criteo Account id}} },
      { event: "setSiteType", type: getSiteType() },
      { event: "setEmail", email: {{Vnda - User key}} },
      { event: "viewList", item: getProductIds() });
  }  
</script>
```
{% endraw %}

***

### Product page

| Propriedade | Valor                                                    |
|-------------|----------------------------------------------------------|
| Tag         | Vnda - Criteo - Product page                             |
| Event       | Window Loaded                                            |
| Pages       | `when {% raw %}{{Template page}}{% endraw %} == 'product'` |
| Event Name  | Product page - Window loaded                             |

**Conteúdo da tag:**

{% raw %}
```html
<script>
  window.criteo_q = window.criteo_q || [];
  window.criteo_q.push(
    { event: "setAccount", account: {{Vnda - Criteo Account id}} },
    { event: "setSiteType", type: getSiteType() },
    { event: "setEmail", email: {{Vnda - User key}} },
    { event: "viewItem", item: location.pathname.split(/-(\d+)$/, 2)[1] });
</script>
```
{% endraw %}

***

### Cart Page

| Propriedade | Valor                                                        |
|-------------|--------------------------------------------------------------|
| Tag         | Vnda - Criteo - Cart Page                                    |
| Event       | Window Loaded                                                |
| Pages       | `when {% raw %}{{ Page URL }}{% endraw %} contains 'carrinho'` |
| Event Name  | Purchase page - Window loaded                                |

**Conteúdo da tag:**

{% raw %}
```html
<script>
  function getEmailUser() {
    var email = '';

    try {
      if (!!document.querySelector('.email-container input')) {
        if (document.querySelector('.email-container input').value != '') {
          email = document.querySelector('.email-container input').value;
        }
      }

      if (email == '') {
        if (!!localStorage.ajs_user_traits) {
          var objEmail = JSON.parse(localStorage.ajs_user_traits);

          if (typeof objEmail.email != 'undefined') {
            email = objEmail.email;
          }
        }
      }
    } catch (err) {
      console.error('error', err);
    }

    return email;
  }


  function getItemsCart() {
    var products = [];

    try {
      var elementIds = document.getElementById('rmkt-product-ids') || false;
      var ids = [];

      if (elementIds) {
        ids = elementIds.value.split(' ');

        if (ids[0] != '') {
          for (var i = 0; i < ids.length; i++) {

            var elemId = document.querySelector('[data-product-id="' + ids[i] + '"]');
            elemId = elemId.children[0];
            var infosProduct = JSON.parse(elemId.dataset.eventJson);

            products.push({
              id: ids[i],
              price: infosProduct.price,
              quantity: infosProduct.quantity,
            })
          }
        }
      }

    } catch (err) {
      console.error('error', err);
    }

    return products;
  }


  if (getItemsCart().length > 0) {
    window.criteo_q = window.criteo_q || [];
    window.criteo_q.push(
      { event: "setAccount", account: {{Vnda - Criteo Account id}} },
      { event: "setSiteType", type: getSiteType() },
      { event: "setEmail", email: getEmailUser() },
      {
        event: "viewBasket",
        item: getItemsCart(),
      });
    }
</script>
```
{% endraw %}

***

### Track Transaction

| Propriedade | Valor                                                       |
|-------------|-------------------------------------------------------------|
| Tag         | Vnda - Criteo - Track Transaction                           |
| Event       | Window Loaded                                               |
| Pages       | `when {% raw %}{{Page URL}}{% endraw %} contains 'concluido'` |
| Event Name  | Purchase page - Window loaded                               |

**Conteúdo da tag:**

{% raw %}
```html
<script>
  function getEmailUser() {
    var clientInfo = '';
    var email = '';

    try {
      clientInfo = document.getElementById('client_data');
      email = clientInfo.querySelector('p:nth-child(2)').innerText;
    } catch (err) {
      console.error('error', err);
    }

    return email;
  }


  function getTransactionId() {
    var transactionId = '';

    try {
      transactionId = dataLayer[0].ecommerce.purchase.id;
    } catch (err) {
      console.error('error', err);
    }

    return transactionId;
  }


  function getItemsTransaction() {
    var products = [];

    try {
      for (var i = 0; i < dataLayer[0].fb_pixel.items.length; i++) {
        var product = dataLayer[0].fb_pixel.items[i];

        products.push({
          id: product.id,
          price: product.price,
          quantity: product.quantity
        });
      }
    } catch (err) {
      console.error('error', err);
    }

    console.info(products);

    return products;
  }


  window.criteo_q = window.criteo_q || [];
  window.criteo_q.push(
    { event: "setAccount", account: {{Vnda - Criteo Account id}} },
    { event: "setSiteType", type: getSiteType() },
    { event: "setEmail", email: getEmailUser() },
    {
      event: "trackTransaction",
      id: getTransactionId(),
      item: getItemsTransaction(),
    });
</script>
```
{% endraw %}
