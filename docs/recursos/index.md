---
layout: default
title: Recursos
nav_order: 6
has_children: true
---

# Recursos
{: .no_toc }

Confira nessa seção recursos da plataforma que podem ser contratados e/ou integrados nas lojas.
