---
layout: default
title: Internacionalização
parent: Recursos Extras
grand_parent: Recursos
has_children: true
---

# Internacionalização
{: .no_toc }

A internacionalização das lojas da Vnda permite que uma loja ofereça navegação e conteúdos em idiomas diferentes enquanto aproveita o mesmo layout e estrutura.

## Nesse artigo
{: .no_toc .text-delta }

1. TOC
{:toc}

Para ativar a internacionalização da loja é preciso criar condições para a exibição de partes do layout em um idioma. Essas condições são feitas com o objeto {% raw %}`{{ language }}`{% endraw %}:

###### Exemplo
{: .no_toc .text-delta }

{% raw %}
```liquid
{% if language == "en" %}
  Esse texto aparecerá na versão da loja em inglês.
{% elsif language == "pt-BR" %}
  Esse texto aparecerá na versão da loja em português.
{% endif %}
```
{% endraw %}

Esse artigo documenta como traduzir e dinamizar áreas do site para criar lojas internacionais com o maior proveito de código possível.

***

## Acessando a versão internacional

Por padrão, os idiomas das lojas são automaticamente definidos como português (`pt-BR`). Para acessar a versão internacional da loja, é preciso enviar o idioma como um parâmetro de URL `language`.

Para que visitantes da loja possam acessar as versões internacionais dela, é preciso criar links para a página atual com o parâmetro `language`.

###### Exemplo
{: .no_toc .text-delta }

{% raw %}
```liquid
{% comment %}
  É importante conferir se a página atual já possui 
  parâmetros de URL sendo enviados (como termos de busca 
  ou filtros de tag).
{% endcomment %}

{% if current_url contains "?" %}
  <a href="{{ current_url | append: '&language=pt-BR' }}" rel="alternate">PT</a>
  <a href="{{ current_url | append: '&language=en' }}" rel="alternate">EN</a>
{% else %}
  <a href="{{ current_url | append: '?language=pt-BR' }}" rel="alternate">PT</a>
  <a href="{{ current_url | append: '?language=en' }}" rel="alternate">EN</a>
{% endif %}
```
{% endraw %}

***

## Considerações de internacionalização

Na Vnda, consideramos uma boa prática mantermos a versão internacional de uma página junto ao seu código original. Assim exige menos repetição de código e, consequentemente, a manutenção do código é mais ágil e segura.

É importante perceber que, na maioria dos casos, as versões internacionais de uma loja mantém toda a mesma lógica dos templates, enquanto o que muda é o conteúdo dinâmico. Por exemplo:

### `partials/header/_menu.liquid`
{: .no_toc .text-delta .d-inline-block}

Liquid v2
{: .label .label-yellow }

{% raw %}
```liquid
{% capture main_menu %}
  {% menus position: 'principal' %}
    <li class="menu-item">
      <a href="{{ menu.url }}" {% if menu.external %}target="_blank"{% endif %}>
        {{ menu.title }}
      </a>
    </li>
  {% endmenus %}
{% endcapture %}

{% unless main_menu == blank %}
  <nav class="site-navigation">
    <ul class="main-menu">
      {{ main_menu }}
    </ul>
  </nav>
{% endunless %}
```
{% endraw %}

No exemplo acima, estamos criando a estrutura do menu de navegação principal da loja. Como um menu terá itens diferentes na versão internacional, ao invés de duplicarmos toda a lógica de exibição podemos apenas controlar a posição. Em uma loja internacional, o snippet acima ficaria assim:

### `partials/header/_menu.liquid`
{: .no_toc .text-delta .d-inline-block}

Liquid v2
{: .label .label-yellow }

{% raw %}
```liquid
{% assign menu_position = "principal" %}

{% unless language == "pt-BR" %}
  {% assign menu_position = menu_position | append: "_" | append: language %}
{% endunless %}

{% capture main_menu %}
  {% menus position: menu_position %}
    <li class="menu-item">
      <a href="{{ menu.url }}" {% if menu.external %}target="_blank"{% endif %}>
        {{ menu.title }}
      </a>
    </li>
  {% endmenus %}
{% endcapture %}

{% unless main_menu == blank %}
  <nav class="site-navigation">
    <ul class="main-menu">
      {{ main_menu }}
    </ul>
  </nav>
{% endunless %}
```
{% endraw %}

Controlando a posição do menu `principal` no mesmo arquivo onde a lógica do menu se encontra, podemos manter a mesma estrutura para as versões da loja, duplicando o mínimo possível de código.

***

## Traduzindo strings, botões e mensagens

Todas as strings presentes no código dos templates e scripts da loja precisam ser traduzidas usando [essa relação de expressões padrão]({%- link docs/recursos/extras/i18n/expressoes-padrao.md -%}).

Nos templates, essa tradução é feita diretamente nos arquivos onde as strings estão presentes para facilitar a manutenção.

### `partials/common/_footer.liquid`
{: .no_toc .text-delta }

{% raw %}
```liquid
{% assign text_newsletter_subscribe = "Assine a newsletter" %}

{% if language == "en" %}
  {% assign text_newsletter_subscribe = "Sign up for the newsletter" %}
{% elsif language == "es" %}
  {% assign text_newsletter_subscribe = "Suscríbase al boletín" %}
{% endif %}


<form>
  <h4>{{ text_newsletter_subscribe }}</h4>
[...]
```
{% endraw %}

Nos scripts, é importante localizar strings e dinamizar a exibição de mensagens através do [objeto `texts[]`, definido no `store.js`](https://gitlab.com/vnda/setup/estrutura_padrao/blob/master/assets/javascripts/store.js#L11):

```javascript
texts['pt-BR'] = {
  quantity: 'quantidade',
  price: 'preço',
  cart_empty: 'O seu carrinho está vazio',
  go_product: 'Ir para a página do produto',
  btn_edit_cart: 'Editar pedido',
  btn_checkout: 'Finalizar pedido',
  btn_success: 'Adicionado!',
  errorsTxt: {
    withoutSku: 'Selecione um atributo para o produto'
  }
};

texts['en'] = {
  quantity: 'quantity',
  price: 'price',
  cart_empty: 'Your cart is empty',
  go_product: 'Go to product page',
  btn_edit_cart: 'Edit',
  btn_checkout: 'Finish',
  btn_success: 'Added',
  errorsTxt: {
    withoutSku: 'Select an attribute'
  }
};
```

Sinta-se livre para adicionar mais strings no objeto `texts[]` e integrá-lo nas funções personalizadas da loja, se necessário.

***

## Traduzindo blocos dinâmicos da loja

Blocos dinâmicas da loja que são editadas pelos clientes (como banners, menus e páginas personalizadas), são traduzidos criando posições alternativas. É uma boa prática manter as posições originais para a loja em português, e adicionar `_<idioma>` para as versões internacionais.
  
### `partials/home/_fullbanner.liquid`
{: .no_toc .text-delta }

{% raw %}
```liquid
{% assign fullbanner_position = "fullbanner" %}

{% unless language == "pt-BR" %}
  {% assign fullbanner_position = fullbanner_position | append: "_" | append: language %}
{% endunless %}
```
{% endraw %}

O snippet acima define uma posição `fullbanner` diferente para cada idioma. Assim, podemos usar a posição dinâmica mantendo a mesma lógica do template:

### `partials/components/fullbanner.liquid`
{: .no_toc .text-delta .d-inline-block}

Liquid v2
{: .label .label-yellow }

{% raw %}
```liquid
{% banners tag: fullbanner_position %}
  A estrutura do fullbanner vem aqui.
{% endbanners %}
```
{% endraw %}

A mesma lógica pode ser feita com os menus, definindo uma posição utilizando o objeto {% raw %}`{{ language }}`{% endraw %}:

### `partials/header/_menu.liquid`
{: .no_toc .text-delta .d-inline-block}

Liquid v2
{: .label .label-yellow }

{% raw %}
```liquid
{% assign menu_position = "principal" %}

{% unless language == "pt-BR" %}
  {% assign menu_position = menu_position | append: "_" | append: language %}
{% endunless %}

{% menus position: menu_position %}
  A estrutura do item de menu vem aqui.
{% endmenus %}
```
{% endraw %}

### Páginas personalizadas

Se as páginas personalizadas possuem estruturas controladas pela URL da página através do template `page.liquid` (e suas partials), é preciso criar condições para a exibição de páginas internacionais. Por exemplo:

#### `page.liquid`
{: .no_toc .text-delta }

{% raw %}
```liquid
{% if page.slug == "sobre" %}
  {% include "partials/pages/_sobre" %}
{% elsif page.slug == "about" %}
  {% include "partials/pages/_sobre_en" %}
{% endif %}
```
{% endraw %}

Se a página personalizada for controlada através de benners específicos, é necessário criar as posições alternativas e referenciá-las no novo template:

#### `partials/pages/_sobre_en.liquid`
{: .no_toc .text-delta .d-inline-block}

Liquid v2
{: .label .label-yellow }

{% raw %}
```liquid
{% assign sobre_conteudo = "sobre-conteudo_en" %}

{% banners tag: sobre_conteudo %}
  Estrutura do banner.
{% endbanners %}
```
{% endraw %}


Se as páginas personalizadas forem controladas pelo atributo `language` definido na URL (se tanto `Sobre` quanto `About` respeitarem a mesma URL `/sobre`), é preciso criar um comportamento semelhante aos banners e menus do restante da loja:

#### `partials/pages/_sobre.liquid`
{: .no_toc .text-delta .d-inline-block}

Liquid v2
{: .label .label-yellow }

{% raw %}
```liquid
{% assign sobre_conteudo = "sobre-conteudo" %}

{% unless language == "pt-BR" %}
  {% assign sobre_conteudo = "sobre-conteudo_en" %}
{% endunless %}

{% banners tag: sobre_conteudo %}
  Estrutura do banner.
{% endbanners %}
```
{% endraw %}

***

## Traduzindo informações do produto

É preciso criar condições para a exibição das informações do produto cadastradas. O título, a descrição e as informações de variantes devem seguir os padrões de cadastro documentados abaixo. (Lembre-se que essas informações devem ser traduzidas )

### Nome do produto

Se o nome do produto possuir versões diferentes na loja internacional, é preciso separar essas versões com o caractere `|`, exemplo:

```
Nome do produto em português | Nome do produto em inglês | Nome do produto em espanhol
```

Assim, é preciso identificar qual parte do título será exibida no template:

#### `partials/common/_product_block.liquid`
{: .no_toc .text-delta }

{% raw %}
```liquid
{% assign product_name = product.name %}

{% if product.name contains "|" %}
  {% assign i18n_product_names = product.name | split: "|" %}
  
  {% if language == "en" %}
    {% assign product_name = i18n_product_names[1] %}
  {% elsif language == "es" %}
    {% assign product_name = i18n_product_names[2] %}
  {% else %}
    {% assign product_name = i18n_product_names[0] %}
  {% endif %}
{% endif %}

<h4 class="product-name">{{ product_name }}</h4>
```
{% endraw %}

### Descrição do produto

A descrição do produto, exibida no [template `product.liquid`]({% link docs/liquid4/templates/product.md %}#descrição-do-produto), também deve demarcar o início de outro idioma:

#### `partials/product/_description.liquid`
{: .no_toc .text-delta }

<div class="code-example" markdown="1">
```
Descrição do produto em português
---
Seção de descrição do produto em português

[idioma]

Descrição do produto em inglês
---
Seção de descrição do produto em inglês

[idioma]

Descrição do produto em espanhol
---
Seção de descrição do produto em espanhol
```
</div>
{% raw %}
```liquid
{% comment %}
  Primeiro separamos as versões da descrição do produto.
{% endcomment %}
{% assign product_description = product.description %}

{% if product_description contains "[idioma]" %}
  {% assign i18n_descriptions = product.description | split: '[idioma]' %}

  {% assign product_description = i18n_descriptions[0] %}

  {% if language == "en" %}
    {% assign product_description = i18n_descriptions[1] %}
  {% elsif language == "es" %}
    {% assign product_description = i18n_descriptions[2] %}
  {% endif %}
{% endif %}

{% comment %}
  E então tratamos a descrição do produto em si.
{% endcomment %}
{% assign description_sections = product_description | split: "<hr/>" %}

{% for section in description_sections %}
  Tratamento das seções de descrição do idioma.
{% endfor %}
```
{% endraw %}

**Nota:** Se a loja só possuir um outro idioma na versão internacional, é possível definir a quebra de idioma como `[english]`, para ficar mais claro para o cliente. Lembre-se de adaptar o código de exemplo.

### Preço do produto

Se a loja internacional deve exibir o preço internacional, siga os passos abaixo:

1. Acesse as configurações do admin
2. Configurações adicionais
3. Insira `intl_price: 1` no campo
4. Aperte ENVIAR

Agora cada variante possui seu campo **Preço internacional**.

Após cadastrar os preços internacionais, configure no front da loja qual preço deve ser exibido.

O trecho abaixo deve ser inserido em `_infos.liquid`, `_product_block.liquid` e em qualquer outro arquivo que utilize o atributo `product.price` do objeto `product`.

{: .no_toc .text-delta}
{% raw %}
```liquid
{% assign product_price = product.price %}

{% unless language == "pt-BR" %}

  {% for variant in product.variants %}
    {% if variant.main %}
      {% assign product_price = variant.properties.property3.value %}
    {% endif %}
  {% endfor %}
  
{% endunless %}

<h4 class="product-price">{{ product_price | money_format }}</h4>
```
{% endraw %}

### Atributos

Da mesma forma que a descrição do produto, se for necessário exibir valores diferentes para os atributos, é possível separá-los usando um shortcode `[idioma]`, que demarca as versões internacionais:

<div class="code-example" markdown="1">
```
Amarelo | #ffc [idioma] Yellow | #ffc [idioma] Amarillo | #ffc
```
</div>
{% raw %}
```liquid
{% unless attr2_value == blank %}
  {% assign color = attr2_value %}
  
  {% if attr2_value contains "[idioma]" %}
    {% assign i18n_titles = attr2_value | split: "[idioma]" %}
    
    {% assign color = i18n_titles[0] %}
    
    {% if language == "en" %}
      {% assign color = i18n_titles[1] %}
    {% elsif language == "es" %}
      {% assign color = i18n_titles[2] %}
    {% endif %}
  {% endif %}

  {% assign color_name = color | split: "|" | first %}
  {% assign color_hex = color | split: "|" | last %}
  <span class="color_option" style="background-color: {{ color_hex }};">{{ color_name }}</span>
{% endunless %}
```
{% endraw %}

**Nota:** Se a loja só possuir um outro idioma na versão internacional, é possível definir a quebra de idioma como `[english]`, para ficar mais claro para o cliente. Lembre-se de adaptar o código de exemplo.

***

## Traduzindo informações de tags

Se uma loja utilizará as mesmas tags nas versões internacionais da loja, será necessário controlar a exibição dos nomes e descrições da tag de acordo com o idioma, de uma forma semelhante ao que é feito no produto.

### Título da tag
{: .no_toc .text-delta }

<div class="code-example" markdown="1">
```
Tag em Português | Tag em Inglês | Tag em Espanhol
```
</div>
{% raw %}
```liquid
{% assign tag_title = tag.title %}

{% if tag.title contains "|" %}
  {% assign i18n_titles = tag.title | split: "|" %}
  
  {% assign tag_title = i18n_titles[0] %}
  
  {% if language == "en" %}
    {% assign tag_title = i18n_titles[1] %}
  {% elsif language == "es" %}
    {% assign tag_title = i18n_titles[2] %}
  {% endif %}
{% endif %}

<h1 class="tag-title">{{ tag_title }}</h1>
```
{% endraw %}

### Descrição da tag
{: .no_toc .text-delta }

<div class="code-example" markdown="1">
```
Descrição da tag em portugês

[idioma]

Descrição da tag em inglês

[idioma]

Descrição da tag em espanhol
```
</div>
{% raw %}
```liquid
{% assign tag_description = tag.description %}

{% if tag.description contains "[idioma]" %}
  {% assign i18n_descriptions = tag.description | split: "[idioma]" %}
  
  {% assign tag_description = i18n_descriptions[0] %}
  
  {% if language == "en" %}
    {% assign tag_description = i18n_descriptions[1] %}
  {% elsif language == "es" %}
    {% assign tag_description = i18n_descriptions[2] %}
  {% endif %}
{% endif %}

<div class="tag-description">
  {{ tag_description }}
</div>
```
{% endraw %}
