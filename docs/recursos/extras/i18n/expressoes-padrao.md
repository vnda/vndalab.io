---
layout: default
title: Expressões padrão
parent: Internacionalização
grand_parent: Recursos Extras
has_children: false
---

# Expressões padrão para lojas internacionais
{: .no_toc }

Confira a referência de expressões padrão traduzidas para as lojas internacionais. Se uma expressão que você precisa usar não está na relação abaixo, [nos avise nesse card](https://trello.com/c/FLrVFHqx).

| Expressão | Inglês | Espanhol |
|-----------|--------|----------|
| Busca | Search | Buscar |
| Carrinho | Cart | Carro de compras |
| Seu carrinho está vazio | Your cart is empty | Su carrito esta vacio |
| Assine a newsletter | Sign up for the newsletter | Suscríbase al boletín |
| Voltar ao topo | Back to top | Volver arriba |
| Guia de tamanho | Size guide | Guía de tallas |
| Adicionar ao carrinho | Add to bag | Agregar al carrito |
| Comprar | Purchase | Comprar |
| Quantidade | Quantity | Cantidad |
| Finalizar pedido | Finish | Pago |
| Editar pedido | Edit | Editar orden |
| Ir para a página do produto | Go product | Ir a la página del producto |
| Selecione um atributo | Select an attribute | Selecciona un atributo |
| Adicionado | Added | Agregado |
| Pagamento | Payment | Pago |
| Contato | Contact us | Contacto |
| Sobre | About us | Sobre nosotros |
| Ordenar por | Sort By | Ordenar por |
| Mais antigo | Older | Más antiguo |
| Mais recente | Newest | Más reciente |
| Menor preço | Lowest price | El precio más bajo |
| Maior preço | Biggest price | Precio más alto |
| Filtro | Filter | Filtro |
| Preço | Price | Precio |
| Categoria | Category | Categoria |
| Tamanhos | Size | Tamaños |
| Cor | Color | Color |
| Entrar | Sign In | Iniciar sesión |
| Cadastrar | Register | Registrar |
| Conta | Account | Cuenta |
| Mostrar Mais | Show more | Mostrar más |
| Mostrar Menos | Show less | Mostrar menos |
| Continuar comprando | Continue Shopping | Seguir comprando |
