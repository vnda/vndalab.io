---
layout: default
title: Produtos relacionados
parent: Recursos Extras
grand_parent: Recursos
nav_order: 1
---

# Produtos relacionados x Produtos relacionados por referência
{: .no_toc }

# Índice
{: .no_toc .text-delta }

1. TOC
{:toc}

***

# Produtos relacionados padrão

{% raw %}
```liquid
{% assign current_product = product %}
{% assign related_title = 'Produtos Relacionados' %}

{% comment %} Loop em product.tags ao invés de chamar {% tags %} {% endcomment %}

{% for tag in product.tags %}
  {% if tag.type == 'relacionado' %}

    {% if tag.title and tag.title != '' %}
      {% assign related_title = tag.title %}
    {% endif %}

    {% comment %}
      Limita a quantidade de produtos exibidos com o parametro 'per_page', exemplo:
      {% load_products tag: tag.name per_page: 4%}
    {% endcomment %}
    {% load_products tag: tag.name %}


    {% capture html_relateds %}
      {% for product in products %}
        {% unless product.id == current_product.id %}
          {% include 'partials/common/product_block' %}
        {% endunless %}
      {% endfor %}
    {% endcapture %}

    {% break %}
  {% endif %}
{% endfor %}

{% if html_relateds != blank %}
  {{ related_title }}
  {{ html_relateds }}
{% endif %}
```
{% endraw %}

***

# Orientaçao de cadastro padrão

- Criar uma tag com o **tipo** `relacionado`, o nome da tag pode variar de acordo com a organização do cliente;

- O título da seção é o título da tag, se não tiver título, o texto padrão será **Produtos Relacionados**;

- Adicionar a tag no grupo de produtos que será relacionado.

***

# Produtos relacionados por referência

Quando utilizamos os relacionados por tag do tipo relacionados a seção é exibida em todos os produtos que possuem aquele tipo de tag.

A funcionalidade Relacionados por referencia foi desenvolvida com o objetivo de permitir que o cliente escolha em quais produtos deseja exibir a seção e selecionar especificamente os produtos que devem ser exibidos.

Para pegar esses produtos verificamos se o produto atual possui uma tag com o tipo igual a sua referencia, caso tenha, utilizamos o {% raw %} `{% load_products %}` {%endraw}  para fazer um for nos produtos com essa tag (excluindo o produto atual).

***

{% raw %}
```liquid

{% assign current_reference = product.reference %}
{% assign current_product = product %}
{% assign related_title = 'Produtos Relacionados' %}

{% comment %} Loop em product.tags ao invés de chamar {% tags %} {% endcomment %}

{% for tag in product.tags %}
  {% if tag.type == current_reference %}

    {% if tag.title and tag.title != '' %}
      {% assign related_title = tag.title %}
    {% endif %}

    {% comment %}
      Limita a quantidade de produtos exibidos com o parametro 'per_page', exemplo:
      {% load_products tag: tag.name per_page: 4%}
    {% endcomment %}
    {% load_products tag: tag.name %}

    {% capture html_relateds %}
      {% for product in products %}
        {% unless product.id == current_product.id %}
          {% include 'partials/common/product_block' %}
        {% endunless %}
      {% endfor %}
    {% endcapture %}

    {% break %}
  {% endif %}
{% endfor %}

{% if html_relateds != blank %}
  {{ related_title }} 
  {{ html_relateds }}
{% endif %}
```
{% endraw %}

***

# Orientaçao de cadastro por referência

- Escolher o produto principal que será o único a exibir os relacionados e copiar a **referencia deste produto**

- Criar uma tag com o tipo igual a **referencia copiada**

- O título da seção é o título da tag, se não tiver título, o texto padrão será **Produtos Relacionados**;

- Inserir a tag criada no **produto principal** e nos **produtos que devem ser exibidos** na seção de relacionados.

Desta forma, a seção de produtos relacionados será exibida somente na página do produto que possui a referencia informada no tipo da tag.
