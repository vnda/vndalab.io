---
layout: default
title: Formulários
parent: Recursos Extras
grand_parent: Recursos
nav_order: 2
published: false
---

# Formulários
{: .no_toc }

# Índice
{: .no_toc .text-delta }

1. TOC
{:toc}

***

## Criando uma entrada de formulário no Admin ##

* No admin da loja clique em `Configurações`.
* Clique em `Adicionar Form`.
* No campo `Destinatário` em Stg você pode colocar seu e-mail para testar, em Prod coloque o e-mail indicado pelo cliente.
* No campo `Assunto` coloque o nome do form. Esse campo será usado para ativar o formulário no Liquid.

***


## Adicionando um formulário na Loja ##

O seguinte modelo pode ser utilizado para formulários:

### Estrutura do formulário

O exemplo abaixo é a estrutura básica para qualquer formulário alimentado pela plataforma, basta adicionar os campos desejados e adicionar as validações no arquivo Javascript. Note que o formulário não possui o atributo `action`, que deve ser 

{: .label .label-yellow }
O campo `vnda` é utilizado para captura de spam. Ele não deve ser exibido na loja, mas deve existir na estrutura do formulário.

{% raw %}
```html
<form data-webform="nomedoform">
  <!--'nomedoform' de acordo com o webform cadastrado no admin -->
  <input type="hidden" name="key" value="{nomedaloja}-{nomedoform}">
  <input type="hidden" name="reply_to" id="reply_to" value="">
  <!-- Campo do honeypot -->
  <input type="text" name="vnda" tabindex="-1" autocomplete="false">
  
  <div class="field">
    <input type="email" name="email" placeholder="Seu email" required>
  </div>
  <div class="field submit">
    <button type="submit" disabled="disabled">
        ENVIAR
    </button>
  </div>
  
  <!-- Essa é a div recebe o retorno do envio do formulário -->
  <div class="msg-retorno" data-msg-retorno="">
    <div data-msg=""></div>
    <div class="msg-success" data-msg-success="">SUCESSO</div>
    <div class="msg-error" data-msg-error="">FALHA</div>
  </div>
</form>
```
{% endraw %}

### Função de validação

Em store.js existe o método `submitForm()` que é responsável por lidar com webforms
O trecho de código a seguir é uma validação comumente usada em formulários de newsletter

{: .label .label-yellow }
Esse código deve ser copiado na função apropriada dentro do arquivo store.js > template_store

{% raw %}
```js
 /**
 * Validação que pode ser específica para o formulário
 * Nesse caso está sendo validado e-mail com formato email@email.domínio
 * No exemplo do form HTML já está sendo utilizado a propriedade type="email" que valida se o formato digitado
 * é realmente um e-mail e também o atributo Required que obriga que o campo seja preenchido, mas caso seja
 * necessária alguma customização diferente do padrão do HTML você pode utilizar essa função.*/

validateFormNewsletter: function (_$form) {
  var validated = true;
  var errorsTxt = [];

  if (_$form.find('[name="email"]').val() == '') {
    validated = false;
    errorsTxt.push('Informe seu email!');
  } else if (/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(_$form.find('[name="email"]'))) {
    validate = false;
    errorsTxt.push('Informe um e-mail válido!');
  }
  return { validated: validated, errors: errorsTxt };
},

/* O campo `vnda` utiliza o estilo `display: none;`. Se ele for preenchido essa função identifica
* que o formulário foi preenchido por um robô e impede o envio. */

/* Deixa o botão desabilitado por 6 segundos para que não seja preenchido por um robô
* pode ser alterado o tempo conforme necessidade. */
setTimeout(function () {
        $('[data-webform] button').attr("disabled", false);
        console.log('form liberado');
}, 6000);

$('[data-webform]').on('submit', function (event) {
  event.preventDefault();
  var $form = $(this);

  var honeypot = $(this).find('[name="vnda"]');

  if (honeypot.length > 0) {
    if (honeypot.val() != '') {
      console.info('ROBOT DETECTED');
      return false;
    }
  }

  if ($(this).data('webform') == 'newsletter') {

    /* Chama a validação específica declarada anteriormente */
    var respValidated = _this.validateFormNewsletter($(this));
    var htmlErrors = '';
    var email = $(this).find('[name="email"]').val();

    /* Se os campos forem validados ele executa  a função submitForm que executa o envio
    * do formulário. Se ocorrer algum problema na validação ele retorna a mensagem com o erro.*/

    if (respValidated.validated) {
      store.submitForm($form, email, function () {
        store.setMsgResponse(false, 'clear', $form);
      });
      setTimeout(function() {
        store.setMsgResponse('', 'clear', $form);
      }, 3500);
    } else {
      store.setMsgResponse(respValidated.errors, 'error', $form);
      setTimeout(function() {
        store.setMsgResponse('', 'clear', $form);
      }, 3500);
    }

  }

  /* Caso exista mais de um webform podemos usar:
    
    if($(this).data('webform') == 'nomedoform') {
      var respValidated = _this.validateNomeDoForm($(this));
    }

  */
});

```
{% endraw %}

### Função submitForm ###

{% raw %}
```js
 submitForm: function (_$form) {

    _$form.find('[name="reply_to"]').val(_$form.find('[name="email"]').val());

     /* Executa a função Ajax para envio do form.
     Enquanto ele estiver sendo enviado é adicionada a classe sending que 
     evita que o form seja enviado antes de concluir o processo. */

    if (!_$form.hasClass('sending')) {
      $.ajax({
        url: '/webform',
        type: 'POST',
        data: _$form.serialize(),
        beforeSend: function () {
          _$form.addClass('sending');
        }
      })

      /* Caso o form seja enviado ele oculta as mensagens de erro que possam ter aparecido
      e mostra a mensagem de sucesso. */

      .done(function() {
        // store.setMsgResponse('Mensagem enviada com sucesso!', 'success', _$form);
        _$form.find('[data-msg-retorno] [data-msg-success]').addClass('visible');
        _$form.find('[data-msg-retorno] [data-msg-error]').removeClass('visible');
        _$form.find("button[type=submit]").text('Enviado');

        setTimeout(function() {
          _$form.find("button[type=submit]").text('Enviar');
        }, 3500);
        _$form[0].reset();
      })

      /* Caso ocorra um problema ele mostra a mensagem de erro. */

      .fail(function() {
        // console.info('foi, mas deu ruim');
        _$form.find('[data-msg-retorno] [data-msg-success]').removeClass('visible');
        _$form.find('[data-msg-retorno] [data-msg-error]').addClass('visible');
        _$form.find("button[type=submit]").text('Falha ao enviar');
        setTimeout(function() {
          _$form.find("button[type=submit]").text('Enviar');
        }, 3500);
        // store.setMsgResponse('Houve um problema ao enviar sua mensagem, tente novamente! Ou entre em contato conosco pelo telefone (21) 99644-6925 ', 'error', $form);
      })

      /* Após o envio ele remove a classe sending que permite que o form seja enviado novamente. */

      .always(function() {
        console.log("complete");
        _$form.removeClass('sending');
      });
    }
  }

```
{% endraw %}
