---
layout: default
title: B2B
parent: Recursos Extras
grand_parent: Recursos
---

# B2B
{: .no_toc }

***

**Na venda B2B, o cliente normalmente é outra empresa**

Comprando para seu consumo, para revenda ou como insumo para sua cadeia produtiva. São operações de venda atacadista.

***

**Na venda B2C o cliente normalmente é pessoa física**

São as operações tradicionais de varejo.

***

A grande maioria das lojas que desenvolvemos na Vnda tem o foco na operação B2C, porém,  conforme necessidade do cliente é possível adaptá-las das seguintes formas:

-  Somente operação B2C - padrão


-  Somente operação B2B - Gerenciando pedidos pelo Airtable (não finaliza os pedidos na plataforma)
-  Somente operação B2B - Gerenciando pedidos pelo Checkout


-  Operações B2C e B2B - Com catálogo gerenciando pedidos pelo Airtable (B2B) e Checkout (B2C)
-  Operações B2C e B2B - Gerenciando pedidos pelo Checkout no B2B (com preços especiais) e no B2C (preço normal)

***

## Nesse artigo
{: .no_toc .text-delta }

1. TOC
{:toc}

***

## Recursos úteis

- Variável global `logged_in`
- Objeto global `client`
- Filtro `client_price` no objeto `product`

***

## B2B - Gerenciando pedidos pelo Airtable

Ref: https://azizbrasil.vnda.com.br/ (essa é só B2B)

Alguns itens que foram feitos no desenvolvimento da funcionalidade nessa ref:

1. Adicionar condição no `layout.liquid` para mostrar a tela de login quando o/a usuário/a estiver deslogado/a usando `logged_in`
1. Na Aziz, os arquivos estão separados na pasta `b2b`
1. Criar form com o assunto `cadastro` no admin
1. Implementar popup de carrinho
1. Criar base no [Airtable](https://airtable.com/) a partir do Venda para Atacado
1. Pegar id do board do Airtable

***

## B2B - Gerenciando pedidos pelo Checkout

Utilize o filtro `client_price` para mostrar o preço especial.

Confira como criar uma [promoção para clientes atacado](http://ajuda.vnda.com.br/pt-BR/articles/4192800-promocao-para-clientes-atacado-no-b2b).

