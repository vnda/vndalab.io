---
layout: default
title: Recursos Extras
nav_order: 3
parent: Recursos
has_children: true
---


# Recursos Extras
{: .no_toc }

Exemplos de trechos de código de responsabilidade única que costumam ser reutilizados com frequência mas que não são tão críticos a ponto de precisarem fazer parte da estrutura padrão.
