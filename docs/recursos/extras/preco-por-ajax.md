---
layout: default
title: Preço por Ajax
parent: Recursos Extras
grand_parent: Recursos
---

# Preço por Ajax
{: .no_toc }

## Como funciona

Atualiza informações cacheadas de preço e disponibilidade dos produtos na listagem e/ou na interna de produto através de um request para o template `product_price.liquid`.

### product_price.liquid
{: .d-inline-block}

Liquid v2
{: .label .label-yellow }

É possível passar um parâmetro no request para retornar o html diferente para a listagem e para a interna.

{% raw %}
```liquid
{% if params.page == 'product' %}

  {% comment %}
    - Informações a serem atualizadas na interna de produto
  {% endcomment %}

{% else %}

  {% comment %}
    - Informações a serem atualizadas na listagem de produto / relacionados / etc
  {% endcomment %}

  {% if product.available %}
    <p class="price">
      {% if product.on_sale %}
        <del>{{ product.price | money_format }}</del>
        <ins>{{ product.sale_price | money_format }}</ins>
      {% else %}
        {{ product.price | money_format }}
      {% endif %}
    </p>
  {% else %}
    <p class="price">ESGOTADO</p>
  {% endif %}

{% endif %}
```
{% endraw %}

### product_price.liquid
{: .d-inline-block}

Liquid v4
{: .label .label-blue }

No Liquid 4 é necessário utilizar a tag **getparam**.

{% raw %}
```liquid
{% getparam 'page' as params_page %}
{% if params_page == 'product' %}

  [...]

{% else %}

  [...]

{% endif %}
```
{% endraw %}

### product

O objeto **product** no template `product_price.liquid`

```json
{
  "available":true,
  "on_sale":false,
  "price":90.0,
  "sale_price":90.0,
  "discount_rule":null,
  "installments":[
    {
      "number":1,
      "price":90.0,
      "interest":false,
      "interest_rate":0,
      "total":90.0
    },
    {
      "number":2,
      "price":45.0,
      "interest":true,
      "interest_rate":0.0,
      "total":90.0
    },
    {
      "number":3,
      "price":30.0,
      "interest":true,
      "interest_rate":0.0,
      "total":90.0
    }
  ],
  "variants":[
    {
      "sku":"01AF1",
      "price":90.0,
      "sale_price":90.0,
      "available":true,
      "properties":{
        "property1":{
          "name":"Tamanho",
          "value":"P",
          "defining":true
        }
      },
      "stock":9
    },
    {
      "sku":"01AF2",
      "price":90.0,
      "sale_price":90.0,
      "available":true,
      "properties":{
        "property1":{
          "name":"Tamanho",
          "value":"M",
          "defining":true
        }
      },
      "stock":3
    },
    {
      "sku":"01AF3",
      "price":90.0,
      "sale_price":90.0,
      "available":true,
      "properties":{
        "property1":{
          "name":"Tamanho",
          "value":"G",
          "defining":true
        }
      },
      "stock":1
    },
    {
      "sku":"01AF4",
      "price":90.0,
      "sale_price":90.0,
      "available":false,
      "properties":{
        "property1":{
          "name":"Tamanho",
          "value":"GG",
          "defining":true
        }
      },
      "stock":0
    }
  ]
}
```

### Request

| Method   | URL                 | Datatype | Response |
|:--------:|:-------------------:|:--------:|:--------:|
| **GET**  | `/produto/preco/id` |  `text`  | **200**  |


***


## Exemplo

### _product_block.liquid

{% raw %}
```liquid
[...]
  <span data-update-price='{{ product.id }}'><br /></span>
[...]
```
{% endraw %}

### product_price.liquid

{% raw %}
```liquid
{% if product.available %}
  <p class="price">
    {% if product.on_sale %}
      <del>{{ product.price | money_format }}</del>
      <ins>{{ product.sale_price | money_format }}</ins>
    {% else %}
      {{ product.price | money_format }}
    {% endif %}
  </p>
{% else %}
  <p class="price">ESGOTADO</p>
{% endif %}
```
{% endraw %}

### store.js

```javascript
var store = {
  config: {
    [...]
    priceProds: {
      selector: '[data-update-price]',
      attr: 'update-price',
    }
  },
  [...]
  getPriceProd: function () {

    var selector = store.config.priceProds.selector;
    var attr = store.config.priceProds.attr;

    if ($(selector).length > 0) {

      $(selector).each(function(index, value) {
        var $this = $(this);
        var prodId = $(this).data(attr);
        var url = '/produto/preco/' + prodId;

        if(prodId != '' && prodId != null) {
          $.ajax({
            url: url,
            type: 'GET',
          })
          .done(function(resp) {
            // console.info(resp);
            $this.html(resp);
          })
          .fail(function(resp) {
            console.error(resp);
          });
        }
      });

    }
  },
  [...]
}

var template_store = {
  [...]
  init: function () {
    [...]
    store.getPriceProd();
    [...]
  }
}
```
