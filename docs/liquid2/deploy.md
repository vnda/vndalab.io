---
layout: default
title: Processo de deploy
parent: Liquid 2
nav_order: 7
---

# Processo de deploy

O processo de deploy consiste em colocar no ar todas as mudanças que tiveram seu desenvolvimento concluído em Setup.
Atualmente, o processo é iniciado através de linha de comando, e sua execução transfere todos os arquivos modificados automaticamente, sem a necessidade de acesso FTP com transferencia manual.

Descrito abaixo está as instruções para o processo de deploy de lojas em Liquid 2. Para instruções sobre deploy de lojas em liquid 4, acessar a documentação de [Processo de deploy em Liquid 4]({%- link docs/liquid4/deploy.md -%}).

&nbsp;

## Configuração para deploy em lojas em Liquid 2 + S3

No arquivo `package.json`, o script de deploy deve ficar como no exemplo abaixo, porem com o nome da loja no lugar de `exemplo`:

***package.json***

```
"deploy": "aws s3 sync s3://vnda-liquid/vnda_stg_exemplo/ s3://vnda-liquid/vnda_prod_exemplo/ && aws s3 sync s3://vnda-assets-us/vnda_stg_exemplo/public/ s3://vnda-assets-us/exemplo/"
```
