---
layout: default
title: Controle de espaços em branco
parent: Básico
grand_parent: Liquid 2
nav_order: 4
---

# Controle de espaços em branco
{: .no_toc }


{% raw %}

No Liquid, você pode incluir um hífen na sintaxe de sua tag `{{-`, `-}}`, `{%-` e `-%}` para remover os espaços em branco do lado esquerdo ou direito de uma tag renderizada.

Normalmente, mesmo que não imprima texto, qualquer linha de Liquid em seu modelo ainda imprimirá uma linha em branco em seu HTML renderizado:

```liquid
{% assign my_variable = "tomate"%}
{{ my_variable }}
```

Observe a linha em branco antes de "tomate" no modelo renderizado:

```liquid

tomate
```

Ao incluir hifens em sua tag de atribuição, você pode retirar o espaço em branco gerado do modelo renderizado:

```liquid
{%- assign my_variable = "tomate" -%}
{{ my_variable }}
```

Resultado

```liquid
tomate
```

Se você não quiser que nenhuma de suas tags imprima espaços em branco, como regra geral, você pode adicionar hífens a ambos os lados de todas as suas tags (`{%-` e `-%}`):

```liquid
{% assign username = "John G. Chalmers-Smith" %}
{% se nome de usuário e tamanho de nome de usuário > 10 %}
  Uau, {{ username }}, você tem um nome comprido!
{% outro %}
  Olá!
{% fim se %}
```

Saída sem controle de espaço em branco

```liquid


  Uau, John G. Chalmers-Smith, você tem um nome comprido!
```

Agora adicionando os hífens:

```liquid
{%- assign username = "John G. Chalmers-Smith" -%}
{%- se nome de usuário e tamanho de nome de usuário> 10 -%}
  Uau, {{ username }}, você tem um nome comprido!
{%- outro -%}
  Olá!
{%- fim se -%}
```

Saída com controle de espaço em branco:

```liquid
Uau, John G. Chalmers-Smith, você tem um nome comprido!
```
{% endraw %}
