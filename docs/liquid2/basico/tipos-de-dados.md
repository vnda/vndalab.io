---
layout: default
title: Tipos de dados
parent: Básico
grand_parent: Liquid 2
nav_order: 3
---

# Tipos de dados
{: .no_toc }

Objetos podem ter um de cinco tipos. Você pode inicializar uma variável/objeto no Liquid com as tags [`assign`]({%- link docs/liquid2/tags/variaveis.md -%}#assign) e [`capture`]({%- link docs/liquid2/tags/variaveis.md -%}#capture).

***

## Nesse artigo
{: .no_toc .text-delta }

1. TOC
{:toc}

***

## String

Strings são declaradas ao delimitar o conteúdo de uma variável com aspas simples ou duplas:

{% raw %}
```liquid
{% assign brand = "Vnda" %}
```
{% endraw %}

***

## Número

Números podem ser inteiros (-1, 7, 14) ou racionais (0,25; 32; -1,4):

{% raw %}
```liquid
{% assign pie = 42 %}
{% assign pi = 3.14159 %}
```
{% endraw %}

***

## Booleano

Booleanos são `true` ou `false`. Não use aspas para definir um booleano:

{% raw %}
```liquid
{% assign yes = true %}
{% assign no = false %}
```
{% endraw %}

***

## nil

`nil` é um valor vazio especial que é retornado quando não há resultados para um objeto ou propriedade.

`nil` é considerado `false` em blocos `if` e `unless`.

No exemplo a seguir, considere que um produto não possui tags (ou seja, `product.tags_names` não possui valor). Ou seja, o Liquid não exibirá o texto:

{% raw %}
```liquid
{% if product.tag_names %}
  Esse produto tem tags.
{% endif %}
```
{% endraw %}

Se você usar um atributo ou objeto `nil`, ele não será renderizado na página:

<div class="code-example" markdown="1">
```
Tags: 
```
</div>
{% raw %}
```liquid
Tags: {{ product.tag_names }}
```
{% endraw %}

***

## Array

Arrays (ou vetores) são listas de variáveis de qualquer tipo.

### Acessando itens de um array
{: .no_toc }

Para acessar todos os itens de um array, você pode fazer um loop em um array usando as tags [for]({%- link docs/liquid2/tags/iteracao.md -%}#for) ou [tablerow]({%- link docs/liquid2/tags/iteracao.md -%}#tablerow):

<div class="code-example" markdown="1">
```
verao camisa amarelo promo
```
</div>

{% raw %}
```liquid
<!-- product.tag_names = "verao" "camisa" "amarelo" "promo" -->
{% for tag in product.tag_names %}
  {{ tag }}
{% endfor %}
```
{% endraw %}

### Acessando um item específico em um array
{: .no_toc }

<div class="code-example" markdown="1">
```
verao
```
</div>

{% raw %}
```liquid
<!-- product.tag_names = "verao" "camisa" "amarelo" "promo" -->

{{ product.tag_names[0] }}
```
{% endraw %}

### Inicializando arrays
{: .no_toc }

Você não pode iniciar arrays usando o Liquid. Mas você pode usar o filtro `split` para “quebrar” uma variável em um array com suas partes.
