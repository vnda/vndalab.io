---
layout: default
title: Básico
parent: Liquid 2
nav_order: 1
has_children: true
---

# Noções Básicas
{: .no_toc }


O Liquid usa uma combinação de **tags**, **objetos** e **filtros** para carregar conteúdo dinâmico. Eles são usados dentro de **arquivos de template `.liquid`**, que são os temas que compõem a estrutura de uma loja, veremos mais sobre isso a seguir.

***

## Conteúdos
{: .no_toc .text-delta }

1. TOC
{:toc}

***

## Tags

Tags são usadas para criar a lógica de exibição e controle de dados nos templates. Os delimitadores {% raw %}`{% %}`{% endraw %} e o texto que eles cercam não produz um resultado visível quando a página é renderizada no navegador, mas ela permite acessar objetos da plataforma e criar condições e repetições sem mostrar o código Liquid na página.

Por exemplo, você pode usar tags do Liquid para exibir um conteúdo diferente na página de produto se o produto estiver em promoção:

{% raw %}
```liquid
{% if product.on_sale %}
  De: {{ product.price | money_format }}
  Por: {{ product.sale_price | money_format }}
{% else %}
  Preço: {{ product.price | money_format }}
{% endif %}
```
{% endraw %}

Se o produto estiver em promoção, será exibido:

```
De: R$ 99,90
Por: R$ 79,90
```

Se o produto não estiver em promoção, será exibido:

```
Preço: R$ 99,90
```

O exemplo acima usa as tags `if` e `else`, que são chamadas de tags de controle.

Existem vários tipos de tags Liquid:

- [Tag de comentário]({%- link docs/liquid2/tags/comentario.md -%})
- [Tags de controle]({%- link docs/liquid2/tags/controle.md -%})
- [Tags de iteração]({%- link docs/liquid2/tags/iteracao.md -%})
- [Tag raw]({%- link docs/liquid2/tags/raw.md -%})
- [Tags de variáveis]({%- link docs/liquid2/tags/variaveis.md -%})
- [Tags de loja]({%- link docs/liquid2/tags/loja.md -%})

***

## Objetos

Objetos exibem pedaços de dados vindos de um admin de loja da Vnda. Em um template, objetos são identificados pelos delimitadores {% raw %}`{{ }}`{% endraw %}, e se parecem com isso:

{% raw %}
```liquid
{{ product.name }}
```
{% endraw %}

No exemplo acima **`product` é o objeto**, e **`name` é um atributo** desse objeto. Cada objeto tem uma lista de propriedades associadas a ele.

O  {% raw %}`{{ product.name }}`{% endraw %} pode ser usado no [template de produto]({%- link docs/liquid2/templates/product.md -%}), por exemplo. Quando o código for compilado e renderizado na página de produto de uma loja, o que será exibido no navegador é o nome do produto. Por exemplo, em uma loja de roupas, o resultado poderia ser

```liquid
Camiseta de verão
```

Confira os objetos que temos disponíveis para cada loja:

- [Objetos]({%- link docs/liquid2/objetos/index.md -%})

***

## Filtros

Filtros são usados para modificar a exibição de números, strings, objetos e variáveis. Eles são colocados em objetos {% raw %}`{{ }}`{% endraw %}, e são separados por uma barra vertical (`|`).

Um exemplo simples é o filtro `capitalize`:

{% raw %}
```liquid
{{ "olá, mundo!" | capitalize }}
```
{% endraw %}

O filtro vai modificar a string, fazendo ela começar com uma letra maiúscula. Na página renderizada, será exibido:

```
Olá, mundo!
```

Você pode usar vários filtros em um elemento. Eles serão aplicados da esquerda para a direita:

{% raw %}
```liquid
{{ "olá, mundo!" | capitalize | remove: "mundo" }}
```
{% endraw %}

A string “olá, mundo!” será iniciada com uma letra maiúscula, e então a palavra “mundo” será removida. O resultado será:

```
Olá, !
```


Você pode usar filtros para fazer várias manipulações de dados. Os filtros são categorizados da seguinte maneira:

- [Filtros nativos]({%- link docs/liquid2/filtros/nativos.md -%})
- [Filtros Vnda]({%- link docs/liquid2/filtros/index.md -%})

***

## Templates

As lojas da Vnda são feitas de arquivos de template Liquid, cada uma servindo a um propósito específico. Por exemplo, `tag.liquid` é usada para exibir uma coleção de produtos, enquanto `product.liquid` é usada para mostrar os detalhes de um produto só.

Os templates que compõem a estrutura do projeto de uma loja são:

- [account.liquid]({%- link docs/liquid2/templates/account.md -%})
- [cart_popup.liquid]({%- link docs/liquid2/templates/cart_popup.md -%})
- [home.liquid]({%- link docs/liquid2/templates/home.md -%})
- [layout.liquid]({%- link docs/liquid2/templates/layout.md -%})
- [login.liquid]({%- link docs/liquid2/templates/login.md -%})
- [page.liquid]({%- link docs/liquid2/templates/page.md -%})
- [product.liquid]({%- link docs/liquid2/templates/product.md -%})
- [search.liquid]({%- link docs/liquid2/templates/search.md -%})
- [tag.liquid]({%- link docs/liquid2/templates/tag.md -%})
