---
layout: default
title: Operadores
parent: Básico
grand_parent: Liquid 2
nav_order: 1
---

# Operadores
{: .no_toc }

O Liquid possui vários operadores lógicos e de comparação.

***

## Nesse artigo
{: .no_toc .text-delta }

1. TOC
{:toc}

***

## Operadores básicos

| `==`  | igual a          |
| `!=`  | diferente de     |
| `>`   | maior que        |
| `<`   | menor que        |
| `>=`  | maior ou igual a |
| `<=`  | menor ou igual a |
| `or`  | “ou” lógico      |
| `and` | “e” lógico       |

Por exemplo:

{% raw %}
```liquid
{% if product.name == "Camiseta de verão" %}
  Essa camiseta é perfeita para os dias quentes!
{% endif %}
```
{% endraw %}

Você pode usar vários operadores:

{% raw %}
```liquid
{% if product.on_sale and product.name == "Camiseta de verão" %}
  Aproveite a promoção dessa camiseta!
{% endfor %}
```
{% endraw %}

***

## contains

`contains` verifica se uma string possui uma palavra ou expressão:

{% raw %}
```liquid
{% if product.name contains "camiseta" %}
  O nome desse produto possui a palavra "camiseta".
{% endif %}
```
{% endraw %}

O `contains` também pode ser usado para verificar se existe uma string em um array de strings:

{% raw %}
```liquid
{% if product.tag_names contains "verao" %}
  Esse produto possui a tag "verão".
{% endif %}
```
{% endraw %}

`contains` só pode verificar strings. Você não pode usá-lo para procurar objetos em um array de objetos.

***

## Ordem dos operadores

Tags com mais de um operador `and` ou `or` são **lidas da direita para a esquerda**. Você **não pode usar parênteses** para manipular a ordem ds operadores — parênteses são caracteres inválidos no Liquid e vão gerar um erro.

{% raw %}
```liquid
{% if true or false and false %}
  Isso vai ser avaliado como `true`, porque a condição `and` é verificada antes.
{% endif %}
```

```liquid
{% if true and false and false or true %}
  Isso vai ser avaliado como `false`:

  true and (false and (false or true))
  true and (false and true)
  true and false
  false
{% endif %}
```
{% endraw %}

