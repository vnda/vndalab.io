---
layout: default
title: Verdadeiro e falso
parent: Básico
grand_parent: Liquid 2
nav_order: 2
---

# Verdadeiro e falso
{: .no_toc }

Quando um dado do tipo [não booleano]({%- link docs/liquid2/basico/tipos-de-dados.md -%}) é usado em um contexto booleano (como em [tags de controle]({%- link docs/liquid2/tags/controle.md -%})), o Liquid decide se vai considerá-lo como `true` ou `false`. Tipos de dados que retornam `true` são considerados **verdadeiros**, enquanto tipos de dados que retornal `false` são considerados **falsos**.

***

## Nesse artigo
{: .no_toc .text-delta }

1. TOC
{:toc}

***

## Verdadeiro

No Liquid, tudo é verdadeiro, menos `nil` ou `false`.

No exemplo abaixo, o texto “Vnda” não é um valor booleano, mas ele é verdadeiro em uma condição (porque ele existe e não é nulo [`nil`]).

{% raw %}
```liquid
{% assign brand = “Vnda” %}

{% if brand == true %}
  Esse texto sempre será exibido enquanto `brand` existir.
{% endif %}
```
{% endraw %}

Strings vazias são verdadeiras, porque elas foram definidas. O exemplo abaixo:

{% raw %}
```liquid
{% assign brand = "" %}

{% if brand %}
 <h1>{{ brand }}</h1>
{% endif %}
```
{% endraw %}

Irá exibir:

```html
<h1></h1>
```

Para evitar isso, você pode verificar se uma string está vazia:

{% raw %}
```liquid
{% unless brand == blank %}
  <h1>{{ brand }}</h1>
{% endunless %}
```
{% endraw %}

Agora, `brand` só será exibido se ele não estiver vazio.

***

## Falso

Os únicos valores falsos no Liquid são `nil` e `false`.

`nil` é retornado quando o Liquid não tem nada para retornar de um objeto. Por exemplo, se um produto não possui tags, `product.tag_names` retornará `nil`:

{% raw %}
```liquid
{% if product.tags %}
  Esse bloco só será exibido se `product.tags` não for `nil`.
{% endif %}
```
{% endraw %}

O valor `false` é retornado através de objetos Liquid ou variáveis definidas como `false` pelo desenvolvedor. Um exemplo de propriedade que pode retornar `false` é `product.available`. 
