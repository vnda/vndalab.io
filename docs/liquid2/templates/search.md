---
layout: default
title: search.liquid
parent: Templates
grand_parent: Liquid 2
nav_order: 5
---

# search.liquid
{: .no_toc }

## Conteúdos
{: .no_toc .text-delta }

1. TOC
{:toc}

---

{% raw %}

O template `search.liquid` é usado para exibir uma lista de produtos que correspondem a uma busca do cliente.

![Captura de tela da página de resultados de busca](https://gitlab.com/vnda/setup/guides/-/raw/master/Recursos/Screenshots/search.jpg)

## Listando os resultados com **{% load_products %}**

O template de busca funciona muito como o template [`tag.liquid`](setup/templates/tag.liquid), já que o [`{% load_products %}`](liquid/tags/loja#load_products) funciona de forma semelhante. A maior diferença é que, ao invés de passar um parâmetro `tag`, você deve passar os termos de busca no parâmetro `q`.


## Considerações do template

### Incluindo um campo de busca na loja

![Captura de tela do campo de busca](https://gitlab.com/vnda/setup/guides/-/raw/master/Recursos/Screenshots/search_form.png)

Para incluir um campo de busca na loja, você precisa criar um formulário HTML com o atributo `action` definido para `{{ search_url }}`. Nesse formulário, o campo de busca precisa ter o atributo `name` definido como `q`.

Um exemplo simples de um formulário de busca no cabeçalho de uma loja:

```liquid
<form method="get" action="{{ search_url }}">
  <label for="store-search">Buscar...</label>
  <input id="store-search" type="search" name="q" autocomplete="off" aria-label="Buscar produtos na loja" required />
  <button type="submit">OK</button>
</form>
```

### Filtrando, ordenando e paginando os resultados de busca

Como os resultados de busca são carregados através da tag [`{% load_products %}`](liquid/tags/loja#load_products), o código necessário para filtrar, ordenar e paginar o template [`tag.liquid`](setup/templates/tag.liquid) pode ser reutilizado.

Considere criar [partes reutilizáveis](setup/partes) para filtros, opções de ordenação e paginação para poder usar o mesmo código em ambos os templates.

Confira guias específicos:

- [Objeto `{{ aggregations }}`](liquid/objetos/aggregations), que permite criar filtros por atributos de produto e tipos de tag.
- [Objeto `{{ sort_options }}`](liquid/objetos/sort_options), para criar opções de ordenação dos resultados de busca.
- [Objeto `{{ pagination }}`](liquid/objetos/pagination), com exemplos de diferentes formas de exibir links de paginação.

{% endraw %}
