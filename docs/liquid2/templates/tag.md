---
layout: default
title: tag.liquid
parent: Templates
grand_parent: Liquid 2
nav_order: 4
---

# tag.liquid
{: .no_toc }

## Conteúdos
{: .no_toc .text-delta }

1. TOC
{:toc}

---

{% raw %}

O template **tag.liquid** é usado para renderizar uma página de tag. O principal objetivo dessa página é listar produtos em uma tag, geralmente através de um grid de imagens com os títulos e preços do produto.

![Captura de tela de uma página de tag](https://gitlab.com/vnda/setup/guides/-/raw/master/Recursos/Screenshots/tag.jpg)

## A tag **{% load_products %}**

A listagem de produtos por uma tag é feita através da tag [`{% load_products %}`](liquid/tags/loja#load_products). O `load_products` retorna um array de objetos `products`, que pode ser percorrido usando a tag [`{% for %}`](liquid/tags/iteração#for).

A tag `{% load_products %}` permite vários parâmetros de controle de ordenação, paginação e filtragem que podem ser definidos através de parâmetros de URL. [Veja mais informações na referência da tag `{% load_products %}`](liquid/tags/loja#load_products).

## Considerações do template

### Acessando informações da tag atual

O template dá acesso à um [objeto `{{ tag }}`](liquid/objetos/tag) que permite exibir informações da tag atual, como o título (`tag.title`), a imagem em destaque (`tag.image_url`) e descrição (`tag.description`).

Saiba mais conferindo a [referência do objeto `{{ tag }}`](liquid/objetos/tag).

### Opções de filtros

![Captura de tela dos filtros de uma página de tag](https://gitlab.com/vnda/setup/guides/-/raw/master/Recursos/Screenshots/tag_filters.jpg)

Você pode criar opções para filtrar a lista de produtos na tag através das variantes dos produtos sendo listados e de outras tags que esses produtos possuem, usando o [objeto `{{ aggregations }}`](liquid/objetos/aggregations).

Por exemplo, ao acessar a tag `camisetas`, o cliente pode filtrar a lista de produtos em um conjunto de produtos menores, querendo ver apenas as camisetas da coleção `verão`.

Para isso, a tag `verão` precisa ter um tipo `colecao`, que pode ser acessado através do objeto `{{ aggregations }}`, e o `load_products` precisa ser atualizado com o nome do parâmetro que esse filtro vai usar:

```liquid

{% if aggregations.types.colecao %}
<form method="get">
  <h3>Coleções</h3>

  {% for colecao in aggregations.types.colecao %}
    <p>
      <input name="collections[]" value="{{ colecao.name }}" id="colecao_{{ tag.name }}" />
      <label for="colecao_{{ tag.name }}">{{ tag.title }}</label>
    </p>
  {% endfor %}
</form>
{% endif %}

{% load_products tag.tag_name colecao_tags: params.collections %}

```

Confira mais exemplos de filtros na documentação do [objeto `{{ aggregations }}`](liquid/objetos/aggregations).

### Ordenando produtos em uma tag

Você pode ordenar produtos em uma tag passando o parâmetro `sort` na tag `{% load_products %}`. Para permitir que o cliente escolha como ordenar a lista de produtos da tag, você pode criar um `<select>` com o [objeto `{{ sort_options }}`](liquid/objetos/sort_options).

```liquid

<select name="sort_by">
  {% for sort_option in sort_options %}
    <option value="{{ sort_option.value }}"
      {% if params.sort_by == sort_option.value %}
        selected
      {% endif %}>
      {{ sort_option.label }}
    </option>
  {% endfor %}
</select>

```

Note que, sempre que você criar um filtro ou uma opção para a listagem de produtos, o nome do elemento do formulário precisa ser adicionado como parâmetro no `{% load_products %}`:

```liquid

{% load_products tag:tag.name sort:params.sort_by %}

```

Confira mais exemplos na documentação do [objeto `{{ sort_options }}`](liquid/objetos/sort_options)

### Paginação

Por padrão, o `{% load_products %}` exibe no máximo 25 produtos por vez. Você pode alterar esse valor através do parâmetro `per_page`:

```liquid

{% load_products tag:tag.name per_page:50 %}

```

Caso uma tag tenha mais que 50, por exemplo, será necessário exibir links de paginação para o cliente acessar os outros produtos. Você pode fazer isso com o [objeto `{{ pagination }}`](liquid/objetos/pagination).

```liquid

{% if pagination.total_pages > 1 %}
  <a href="{{ pagination.prev_url }}" rel="prev">Página anterior</a>

  <a href="{{ pagination.next_url }}" rel="next">Próxima página</a>
{% endif %}

```

Existem várias formas de exibir a paginação de uma lista de produtos. Confira a referência do [objeto `{{ pagination }}`](liquid/objetos/pagination) para mais exemplos.

{% endraw %}
