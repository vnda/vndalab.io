---
layout: default
title: account.liquid
parent: Templates
grand_parent: Liquid 2
nav_order: 6
---

# account.liquid
{: .no_toc }


O template `account.liquid` é usado para exibir a conta de cliente da loja.

![Captura de tela da página de Conta](https://gitlab.com/vnda/setup/guides/-/raw/master/Recursos/Screenshots/account.png)

Toda a estrutura da página de conta do usuário é trazida pela plataforma através do código padrão presente na `estrutura_padrao`.


## Opções de personalização

```
var widget = new Vnda.AccountWidget({
  spacing: 0.4,
  fontFamily: 'Space Mono, monospace',
  borderRadius: 0,
  palette: {
    primary1Color: '#E0AF1F',
    primary2Color: 'red',
    primary3Color: 'red',
    accent1Color: 'red',
    accent2Color: 'red',
    accent3Color: 'red',
    textColor: 'red',
    secondaryTextColor: 'red',
    alternateTextColor: 'red',
    canvasColor: 'red',
    borderColor: 'red',
    disabledColor: 'red',
    pickerHeaderColor: 'red',
    clockCircleColor: 'red',
    shadowColor: 'red'
  }
});
```
