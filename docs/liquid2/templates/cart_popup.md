---
layout: default
title: cart_popup.liquid
parent: Templates
grand_parent: Liquid 2
nav_order: 7
---

# cart_popup.liquid
{: .no_toc }

## Conteúdos
{: .no_toc .text-delta }

1. TOC
{:toc}

---

{% raw %}

O template `cart_popup.liquid` oferece uma lista de produtos adicionados ao carrinho de compras pelo cliente.

![Captura de tela da popup de carrinho](https://gitlab.com/vnda/setup/guides/-/raw/master/Recursos/Screenshots/cart_popup.png)

## Considerações do template

Diferente de outros templates, o `cart_popup.liquid` não pode ser acessado diretamente por uma URL. É preciso chamá-lo por javascript em um request para `/carrinho/popup`. Quando um cliente adiciona ou remove um produto do carrinho, a loja recarrega o conteúdo para atualizar a lista de produtos.

### Entendendo os itens no carrinho

Os produtos no carrinho de compras podem ser acessados no objeto `{{ cart.items }}`.

```liquid
{% for item in cart.items %}
  <div class="item"
       data-item-id="{{ item.id }}" 
       data-item-product-id="{{ item.product_id }}" 
       data-item-product-sku='{{ item.variant_sku }}' 
       data-item-quantity="{{ item.quantity }}" 
       data-product-reference="{{ item.product_reference }}" 
       data-item-cart>

    <!-- exibição do {{ item }} aqui -->

  </div>
{% endfor %}
```
No exemplo, o contâiner de item no carrinho possui atributos `data` com informações do item. Esses atributos permitem que a quantidade do item seja manipulada dinamicamente na popup de carrinho.

Cada [`{{ item }}`](liquid/objetos/item) oferece informações sobre a <abbr title="As versões diferentes de um produto, como cor ou tamanho">variante</abbr> de um produto que foi adicionada ao carrinho, e haverá um `{{ item }}` diferente para cada variante que foi adicionada. Por exemplo, se um cliente adiciona `camisa` com `tamanho: M` ao carrinho, e adiciona outra `camisa` com `tamanho: G`, haverá dois itens diferentes: um para a camisa M e outro para a camisa G.


### Indo para o carrinho

O `cart_popup.liquid` não finaliza uma compra, mas pode levar o cliente para a página de carrinho/checkout com `{{ cart_url }}`:

```liquid
<a href="{{ cart_url }}">
  Finalizar compra
</a>
```
{% endraw %}
