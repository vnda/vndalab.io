---
layout: default
title: product.liquid
parent: Templates
grand_parent: Liquid 2
nav_order: 3
---

# product.liquid
{: .no_toc }

O `product.liquid` renderiza uma página com detalhes de um produto individual. Ele inclui um `<form>` HTML que clientes usam para selecionar uma <abbr title="As diferentes versões de um produto, como opções de cores e tamanhos">variante</abbr> e adicioná-la ao carrinho. O template `product.liquid` oferece um objeto global [{% raw %}`{{ product }}`{% endraw %}]({%- link docs/liquid2/objetos/product.md -%}).

  ***

## Nesse artigo
{: .no_toc .text-delta }

1. TOC
{:toc}

***

![Captura de tela da página de produto](https://gitlab.com/vnda/setup/guides/-/raw/master/Recursos/Screenshots/product.jpg)


## Construindo o formulário de compra

Para adicionar itens ao carrinho, é preciso fornecer o SKU da variante de produto selecionada e a quantidade dentro de um `<form>`. O formulário deve encaminhar essas informações para {% raw %}`{{ cart_item_url }}`{% endraw %}.

A `estrutura_padrao` já possui o código necessário para o funcionamento do formulário de compra.


Existem algumas informações que podem ser enviadas no formulário de produto:

| Informação             | Nome do atributo             | Obrigatório |
|------------------------|------------------------------|-------------|
| SKU da variante        | `sku`                        | Sim         |
| Quantidade             | `quantity`                   | Sim         |
| Atributo personalizado | `extra[<Nome do atributo>]`  | Não         |

### Selecionando uma variante de produto
{: .no_toc }

Para permitir que clientes selecionem a variante de um produto para adicioná-la ao carrinho, você precisa de um elemento no formulário com o atributo `name=sku`. Nesse elemento, o valor deve ser o SKU da variante ({% raw %}`{{ variant.sku }}`{% endraw %}) selecionada.


### Selecionando a quantidade
{: .no_toc }

É necessário informar a quantidade no formulário de produto através de um elemento com nome `quantity`. Você pode querer disponibilizar um campo para o cliente alterar a quantidade, ou deixá-lo oculto com um `input type="hidden"`.

O tipo de `input` que você disponibiliza para o usuário não influencia o comportamento do carrinho propriamente, mas para adicionar itens no carrinho o valor do campo `quantity` deve ser um número inteiro.

### Suporte à popup de carrinho
{: .no_toc }

Por padrão o cliente será redirecionado para a página `/carrinho` ao clicar no botão de _Adicionar ao carrinho_. Porém, a loja já possui a funcionalidade de exibir a popup de carrinho atualizada ao invés de redirecionar o cliente.

Para ativar a popup de carrinho abra o arquivo `store.js` e mude o `store.config.addToCartOpenCartpopup` (e `store.config.addToCartOpenCartpopupMobile` se você quiser esse recurso no celular).

```js
var store = {
  config: {
    [...]
    addToCartOpenCartpopup: true,
    addToCartOpenCartpopupMobile: true,
    [...]
  }
}
```

Confira se possui o seletor `data-cart-body` para ser populado:

{% raw %}
```liquid
<div class="cart-body" id="cart-body" data-cart-body></div>
```
{% endraw %}

***

## Considerações do template

### Descrição do produto

É comum que páginas de produtos dividam a descrição do produto em várias seções como Descrição, Composição e Medidas, por exemplo. É possível filtrar o conteúdo da descrição do produto (o {% raw %}`{{ product.description }}`{% endraw %}) para exibí-lo em diferentes partes do template quando necessário.


### Variantes e `variants.js`

Um produto pode ter até três atributos. Por exemplo, uma camiseta pode ter as opções Tamanho, Cor e Material. Nesses cenários, é recomendado que você use o script padrão `variants.js` e a estrutura de variantes presente na `estrutura_padrao` para exibir e dinamizar cada opção.

![Captura de tela da página de produto com mais de uma variação](https://gitlab.com/vnda/setup/guides/-/raw/master/Recursos/Screenshots/product_variants.png)

A estrutura e o script montam as opções de compra que permitem que o cliente escolha opções disponíveis entre esses três atributos separadamente, e evitam que o cliente faça o pedido de uma variação de produto que não existe ou que não está disponível. Ele também permite atualizar a galeria de imagens do produto para exibir imagens relacionadas à variante selecionada.


### Avise-me quando chegar

O recurso de Avise-me quando chegar (ou Lista de Espera) é um recurso da plataforma que notifica o cliente sempre que um produto esgotado voltar ao estoque.

O cadastro é feito através de formulário para a url `/lista_de_espera` (com o método `POST`).

Essas são as informações que podem ser enviadas nesse formulário:

<div class="code-example" markdown="1">

  | Informação             | Nome do atributo             | Obrigatório |
  |------------------------|------------------------------|-------------|
  | SKU da variante        | `sku`                        | Sim         |
  | Email do cliente       | `email`                      | Sim         |
  | Chave do formulário criado no admin | `key`           | Sim         |
  | Referência do produto  | `referencia`                 | Não         |
  | URL do produto         | `referencia`                 | Não         |
  | Imagem da variante     | `imagemvariante`             | Não         |
  | Nome do produto        | `nomeproduto`                | Não         |
  | Preço                  | `preco`                      | Não         |

</div>

Lojas baseadas nas versões mais recentes da `estrutura_padrao` já possuem o código necessário para esse recurso funcionar, que consiste em:

1. Verificar se a variante selecionada possui estoque ([`variants.init()`](https://gitlab.com/vnda/setup/estrutura_padrao/blob/master/assets/javascripts/variants.js#L467));
2. Atualizar o formulário com o SKU da variante sem estoque que foi selecionada ([`variants.setFormNotify()`](https://gitlab.com/vnda/setup/estrutura_padrao/blob/master/assets/javascripts/variants.js#L313));
3. Exibir o formulário para o cliente;
4. Enviar o formulário e receber a resposta da API ([`template_store.submitFormNotify()`](https://gitlab.com/vnda/setup/estrutura_padrao/blob/master/assets/javascripts/store.js#L545)).

É preciso criar um formulário através da plataforma para gerar a chave do formulário (`key`).

**Nota:** Para esse recurso funcionar, a loja deve possuir um token de Lista de Espera cadastrado. Essa configuração é realizada pela equipe de atendimento.
