---
layout: default
title: login.liquid
parent: Templates
grand_parent: Liquid 2
nav_order: 8
---

# login.liquid
{: .no_toc }

## Conteúdos
{: .no_toc .text-delta }

1. TOC
{:toc}

---

{% raw %}

O template `login.liquid` é usado por clientes para iniciar sessão, recuperar senha ou criar uma conta na loja. Esse template consiste em três formulários HTML diferentes, um para cada ação. Os três formulários devem usar o método `post`.

![Captura de tela da página de login](https://gitlab.com/vnda/setup/guides/-/raw/master/Recursos/Screenshots/login.png)

## Considerações do template

### Criando um link para a página de login

Para criar um link para a página de login, você precisa conferir se um cliente já está logado na loja ou não.

```liquid
{% if logged_in %}
  <a href="{{ account_url }}">Olá, {{ client.first_name }}!</a>
  <a href="{{ logout_url }}">Sair</a>
{% else %}
  <a href="{{ login_url }}">Entrar</a>
{% endif %}
```

O exemplo acima possui os seguintes elementos:

- `logged_in`: é `true` se o cliente está logado, ou `false` se ele não possui uma conta.
- `account_url`, `login_url` e `logout_url` são os objetos que são renderizados como URLs automaticamente.
- `{{ client.first_name }}` exibe o primeiro nome cadastrado no cliente. Saiba mais sobre o [objeto `{{ client }}`](liquid/objetos/client) para mais informações.

### Exibindo o formulário de login

![Captura de tela do formulário de login](https://gitlab.com/vnda/setup/guides/-/raw/master/Recursos/Screenshots/login_login.png)

O formulário de login é um `form` HTML com os seguintes campos:

- um campo do tipo `email` com o nome `email`.
- um campo do tipo `password` com o nome `password`.
- um campo do tipo `hidden` com o nome `_method` e o valor `put`.
- um campo do tipo `hidden` com o nome `redirect_to` e o valor `account`.

O formulário deve apontar para `{{ login_url }}`. Abaixo você pode conferir um exemplo de um formulário de login simples:

```html
<form method="post" action="{{ login_url }}">
  <p>
    <label for="customer-email">Endereço de email:</label>
    <input id="customer-email" type="email" name="email" />
  </p>
   <p>
    <label for="customer-password">Senha:</label>
    <input id="customer-password" type="password" name="password" />
  </p>
  
  <p>
    <input type="hidden" name="_method" value="put" />
    <input type="hidden" name="redirect_to" value="account" />
    
    <button type="submit">
      Entrar
    </button>
  </p>
</form>
```

### Oferecendo um formulário de recuperar senha

![Captura de tela do formulário de recuperar senha](https://gitlab.com/vnda/setup/guides/-/raw/master/Recursos/Screenshots/login_recover-password.png)

Bem como o formulário de login, o formulário de recuperação de senha é um `<form>` HTML que deve ser apontado para `{{ recover_password_url }}` com:
  
- um campo do tipo `email` com o nome `email`.

```html
<form method="post" action="{{ recover_password_url }}">
  <p>
    <label for="recover-password-email">Endereço de email:</label>
    <input id="recover-password-email" type="email" name="email" />
  </p>
  
  <p>
    <button type="submit">
      Recuperar senha
    </button>
  </p>
</form>
```

### Exibindo o formulário de registro

![Captura de tela do formulário de criar conta](https://gitlab.com/vnda/setup/guides/-/raw/master/Recursos/Screenshots/login_create-account.png)

Para exibir um formulário que os visitantes possam criar uma conta de cliente da loja, crie o `<form>` HTL com o `action` apontado para `{{ login_url }}` e os seguintes elementos:
  
- dois campos do tipo `text` com os nomes `first_name` e `last_name`.
- um campo do tipo `email` com o nome `email`.
- dois campos do tipo `password` com os nomes `password` e `password_confirmation`.
- um campo com nome `terms`, podendo ser do tipo `checkbox` ou `hidden` e o valor `true`.
- um campo do tipo `hidden` com o nome `new_client[terms]` e o valor `on`.
- um campo do tipo `hidden` com o nome `redirect_to` e o valor `account`.

```html
<form method="post" action="{{ login_url }}">
  <div>
    <p>
      <label for="register-first-name">Nome:</label>
      <input id="register-first-name" type="text" name="first_name" />
    </p>
    <p>
      <label for="register-last-name">Sobrenome:</label>
      <input id="register-last-name" type="text" name="last_name" />
    </p>
  </div>
  
  <p>
    <label for="register-email">Endereço de email:</label>
    <input id="register-email" type="email" name="email" />
  </p>
  
  <div>
    <p>
      <label for="register-password">Senha:</label>
      <input id="register-password" type="password" name="password" />
    </p>
    <p>
      <label for="register-password-confirmation">Confirme sua senha:</label>
      <input id="register-password-confirmation" type="password" name="password_confirmation" />
    </p>
  </div>
  
  <p>
    <input id="register-terms" type="checkbox" value="true" />
    <label for="register-terms">Aceito os <a href="#">Termos</a>.
  </p>

  <p>
    <input type="hidden" name="new_client[terms]" value="on" />
    <input type="hidden" name="redirect_to" value="account" />
    
    <button type="submit">
      Criar conta
    </button>
  </p>
</form>
```

{% endraw %}
