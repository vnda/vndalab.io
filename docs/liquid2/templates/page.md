---
layout: default
title: page.liquid
parent: Templates
grand_parent: Liquid 2
nav_order: 9
---

# page.liquid
{: .no_toc }

## Conteúdos
{: .no_toc .text-delta }

1. TOC
{:toc}

---

{% raw %}

O template `page.liquid` renderiza as páginas personalizadas que podem ser criadas tanto pelo desenvolvedor quanto pelos gestores da loja.

![Captura de tela de uma página personalizada](https://gitlab.com/vnda/setup/guides/-/raw/master/Recursos/Screenshots/page.jpg)

## Considerações do template

### O objeto **{{ page }}**

O template `page.liquid` oferece um [objeto `{{ page }}`](liquid/objetos/page) com as informações cadastradas na página através da plataforma, como `{{ page.title }}`, `{{ page.slug }}` e `{{ page.body }}`, que retorna o HTML do corpo da página inserido no editor da plataforma.


### Habilitando a página no admin

- No painel da loja clique no menu `Páginas`.
- Clique em criar página.
- No campo `Título` coloque o nome da página.
- No campo `URL` coloque como deverá ser o endereço da página (esse é o page slug que será utilizado no if do page.liquid).
- A `Descrição` será utilizada no SEO.

***
### Lojas antigas que utilizam páginas em HTML

- Muitas lojas antigas utilizam arquivos HTML em Stg para o conteúdo das páginas.
- Se esse for o caso, na raiz da pasta no Dropbox haverá uma pasta `Pages`.
- Dentro dela estarão os arquivos HTML da loja.
- Se necessário fazer alguma alteração basta alterá-los diretamente. 
- Se for criar uma nova página, primeiro crie ela no admin e depois crie um arquivo com o nome  do Slug escolhido.

***
### Estrutura do Liquid

Para lojas mais novas são utilizados por padrão arquivos Liquid.

Você pode criar templates específicos para determinadas páginas usando o `page.slug` para chamar uma parte.

```liquid
{% if page.slug == "sobre" %}
  <!-- incluindo uma estrutura específica para a página `sobre`.
  ...
{% else %}
  <!-- estrutura padrão de uma página personalizada -->
  {{ page.body }}
{% endif %}
```

### Dinamizando uma página com banners

Você pode criar uma estrutura para uma página através de banners. Isso é útil quando você quer criar uma estrutura complexa para uma página e ainda assim mantê-la dinâmica para o gestor da loja poder editá-la por conta própria. Isso funciona de forma semelhante ao template `home.liquid`, onde você cria “esperas” de conteúdo usando a tag para banners. Se você quer dinamizar todas as páginas com banners, você pode fazer o nome do banner usando o slug da página.

{% endraw %}
