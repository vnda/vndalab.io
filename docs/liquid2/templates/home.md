---
layout: default
title: home.liquid
parent: Templates
grand_parent: Liquid 2
nav_order: 2
---

# home.liquid
{: .no_toc }


O template `home.liquid` renderiza a página inicial da loja. Esse template é carregado quando você acessa a raíz (`/`) da loja.

![Captura de tela da página inicial](https://gitlab.com/vnda/setup/guides/-/raw/master/Recursos/Screenshots/home.jpg)

## Considerações do template

Para exibir conteúdo dinâmico na página, o `home.liquid` geralmente é montado através de blocos de banners e listagens de produtos.


### Conteúdo dinâmico na página inicial

As seções personalizadas da página inicial são dinamizadas através de banners, que podem exibir imagens e textos que são controlados pelos gestores da loja, sem a necessidade do desenvolvedor front trocar imagens e textos manualmente.
