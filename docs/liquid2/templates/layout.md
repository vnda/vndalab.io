---
layout: default
title: layout.liquid
parent: Templates
grand_parent: Liquid 2
nav_order: 1
---

# layout.liquid
{: .no_toc }

## Conteúdos
{: .no_toc .text-delta }

1. TOC
{:toc}

---

{% raw %}

Você pode pensar no `layout.liquid` como o _template mestre_.  Todos os outros templates serão renderizados dentro do `layout.liquid`. Todos os elementos que se repetem na estrutura da loja (ex.: cabeçalho, rodapé, menus de navegação, o bloco `<head>`) devem ser colocados dentro do `layout.liquid`.

A estrutura do `layout.liquid` pode ser simplificada para em algo como:

```liquid
<!DOCTYPE html>
<html>
  <head>
    {{ content_for_head }}
  </head>

  <body>
    {% include "partials/common/header" %}
    
    <main class="site-content">
      {{ yeld }}
    </main>

    {% include "partials/common/footer %}

    {{ content_for_body }}
  </body>
</html>
```

## Considerações do template

### Cabeçalho e rodapé

As partes comuns entre todas as páginas, como o cabeçalho e o rodapé, são incluídos diretamente no template `layout.liquid`. Caso algum template use variações de cabeçalho e rodapé, ou se existe uma seção específica que deve ser exibida ou escondida dependendo do template sendo renderizado, você pode controlar a exibição criando uma condição com o objeto `template`.

```liquid

{% if template == "home" %}
  {% include "partials/page/header_home %}
{% else %}
  {% include "partials/common/header" %}
{% endif %}

```
{% endraw %}

Dessa forma você também pode especificar outras partes, folhas de estilo específicas e scripts para cada template.

## Nota sobre estilos **inline**

Para melhorar a performance da loja, os arquivos CSS são carregados de maneiras distintas. Páginas como a home e a página de produto, que possuem muitos estilos, são carregadas primeiro os estilos _inline_ antes da dobra da página (aquilo que está visível no navegador quando o usuário acessa a página) e depois o arquivo CSS com o restante dos estilos.


