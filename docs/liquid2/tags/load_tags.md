---
layout: default
title: "load_tags e load_tag"
parent: Tags
grand_parent: Liquid 2
nav_order: 8
---

# load_tags e load_tag
{: .no_toc }

{% raw %}`{% load_tags %}`{% endraw %} e {% raw %}`{% load_tag %}`{% endraw %} são usadas para obter as informações de tags de produto que respeitam um determinado parâmetro. Assim como a [tag {% raw %}`{% load_products %}`{% endraw %}]({% link docs/liquid2/tags/load_products.md %}), essas tags não possuem um delimitador final.

O retorno da tag {% raw %}`{% load_tags %}`{% endraw %} é um objeto {% raw %}`{{ loaded_tags }}`{% endraw %} contendo todas as tags de produto que respeitam o parâmetro passado pela tag, com suas respectivas informações.

O retorno da tag {% raw %}`{% load_tag %}`{% endraw %} é um objeto {% raw %}`{{ loaded_tag }}`{% endraw %} que possui a tag de produto indicada através dos parâmetros.

<div class="code-example" markdown="1">
```
Categorias:

Camisetas
Calças
Vestidos
Bermudas
```
</div>

{% raw %}
```liquid
{% load_tags types:'categoria' %}

Categorias:

{% for tag in loaded_tags %}
  {{ tag.title }}
{% endfor %}
```
{% endraw %}

***

## Nesse artigo
{: .no_toc .text-delta }

1. TOC
{:toc}

***

## Parâmetros da tag load_tags

É preciso indicar um tipo de tag ou um nome para que a tag {% raw %}`{% load_tags %}`{% endraw %} retorne as tags de produto desejadas.

### names
{: .no_toc }

O parâmetro `names` deve receber uma string com o nome ou a lista de nomes de tags de produto que devem ser retornados.

#### Exemplo
{: .no_toc .text-delta }

{% raw %}
```liquid
{% load_tags names:'home,carrossel' %}
```
{% endraw %}

***

### types
{: .no_toc }

O parâmetro `types` pode receber uma string com o tipo ou uma lista de tipos de tags de produto que devem ser retornados.

#### Exemplo
{: .no_toc .text-delta }

{% raw %}
```liquid
{% load_tags types:'destaque1,destaque2,destaque3' %}
```
{% endraw %}

***

## Parâmetros da tag load_tag

É preciso indicar qual tag de produto deve ser retornado pela {% raw %}`{% load_tag %}`{% endraw %} através dos parâmetros `name` e `type`.

### name
{: .no_toc }

O parâmetro `name` deve receber uma string com o nome identificador da tag de produto.

#### Exemplo
{: .no_toc .text-delta }

{% raw %}
```liquid
{% load_tag name:'destaque' %}
```
{% endraw %}

***

### type
{: .no_toc }

O parâmetro `type` recebe uma string com o tipo da tag de produto que deve ser retornada no objeto {% raw %}`{{ loaded_tag }}`{% endraw %}.

#### Exemplo
{: .no_toc .text-delta }

{% raw %}
```liquid
{% load_tag type:'home' %}
```
{% endraw %}

***

## Os objetos {% raw %}`{{ loaded_tags }}` e `{{ loaded_tag }}`{% endraw %}

O objeto {% raw %}`{{ loaded_tag }}`{% endraw %} é uma cópia do [objeto {% raw %}`{{ tag }}`{% endraw %}]({% link docs/liquid2/objetos/tag.md %}), com os mesmos atributos `name`, `title`, `subtitle`, `image_url`, etc.

O objeto {% raw %}`{{ loaded_tags }}`{% endraw %} é um array de vários objetos {% raw %}`{{ loaded_tag }}`{% endraw %}, que podem ser acessados através de uma [tag `for`]({% link docs/liquid2/tags/iteracao.md %}#for):

### Exemplo
{: .no_toc .text-delta}

{% raw %}
```liquid
{% for tag in loaded_tags %}
  <h2>{{ tag.title }}</h2>
  <p>{{ tag.description }}</p>
{% endfor %}
```
{% endraw %}

Para mais informações sobre a composição dos objetos {% raw %}`{{ loaded_tag }}`{% endraw %} e do array de objetos {% raw %}`{{ loaded_tags }}`{% endraw %}, confira a documentação do objeto [{% raw %}`{{ tag }}`{% endraw %}]({% link docs/liquid2/objetos/tag.md %}).

***

## Considerações

### Ordenando as tags do objeto {% raw %}`{{ loaded_tags }}`{% endraw %}

É possível manipular a ordem das tags de produto presentes no objeto {% raw %}`{{ loaded_tags }}`{% endraw %} usando o filtro `sort`:

#### home.liquid
{: .no_toc .text-delta }

<div class="code-example" markdown="1">
  ```html
  <!-- tags type `home`: ["mais-vendidos", "destaques", "nossas-escolhas"] -->
  
  Seções da Home:
  
  Produtos em Destaque
  Nossas Escolhas
  Mais Vendidos
  ```
</div>
{% raw %}
```liquid
{% load_tags types:'home' %}

{% assign sorted_tags = loaded_tags | sort: "subtitle" %}

Seções da Home:

{% for tag in sorted_tags %}
  {{ tag.title }}
{% endfor %}
```
{% endraw %}

No exemplo acima, o campo subtítulo da tag de produto do tipo `home` é usada para controlar a ordem de exibição das tags de acordo com o que foi informado pelo usuário no admin da loja.





