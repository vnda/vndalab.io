---
layout: default
title: Raw
parent: Tags
grand_parent: Liquid 2
nav_order: 4
---


# Raw

Raw desativa temporariamente o processamento de tags. Isso é útil para gerar conteúdo (por exemplo, Mustache, Handlebars) que usa sintaxe conflitante.

Por exemplo:

```liquid
{ {{ "% raw %" }} }{% raw %}
  In Handlebars, {{ this }} will be HTML-escaped, but
  {{{ that }}} will not.
{% endraw %}{ {{ "% endraw %" }} }
```

Resultará em:

{% raw %}
```
In Handlebars, {{ this }} will be HTML-escaped, but {{{ that }}} will not.
```
{% endraw %}
