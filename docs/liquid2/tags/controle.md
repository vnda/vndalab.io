---
layout: default
title: Controle
parent: Tags
grand_parent: Liquid 2
nav_order: 2
---

# Tags de Controle
{: .no_toc }

Tags de controle criam condições que decidem se blocos de código são executados.

***

## Nesse artigo
{: .no_toc .text-delta }

1. TOC
{:toc}

***

## if

Executa um bloco de código apenas se uma condição for atendida (se o resultado for `true`).

<div class="code-example" markdown="1">
```
Esse produto é ótimo num dia quente
```
</div>

{% raw %}
```liquid
<!-- product.tag_names == ["verao", "camisa", "amarelo", "promo"] -->

{% if product.tag_names contains "verao" %}
  Esse produto é ótimo para um dia quente.
{% endif %}
```
{% endraw %}

***

## unless

Como `if`, mas executa um código apenas se uma condição **não** for atendida (se o resultado dela for `false`).

<div class="code-example" markdown="1">
```
Esse produto não está disponível.
```
</div>

{% raw %}
```liquid
{% unless product.available %}
  Esse produto não está disponível.
{% endunless %}
```
{% endraw %}

O exemplo acima é o mesmo que:

{% raw %}
```liquid
{% if product.available != true %}
  Esse produto não está disponível.
{% endif %}
```
{% endraw %}

***

## else/elsif

Adiciona mais uma condição aos blocos `if` e `unless`.

<div class="code-example" markdown="1">
```
Esse produto é ótimo num dia frio.
```
</div>

{% raw %}
```liquid
<!-- product.tag_names == ["inverno", "calça", "moletom", "promo"] -->

{% if product.tag_names contains "verao" %}
  Esse produto é ótimo num dia quente.
{% elsif product.tag_names contains "inverno" %}
  Esse produto é ótimo num dia frio.
{% else %}
  Esse produto é ótimo em qualquer dia.
{% endif %}
```
{% endraw %}

***

## case/when

Cria uma série de condições para executar um bloco de código quando a variável especificada possuir um valor específico. `case` inicia a instrução, e `when` define as várias condições.

Você também pode adicionar uma tag `else` no fim do `case` que oferece um código para ser executado caso nenhuma condição tenha sido atendida.

{% raw %}
```liquid
{% case tag.name %}
  {% when "verao" %}
    Produtos para dias quentes.
  {% when "inverno" %}
    Produtos para dias frios.
  {% else %}
    Produtos para todos os dias.
{% endcase %}
```
{% endraw %}

***

## Múltiplas condições (and/or)

Você pode usar os [operadores]({%- link docs/liquid2/basico/operadores.md -%}) `and` e `or` para incluir mais de uma condição em uma tag de controle. `and` e `or` podem ser usadas juntas para criar condições complexas.

Se você usar múltiplos operadores `and` e `or`, observe que as condições `and` serão avaliadas primeiro, e depois `or`. Você não pode usar parênteses para controlar a ordem dos operadores. Parênteses são caracteres inválidos no Liquid e farão suas tags não funcionarem.

### and
{: .no_toc }

O operador `and` permite que você adicione mais condições à sua tag. Uma condição com `and` só será verdadeira se **ambos** os lados de uma condição sejam verdadeiros.

{% raw %}
```liquid
{% if product.tag_names == "verao" and product.name contains "calça" %}
  Pode parecer estranho, mas essa calça é boa para o verão!
{% endif %}
```
{% endraw %}

### or
{: .no_toc }

O operador `or` permite que você adicione mais condições à sua tag. Uma condição com `or` será verdadeira se **qualquer um** dos lados da codição é verdadeiro.

{% raw %}
```liquid
{% if product.tag_names == "verao" or product.name contains "inverno" %}
  Você está vendo um produto de estação, aproveite os preços especiais da estação!
{% endif %}
```
{% endraw %}
