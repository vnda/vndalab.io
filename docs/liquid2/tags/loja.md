---
layout: default
title: Loja
parent: Tags
grand_parent: Liquid 2
nav_order: 6
---

# Tags de Loja
{: .no_toc }

Tags de loja possuem várias funções, inclusive:

- Percorrer objetos oferecidos pela plataforma.
- Acessar dados de produtos específicos.
- Listar produtos a partir de parâmetros.

***

## Nesse artigo
{: .no_toc .text-delta }

1. TOC
{:toc}

***

## load_products
{: .d-inline-block}

A tag {% raw %}`{% load_products %}`{% endraw %}, que controla a listagem de produtos e a exibição de produtos em templates como [tag.liquid]({%- link docs/liquid2/templates/tag.md -%}) e [search.liquid]({%- link docs/liquid2/templates/search.md -%}), tem uma página dedicada à sua documentação [aqui]({%- link docs/liquid2/tags/load_products.md -%}).

## load_tags e load_tag
{: .d-inline-block}

Existe uma página dedicada à documentação dessas tags [aqui]({%- link docs/liquid2/tags/load_tags.md -%}).


## load_posts e load_post
{: .d-inline-block}

Retorna na variável `{% raw %}{{ loaded_posts }}{% endraw %}` um array com os objetos dos posts do Instagram.

Obs.: em staging utilizamos placeholder no desenvolvimento pois o Instagram geralmente é configurado apenas no ambiente de produção pela equipe de Atendimento/CS.

```liquid
{% raw %}
  {% load_posts from: "instagram" %}

  {% for post in loaded_posts %}
    {{ post }}
  {% endfor %}
{% endraw %}
```

## load_locals
{: .d-inline-block}

Retorna na variável `{% raw %}{{ loaded_locals }}{% endraw %}` um array com os objetos dos locais cadastrados na loja no **Admin > Configurações > Locais**.

```liquid
{% raw %}
  {% load_locals %}

  {% for local in loaded_locals %}
    {{ local }}
  {% endfor %}
{% endraw %}
```

### Agentes

Para retornar os agentes de cada local

```liquid
{% raw %}
  {% load_locals include: "users" %}

  {% for local in loaded_locals %}
    {% for user in local.users %}
      {{ user }}
    {% endfor %}
  {% endfor %}
{% endraw %}
```

## gtm
{: .d-inline-block}

Conjunto de tags para disparar eventos ou adicionar informações no dataLayer.

Adicionar informações do produto no dataLayer:

***product.liquid***
```liquid
{% raw %}
  {% gtm product_detail: product %}
{% endraw %}
```

Evento de adicionar ao carrinho:

***_infos.liquid***
```liquid
{% raw %}
  <button
    type="submit"
    class="add-to-cart-button"
    data-text-available="<span>Comprar</span>"
    data-text-unavailable="<span>Produto indisponível</span>"
    data-action="add-cart"
    data-event="addToCart"
    data-event-json='{% gtm item: product %}'>
    <span>Comprar</span>
  </button>
{% endraw %}
```

Evento de impressão:

***_product_block.liquid*** 
```liquid
{% raw %}
  {% gtm product_impression: product %}
{% endraw %}
```

Evento de remover um item do carrinho (modal):

***_popup.liquid***
```liquid
{% raw %}
  <button class="btn-delete" data-action="delete-item-cart" data-event-json="{% gtm remove: item %}">remover</button>
{% endraw %}
```

Evento de alterar a quantidade de itens no carrinho (modal):

***_popup.liquid***
```liquid
{% raw %}
  <button class="btn-plus" data-cart-quantity="plus" data-event-json="{% gtm quantity_change: item %}">+</button>
  <button class="btn-minus" data-cart-quantity="minus" data-event-json="{% gtm quantity_change: item %}">-</button>
{% endraw %}
```

## banners
{: .d-inline-block}

A tag {% raw %}`{% banners %}`{% endraw %} é uma tag de iteração que lista os banners cadastrados no painel da loja que possuam uma posição especificada pelo parâmetro `tag`.

A tag retorna um [objeto {% raw %}`{{ banner }}`{% endraw %}]({%- link docs/liquid2/objetos/banner.md -%}) e um [objeto {% raw %}`{{ forloop }}`{% endraw %}]({%- link docs/liquid2/objetos/forloop.md -%}).

{% raw %}

```liquid
{% banners tag:"home-destaque" limit:3 %}
  <!-- Estrutura HTML dos elementos do banner -->
{% endbanners %}
```

{% endraw %}

A tag aceita os seguintes parâmetros:

- `tag` (obrigatória): a posição de banner cadastrada no admin.
- `limit` (opcional): delimita o número máximo de banners que a tag deve exibir.

***

## cartitems
{: .d-inline-block}

A tag {% raw %}`{% cartitems %}`{% endraw %} percorre a lista os itens no carrinho do cliente. Ela retorna [um objeto {% raw %}`{{ item }}`{% endraw %}]({% link docs/liquid2/objetos/item.md %}) a cada iteração.

{% raw %}

```liquid
{% cartitems %}
  <!-- Estrutura HTML do item no carrinho -->
{% endcartitems %}
```

{% endraw %}

***

## include
{: .d-inline-block}

A tag {% raw %}`{% include %}`{% endraw %} é usada para chamar partes de template.

Arquivos de partes de template devem possuir um caractere de sublinhado antes do nome (ex. `_header.liquid`). Esse caractere não é usado na tag {% raw %}`{% include %}`{% endraw %}:

#### layout.liquid
{: .no_toc .text-delta }

{% raw %}
```liquid
{% include "partials/common/header" %}

<main>
  {{ yeld }}
</main>
```
{% endraw %}

Quando o template `layout.liquid` for renderizado, ele irá incluir todo o código presente em `partials/common/_header.liquid` onde ele foi chamado:

#### layout.liquid
{: .no_toc .text-delta }

{% raw %}

```html
<header class="site-header">
  <!-- código presente em partials/common/_header.liquid -->
</header>

<main>
  <!-- conteúdo do template -->
</main>
```

{% endraw %}
