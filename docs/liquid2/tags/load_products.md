---
layout: default
title: load_products
parent: Tags
grand_parent: Liquid 2
nav_order: 7
---

# load_products
{: .no_toc }

O {% raw %}`{% load_products %}`{% endraw %} é usado para listar produtos da loja através de uma série de parâmetros, especificados pelo desenvolvedor ou pelo cliente. Ele é necessário nos templates de [tag]({%- link docs/liquid2/templates/tag.md -%}) e de [resultados de busca]({%- link docs/liquid2/templates/search.md -%}), ou em qualquer outra listagem de produtos, como em [produtos relacionados]({%- link docs/liquid2/templates/product.md -%}#produtos-relacionados).

Ao chamar o {% raw %}`{% load_products %}`{% endraw %}, a tag disponibilizará uma lista de produtos em {% raw %}`{{ products }}`{% endraw %} com os [objetos {% raw %}`{{ product }}`{% endraw %}]({%- link docs/liquid2/objetos/product.md -%}) de cada produto retornado pela listagem, podendo ser acessado através de um [loop `for`]({%- link docs/liquid2/tags/iteracao.md -%}#for):

<div class="code-example" markdown="1">
```
Camiseta Verão
Casaco Outono
Blusão Inverno
Saia Primavera
```
</div>

{% raw %}

```liquid
{% load_products tag:tag.name %}

{% for product in products %}
  {{ product.name }}
{% endfor %}
```

{% endraw %}

***

## Nesse artigo
{: .no_toc .text-delta }

1. TOC
{:toc}

***

## Parâmetros da tag load_products

Para usar o `load_products`, é necessário um parâmetro `tag` ou `q`, dependendo de que tipo de lista de produtos é.

### tag
{: .no_toc }

O parâmetro `tag` deve receber o [nome identificador]({%- link docs/liquid2/objetos/tag.md -%}#name) da tag.

#### tag.liquid
{: .no_toc .text-delta }

{% raw %}

```liquid
{% load_products tag:tag.name %}
```

{% endraw %}

### ids
{: .no_toc}

O parâmetro `ids` deve receber uma string com os ids dos produtos a serem carregados separados por vírgula. Dessa forma é possível carregar produtos específicos independente de suas tags.

#### tag.liquid
{: .no_doc .text-delta}

{% raw %}

```liquid
{% load_products: ids: '1,2,3,4,5' %}
```

{% endraw %}

### q
{: .no_toc }

O parâmetro `q` deve receber os termos de busca informados pelo cliente.

Saiba mais sobre como ter acesso aos parâmetros de URL na [documentação do objeto {% raw %}`{% params %}`{% endraw %}]({%- link docs/liquid2/objetos/params.md -%}) e como criar um formulário de busca na [documentação do template search.liquid]({%- link docs/liquid2/templates/search.md -%}).

#### \_header.liquid
{: .no_toc .text-delta}

{% raw %}

```liquid
<form action="{{ search_url }}" method="get">
  <input type="search" name="search_query" />
</form>
```

{% endraw %}

#### search.liquid
{: .no_toc .text-delta }

{% raw %}

```liquid
{% load_products q:params.search_query %}
```

{% endraw %}

### parent_tag
{: .no_toc }

A tag “pai” da lista de produtos.

Considere que o cliente está acessando o seguinte endereço:

```
https://demo.vnda.com.br/camisetas/algodao/
```

Ele está vendo uma lista de produtos que tenham a tag `camisetas` e, desses, os que possuem a tag `algodao`. Para respeitar ambas as tags, é preciso usar o parâmetro `parent_tag`.

#### tag.liquid
{: .no_toc .text-delta }

{% raw %}

```liquid
{% load_products tag:tag.name parent_tag:parent_tag.name %}
```

{% endraw %}

### per_page
{: .no_toc }

O parâmetro `per_page` controla a quantidade de produtos exibidos por página na lista de produtos. A quantidade padrão de produtos por página é 25.

É possível permitir que o cliente delimite quantos produtos devem ser exibidos por página usando o [objeto {% raw %}`{{ params }}`{% endraw %}]({%- link docs/liquid2/objetos/params.md -%}).

#### search.liquid
{: .no_toc .text-delta }

{% raw %}

```liquid
{% load_products q:params.search_query per_page:params.per_page %}
```

{% endraw %}

### page
{: .no_toc .d-inline-block}

O parâmetro `page` controla qual página da lista está sendo exibida, em uma lista com mais produtos do que os delimitados pelo parâmetro `per_page`.

Veja a documentação do [objeto {% raw %}`{{ pagination }}`{% endraw %}](/liquid2/objetos/pagination) para mais informações sobre como criar a paginação de listas.

### sort
{: .no_toc }

O parâmetro `sort` define a ordenação dos produtos na lista.

Veja a documentação do [objeto {% raw %}`{{ sort_options }}`{% endraw %}]({%- link docs/liquid2/objetos/sort_options.md -%}) para mais informações sobre como criar opções de ordenação de produtos para o cliente.

#### search.liquid
{: .no_toc .text-delta }

{% raw %}

```liquid
{% load_products q:params.search_terms sort:params.sort_by %}
```

{% endraw %}

***

## Parâmetros de filtro

É possível criar filtros que permitem ao cliente delimitar qual seleção de parâmetros vai controlar a exibição da lista de produtos. Filtros são feitos com as informações agregadas de todos os produtos da lista, disponíveis através do [objeto {% raw %}`{{ aggregations }}`{% endraw %}]({% link docs/liquid2/objetos/aggregations.md %}).

Para permitir que o cliente tenha acesso às opções de filtro, é necessário criar um formulário HTML nos [templates de tag]({% link docs/liquid2/templates/tag.md %}) e de [resultados de busca]({% link docs/liquid2/templates/search.md %}):

#### \_filters.liquid
{: .no_toc .text-delta }

{% raw %}

```liquid
<form action="{{ current_url }}" method="get">
  <h3>Filtro de materiais:</h3>
  
  {% for material in aggregations.types.material %}
    <p>
      <input id="material_{{ forloop.index }}" type="checkbox" name="filtro_materiais[]" value="{{ material.name }}" />
      <label for="material_{{ forloop.index }}">{{ material.title }}</label>
    </p>
  {% endfor %}
</form>
```

{% endraw %}

Note que quando criar um filtro de qualquer tipo é necessário atualizar a tag {% raw %}`{% load_products %}`{% endraw %} de acordo.

#### tag.liquid
{: .no_toc .text-delta }

{% raw %}

```liquid
{% load_products tag:tag.name per_page:20 page:params.page material_tags:params.filtro_materiais %}
```

{% endraw %}

No exemplo acima, é criado um formulário com opções trazidas pela tag {% raw %}`{{ aggregations }}`{% endraw %}, que cria uma lista com todas as tags do tipo `material` que são relacionadas aos produtos na lista de produtos atual. Então, indicamos qual parâmetro de URL que o {% raw %}`{% load_products %}`{% endraw %} deve esperar para usar como filtro desse tipo de tag, através do `material_tags`.

Existem vários filtros que podem ser usados, seja de maneira dinâmica (com o cliente definindo seus valores) ou de maneira estática, com o desenvolvedor definindo-os no código.

### min_price
{: .no_toc }

Parâmetro que delimita um filtro de preço mínimo para a lista de produtos.

{% raw %}

```liquid
{% load_products tag:tag.name min_price:params.min_price %}
```

{% endraw %}

### max_price
{: .no_toc }

Parâmetro que delimita um filtro de preço máximo para a lista de produtos.

{% raw %}
```liquid
{% load_products tag:tag.name max_price:params.max_price %}
```
{% endraw %}

### \<tipo da tag\>\_tags
{: .no_toc .d-inline-block}

Define o parâmetro de URL que deve ser observado para filtrar os produtos por tags de um tipo específico.

{% raw %}
```liquid
{% load_products tag:tag.name material_tags:params.filtro_materiais %}
```
{% endraw %}

Confira a documentação de [`aggregations.types`]({% link docs/liquid2/objetos/aggregations.md %}#types) para exemplos de como criar um filtro de tags relacionadas aos produtos de uma lista.

### \<tipo da tag\>\_operator
{: .no_toc .d-inline-block}

Define o operador lógico que filtra várias tags do mesmo tipo. Possui dois valores possíveis:

- `OR` (padrão) — o produto precisa ter _qualquer uma_ das tags selecionadas para ser exibido.
- `AND` — o produto precisa ter _todas_ as tags selecionadas para ser exibido.

{% raw %}
```liquid
{% load_products tag: tag.name material_tags: params.filtro_materiais material_operator: "and" %}
```
{% endraw %}

### \<propriedade\>\_values
{: .no_toc .d-inline-block}

Define os valores para filtrar os produtos pelos seus atributos de variação.

- `property1_values` filtra os valores de acordo com [`aggregations.properties.property1`]({%- link docs/liquid2/objetos/aggregations.md -%}#properties)
- `property2_values` filtra os valores de acordo com [`aggregations.properties.property2`]({%- link docs/liquid2/objetos/aggregations.md -%}#properties)
- `property3_values` filtra os valores de acordo com [`aggregations.properties.property3`]({%- link docs/liquid2/objetos/aggregations.md -%}#properties)


#### \_filters.liquid
{: .no_toc .text-delta}

{% raw %}
```liquid
<form action="{{ current_url }}" method="get">
  <h3>Filtro de cor</h3>
  
  {% for cor in aggregations.properties.property2 %}
    <p>
      <input 
        id="cor_{{ forloop.index }}" 
        type="checkbox" 
        name="filtro_cor[]" 
        value="{{ cor }}" />
      
      <label for="cor_{{ forloop.index }}">
        {{ cor }}
      </label>
    </p>
  {% endfor %}
</form>
```
{% endraw %}

#### tag.liquid
{: .no_toc .text-delta }

{% raw %}

```liquid
{% load_products tag:tag.name per_page:12 page:params.page property2_values:params.filtro_cor %}
```

{% endraw %}