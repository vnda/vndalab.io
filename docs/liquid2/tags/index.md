---
layout: default
title: Tags
parent: Liquid 2
nav_order: 2
has_children: true
---

# Tags
{: .no_toc }

***


Tags operam a lógica que diz o que os templates devem fazer. Tags são demarcadas com caracteres {% raw %}`{% %}`{% endraw %}.

Algumas tags, como [`for`]({%- link docs/liquid2/tags/iteracao.md -%}#for) e [`cycle`]({%- link docs/liquid2/tags/iteracao.md -%}}#cycle) podem receber parâmetros. Outras, como [`assign`]({%- link docs/liquid2/tags/variaveis.md -%}}#assign), podem receber filtros.
