---
layout: default
title: Descontinuadas
parent: Tags
grand_parent: Liquid 2
nav_order: 5
---


# Descontinuadas


## contentfor
{: .d-inline-block}

Descontinuada
{: .label .label-red }

Tag descontinuada. Utilizar a tag de variável capture, por exemplo: `{% raw %}{% capture foo %}<p>Bar</p>{% endcapture %}{% endraw %}`


## tags e tag
{: .d-inline-block}

Descontinuada
{: .label .label-red }

Tag descontinuada. Utilize load_tags / load_tag.

`{% raw %}{% tags %}{% endtags %}{% endraw %}`


## variants
{: .d-inline-block}

Descontinuada
{: .label .label-red }

Tag descontinuada. Utilize **product.variants** com o produto pelo ElasticSearch ativo.

`{% raw %}{% variants %}{% endvariants %}{% endraw %}`

## installments
{: .d-inline-block}

Descontinuada
{: .label .label-red }

Tag descontinuada. Utilize **product.installments** com o produto pelo ElasticSearch ativo.

`{% raw %}{% installments %}{% endinstallments %}{% endraw %}`

## products
{: .d-inline-block}

Descontinuada
{: .label .label-red }

Tag descontinuada. Utilize **load_products** invés dessa.

`{% raw %}{% products %}{% endproducts %}{% endraw %}`

## aggregations
{: .d-inline-block}

Descontinuada
{: .label .label-red }

Tag descontinuada. Utilize o objeto **aggregations** disponibilidado pelo **load_products** invés dessa.

`{% raw %}{% aggregations %}{% endaggregations %}{% endraw %}`

## images
{: .d-inline-block}

Descontinuada
{: .label .label-red }

Tag descontinuada. Utilize o atributo **images** do objeto **product** invés dessa.

`{% raw %}{% images %}{% endimages %}{% endraw %}`

## sizes
{: .d-inline-block}

Descontinuada
{: .label .label-red }

Tag descontinuada. Utilize o atributo **variants** do objeto **product** invés dessa.

`{% raw %}{% sizes %}{% endsizes %}{% endraw %}`

## colors
{: .d-inline-block}

Descontinuada
{: .label .label-red }

Tag descontinuada. Utilize o atributo **variants** do objeto **product** invés dessa.

`{% raw %}{% colors %}{% endcolors %}{% endraw %}`

## product_count
{: .d-inline-block}

Descontinuada
{: .label .label-red }

Tag descontinuada. Utilize o objeto **products.size** disponibilizado pelo **load_products** invés dessa.

`{% raw %}{% product_count %}{% endproduct_count %}{% endraw %}`

## shippingmethods
{: .d-inline-block}

Descontinuada
{: .label .label-red }

Tag descontinuada. Utilize o cálculo de frete invés dessa.

`{% raw %}{% shippingmethods %}{% endshippingmethods %}{% endraw %}`

## search_options
{: .d-inline-block}

Descontinuada
{: .label .label-red }

Tag descontinuada. Utilize a busca com o **load_products** invés dessa.

`{% raw %}{% search_options %}{% endsearch_options %}{% endraw %}`

## get promotion
{: .d-inline-block}

Descontinuada
{: .label .label-red }

Tag descontinuada. Utilize os atributos do objeto **product** invés dessa.

`{% raw %}{% get promotion ... %}{% endraw %}`
