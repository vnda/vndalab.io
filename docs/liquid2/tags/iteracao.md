---
layout: default
title: Iteração
parent: Tags
grand_parent: Liquid 2
nav_order: 3
---

# Tags de Iteração
{: .no_toc }

Tags de iteração exibem códigos repetidamente.

***

## Nesse artigo
{: .no_toc .text-delta }

1. TOC
{:toc}

***

## for

Executa um bloco de código repetidamente. Para uma lista completa de atributos disponíveis em um loop `for`, veja o objeto [`forloop`]({%- link docs/liquid2/objetos/forloop.md -%})

<div class="code-example" markdown="1">
```
verão camisa amarelo
```
</div>

{% raw %}
```liquid
{% for tag in product.tag_names %}
  {{ tag }}
{% endfor %}
```
{% endraw %}

### else
{: .no_toc }

Especifica um comportamento caso o loop tenha tamanho zero (se, por exemplo, um produto não possuir tags):

<div class="code-example" markdown="1">
```
Esse produto não possui tags.
```
</div>

{% raw %}
```liquid
{% for tag in product.tag_names %}
  {{ tag }}
{% else %}
  Esse produto não possui tags.
{% endfor %}
```
{% endraw %}

### break
{: .no_toc }

Interrompe o loop quando ele encontra a tag `break`.

<div class="code-example" markdown="1">
```
1 2 3
```
</div>

{% raw %}
```liquid
{% for i in (1..5) %}
  {% if i == 4 %}
    {{ break }}
  {% else %}
    {{ i }}
  {% endif %}
{% endfor %}
```
{% endraw %}

### continue
{: .no_toc }

Faz o loop pular para a próxima posição quando encontra a tag `continue`.


<div class="code-example" markdown="1">
```
1 2 3   5
```
</div>

{% raw %}
```liquid
{% for i in (1..5) %}
  {% if i == 4 %}
    {{ continue }}
  {% else %}
    {{ i }}
  {% endif %}
{% endfor %}
```
{% endraw %}

### Parâmetros da tag for
{: .no_toc }

##### limit
{: .no_toc }

Limita o loop para um número específico de vezes:

<div class="code-example" markdown="1">
```
verao camisa
```
</div>

{% raw %}
```liquid
<!-- product.tag_names = ["verao", "camisa", "amarelo", "promo"] -->
{% for tag in product.tag_names limit: 2 %}
  {{ tag }}
{% endfor %}
```
{% endraw %}

##### offset
{: .no_toc }

Começa o loop em uma posição específica:

<div class="code-example" markdown="1">
```
amarelo promo
```
</div>
{% raw %}
```liquid
<!-- product.tag_names = ["verao", "camisa", "amarelo", "promo"] -->
{% for tag in product.tag_names offset:2 %}
  {{ tag }}
{% endfor %}
```
{% endraw %}

##### range
{: .no_toc }

Define um intervalo de números para percorrer. O intervalo pode ser definido literalmente ou através de variáveis numéricas.

<div class="code-example" markdown="1">
```
3 4 5

1 2 3 4
```
</div>
{% raw %}
```liquid
{% for i in (3..5) %}
  {{ i }}
{% endfor %}

{% assign fim = 4 %}
{% for i in (1..fim) %}
  {{ i }}
{% endfor %}
```
{% endraw %}

##### reversed
{: .no_toc }

Percorre o loop na ordem contrária.

<div class="code-example" markdown="1">
```
promo amarelo camisa verao
```
</div>
{% raw %}
```liquid
<!-- product.tag_names = ["verao", "camisa", "amarelo", "promo"] -->
{% for tag in product.tag_names reversed %}
  {{ tag }}
{% endfor %}
```
{% endraw %}

***

## cycle

Percorre um grupo de strings e as exibe na ordem em que foram passadas como parâmetros. Toda a vez que `cycle` é chamado, a próxima string que foi passada será exibida.

**Nota:** `cycle` deve ser usado dentro de um `for`.

<div class="code-example" markdown="1">
```
um
dois
tres
um
```
</div>
{% raw %}
```liquid
{% cycle "um", "dois", "tres" %}
{% cycle "um", "dois", "tres" %}
{% cycle "um", "dois", "tres" %}
```
{% endraw %}


Você pode usar `cycle`, por exemplo, quando:

- quer aplicar classes alternadas em linhas de uma tabela.
- quer aplicar uma tag única no último item de uma lista.


### Parâmetros da tag cycle
{: .no_toc }

O `cycle` aceita o parâmetro `group` no caso de você precisar de múltiplos blocos de ciclos em um template. Se nenhum grupo for dado para o `cycle`, o Liquid assume que todos os ciclos são do mesmo grupo.

O exemplo abaixo mostra porque grupos são necessários quando existem várias instâncias do bloco de `cycle`.

{% raw %}
```liquid
{% load_products tag:camisa %}

<ul>
  {% for product in products %}
    <li {% cycle 'style="clear: both;"', '', '', 'class="last"' %}>
    </li>
  {% endfor %}
</ul>

{% load_products tag:verao %}
<ul>
  {% for product in products %}
    <li {% cycle 'style="clear: both;"', '', '', 'class="last"' %}>
    </li>
  {% endfor %}
</ul>
```
{% endraw %}

No exemplo acima, se a primeira tag só possuir dois produtos, a segunda tag vai continuar o `cycle` onde a primeira parou. Isso vai resultar em um código bagunçado:

```html
<ul>
  <li style="clear: both;"></li>
  <li></li>
</ul>

<ul>
  <li></li>
  <li class="none"></li>
  <li style="clear: both;"></li>
  <li></li>
</ul>
```

Para evitar esse problema, você pode usar um grupo de `cycle` para cada bloco:

{% raw %}
```liquid
{% load_products tag:camisa %}
<ul>
  {% for product in products %}
    <li {% cycle 'camisa': 'style="clear: both;"', '', '', 'class="last"' %}>
    </li>
  {% endfor %}
</ul>

{% load_products tag:verao %}
<ul>
  {% for product in products %}
    <li {% cycle 'verao': 'style="clear: both;"', '', '', 'class="last"' %}>
    </li>
  {% endfor %}
</ul>
```
{% endraw %}

Com o código acima, os blocos `cycle` serão independentes, e vão produzir a seguinte marcação:

```html
<ul>
  <li style="clear: both;"></li>
  <li></li>
</ul>

<ul>
  <li style="clear: both;"></li>
  <li></li>
  <li></li>
  <li class="last"></li>
</ul>
```

***

## tablerow

`tablerow` gera linhas de tabelas HTML. Elas precisam estar dentro de uma tag `<table>`. Para uma lista completa dos atributos disponíveis em um loop `tablerow`, confira a referência do objeto [`tablerow`]({%- link docs/liquid2/objetos/tablerow.md -%}).

<div class="code-example" markdown="1">
```html
<table>
  <tr class="row1">
    <td class="col1">Amarelo</td>
    <td class="col2">Verde</td>
    <td class="col3">Laranja</td>
    <td class="col4">Vermelho</td>
    <td class="col5">Branco</td>
  </tr>
</table>
```
</div>
{% raw %}
```liquid
{% tags type:cor %}
  <table>
    {% tablerow product in products %}
	  {{ tag.title }}
    {% endtablerow %}
  </table>
{% endtags %}
```
{% endraw %}

### Parâmetros da tag tablerow
{: .no_toc }

##### cols
{: .no_toc }

Define quantas colunas uma linha deve ter.

<div class="code-example" markdown="1">
```html
<table>
  <tr class="row1">
    <td class="col1">Amarelo</td>
    <td class="col2">Verde</td>
  </tr>
  <tr class="row2">
    <td class="col1">Laranja</td>
    <td class="col2">Vermelho</td>
  </tr>
  <tr class="row3">
    <td class="col1">Branco</td>
  </tr>
</table>
```
</div>
{% raw %}
```liquid
{% tags type:cor %}
  <table>
    {% tablerow product in products cols:2 %}
      {{ tag.title }}
    {% endtablerow %}
  </table>
{% endtags %}
```
{% endraw %}

##### limit
{: .no_toc }

Termina o loop `tablerow` em uma posição específica.

{% raw %}
```liquid
{% tags type:cor %}
  <table>
    {% tablerow product in products cols:2 limit:3 %}
      {{ tag.title }}
    {% endtablerow %}
  </table>
{% endtags %}
```
{% endraw %}

##### offset
{: .no_toc }

Inicia o loop do `tablerow` em uma posição específica.

{% raw %}
```liquid
{% tags type:cor %}
  <table>
    {% tablerow product in products cols:2 offset:3 %}
      {{ tag.title }}
    {% endtablerow %}
  </table>
{% endtags %}
```
{% endraw %}

##### range
{: .no_toc }

Define um intervalo de números para percorrer. O intervalo pode ser definido literalmente ou através de variáveis numéricas.

{% raw %}
```liquid
<table>
  {% tablerow i in (3..5) %}
    {{ i }}
  {% endtablerow %}
</table>

{% assign fim = 4 %}
<table>
  {% tablerow i in (1..fim) %}
    {{ i }}
  {% endtablerow %}
</table>
```
{% endraw %}
