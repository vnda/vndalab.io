---
layout: default
title: Comentário
parent: Tags
grand_parent: Liquid 2
nav_order: 1
---


# Comentário


Permite que você deixe o código não renderizado dentro de um template Liquid. Qualquer texto dentro dos blocos de comentário de abertura e fechamento não será impresso, e qualquer código Liquid dentro não será executado.

Por exemplo:

{% raw %}
```liquid
Anything you put between {% comment %} and {% endcomment %} tags
is turned into a comment.
```
{% endraw %}

Resultará em:

```
Anything you put between  tags
is turned into a comment.
```
