---
layout: default
title: params
parent: Objetos
grand_parent: Liquid 2
search_exclude: true
---

# params
{: .no_toc }

## Conteúdos
{: .no_toc .text-delta }

1. TOC
{:toc}

---

{% raw %}

`{{ params }}` é um objeto especial que dá acesso aos parâmetros da URL atual. Isso é útil em [páginas de tag](/setup/templates/tag.liquid) e de [resultados de busca](/setup/templates/search.liquid) para controlar a lista de produtos.

É preciso alinhar o objeto `{{ params }}` com as opções da tag [`{% load_products %}`](/liquid2/tags/loja#load_products) para que esse comportaento funcione corretamente. Por exemplo, para controlar a página atual da lista de produtos, você pode indicar um parâmetro `page`, que pode ser acessado no template como `{{ params.page }}`.

###### Exemplo

```liquid
<!-- Controlando a lista de produtos com os parâmetros de URL. -->

{% load_products q:params.q page:params.current_page property1_values:params.tamanhos %}
```

Confira a documentação de objetos que podem ser usados com o objeto `{{ params }}`:

- [Objeto `{{ pagination }}`](/liquid2/objetos/pagination)
- [Objeto `{{ aggregations }}`](/liquid2/objetos/aggregatons)
- [Objeto `{{ sort_options }}`](/liquid2/objetos/sort_options)

{% endraw %}
