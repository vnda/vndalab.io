---
layout: default
title: variant
parent: Objetos
grand_parent: Liquid 2
---

# variant
{: .no_toc }

## Conteúdos
{: .no_toc .text-delta }

1. TOC
{:toc}

---

{% raw %}

O objeto `{{ variant }}` é usado no atributo `variants` do `product` para exibir informações das variantes de um produto.
  
## Atributos do objeto

- [`available`](#available)
- [`available_quantity`](#available_quantity)
- [`custom_attributes`](#custom_attributes)
- [`handling_days`](#handling_days)
- [`height`](#height)
- [`id`](#id)
- [`image_url`](#image_url)
- [`installments`]
- [`inventories`](#inventories)
- [`length`](#length)
- [`main`](#main)
- [`min_quantity`](#min_quantity)
- [`name`](#name)
- [`price`](#price)
- [`product_id`](#product_id)
- [`properties`](#properties)
- [`quantity`](#quantity)
- [`sale_price`](#sale_price)
- [`sku`](#sku)
- [`slug`](#slug)
- [`stock`](#stock)
- [`updated_at`](#updated_at)
- [`weight`](#weight)
- [`width`](#width)

### available

Retorna `true` se a variante está disponível. Retorna `false` se a variante não está disponível.

---

### available_quantity

Retorna o número de unidades da variante disponíveis para compra.

---

### custom_attributes

TODO

---

### handling_days

Retorna o número de dias de manuseio da variante. Útil para produtos feitos sob encomenda.

---

### height

Retorna a altura da unidade, em centímetros.

---

### id

Retorna o ID da variante.

---

### image_url

Retorna a URL da primeira imagem de produto relacionada à variante.

---

### installments

Retorna uma lista com o número e o valor para parcelamento específico de uma variante. Útil para montar uma tabela de parcelamentos. Para exibir a opção de parcelamentos, veja a tag [`{% installments %}`](/liquid2/tags/loja#installments).

---

### inventories

TODO

---

### length

Retorna o comprimento da unidade, em centímetros.

---

### main

Retorna `true` se for a variante principal do produto. Retorna `false` se não for a variante principal.


---

### min_quantity

Retorna a quantidade mínima da variante que deve ser adicionada ao carrinho.

---

### name

Retorna o nome da variante. Geralmente usada para exibir a descrição legível da variação.

---

### price

Retorna o preço da variante.

---

### product_id

Retorna o ID do produto.

---

### properties

Retorna um array com as propriedades da variante. Uma variante possui até três propriedades, cada uma com um nome (fixo) e um valor (variável).

Por exemplo, variantes podem possuir uma variação de Cor, Tamanho e Material.

###### product.liquid

```liquid
<h1>{{ product.name }}</h1>

{% variants product_id: product.id %}
  <p>{{ variant.name }}:</p>
  
  <ul>
    <li>{{ variant.properties.property1.name }}: {{ variant.properties.property1.value }}</li>
    <li>{{ variant.properties.property2.name }}: {{ variant.properties.property2.value }}</li>
    <li>{{ variant.properties.property3.name }}: {{ variant.properties.property3.value }}</li>
  </ul>
{% endvariants %}
```

```html
<h1>Camiseta Verão</h1>

<p>Tamanho P / Cor Amarela</p>

<ul>
  <li>Tamanho: P</li>
  <li>Cor: Amarelo</li>
  <li>Material: Algodão</li>
</ul>

<p>Tamanho M / Cor Amarela</p>

<ul>
  <li>Tamanho: M</li>
  <li>Cor: Amarelo</li>
  <li>Material: Algodão</li>
</ul>
  
<p>Tamanho G / Cor Laranja</p>

<ul>
  <li>Tamanho: G</li>
  <li>Cor: Laranja</li>
  <li>Material: Algodão</li>
</ul>
```

---

### quantity

Retorna a quantidade da variante em estoque.

---

### sale_price

Retorna o preço promocional da variante.

---

### sku

Retorna o SKU da variante, que deve ser enviado no formulário de produto.

---

### slug

Retorna o nome da página em versão URLficada.

---

### stock

Retorna o estoque do produto.

---

### updated_at

Retorna a data absoluta da última modificação do produto.

---

### weight

Retorna o peso da unidade, em gramas.

---

### width

Retorna a largura da unidade, em centímetros.

{% endraw %}
