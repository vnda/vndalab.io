---
layout: default
title: forloop
parent: Objetos
grand_parent: Liquid 2
---

# forloop
{: .no_toc }

## Conteúdos
{: .no_toc .text-delta }

1. TOC
{:toc}

---

{% raw %}

O objeto `{{ forloop }}` contém os atributos de um [loop `for`](/liquid2/tags/iteração#for). O `{{ forloop }}` só pode ser usado dentro das tags `{% for %}`.

## Atributos do objeto

- [`first`](#first)
- [`index`](#index)
- [`index0`](#index0)
- [`last`](#last)
- [`length`](#length)
- [`rindex`](#rindex)
- [`rindex0`](#rindex0)

### first

Retorna `true` se é a primeira iteração do loop. Retorna `false` em todas as outras iterações.

###### Exemplo
```liquid
{% for product in products limit: 5 %}
  {% if forloop.first %}
    Primeiro!
  {% else %}
    Não é o primeiro.
  {% endif %}
{% endfor %}
```

```
Primeiro!
Não é o primeiro.
Não é o primeiro.
Não é o primeiro.
Não é o primeiro.
```

---

### index

Retorna a posição atual do loop, começando em 1.

###### Exemplo
```liquid
{% for product in products %}
  {{ forloop.index }}
{% else %}
  Não há produtos nessa tag.
{% endif %}
```

```
1 2 3 4 5
```

---

### index0

Retorna a posição atual do loop, começando em 0.

###### Exemplo
```liquid
{% for product in products %}
  {{ forloop.index0 }}
{% else %}
  Não há produtos nessa tag.
{% endif %}
```

```
0 1 2 3 4
```

---

### last

Retorna `true` se é a última iteração do loop. Retorna `false` em todas as outras iterações.

###### Exemplo

```liquid
{% for product in products limit: 5 %}
  {% if forloop.last %}
    Adieu!
  {% else %}
    Tchananã…
  {% endif %}
{% endfor %}
```

```
Tchananã…
Tchananã…
Tchananã…
Tchananã…
Adieu!
```

---

### length

Retorna o número de iterações do loop. 

###### Exemplo

```liquid
{% for product in products %}
  {% if forloop.first %}
    Essa tag possui {{ forloop.length }} produtos:
  {% endif %}
  
  {{ product.name }}
{% endfor %}
```

```
Essa tag possui 5 produtos:

Camiseta Verão
Casaco Outono
Blusão Inverno
Saia Primavera
Tênis Ano Todo
```

---

### rindex

Retorna o [`forloop.index`](#index) em ordem reversa.

###### Exemplo

```liquid
{% for product in products %}
  {{ forloop.rindex }}
{% endfor %}
```

```
5 4 3 2 1
```

---

### rindex0

Retorna o [`forloop.index0`](#index0) em ordem reversa.

###### Exemplo

```liquid
{% for product in products %}
  {{ forloop.rindex0 }}
{% endfor %}
```

```
4 3 2 1 0
```

{% endraw %}
