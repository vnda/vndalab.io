---
layout: default
title: item
parent: Objetos
grand_parent: Liquid 2
---

# item
{: .no_toc }

## Conteúdos
{: .no_toc .text-delta }

1. TOC
{:toc}

---

{% raw %}

O objeto `{{ item }}` é usado no atributo `items` do `cart` para renderizar itens adicionados ao carrinho de compras.

###### Exemplo
```liquid
<div>
  {% cartitems %}
    <div>
      <a href="{{ item.product_url }}">
        <img src="{{ item.image_url | resize: '250x' }}" alt="{{ product.name }}" />
      </a>
      
      <p>
        <a href="{{ item.product_url }}">
          {{ item.product_name }}
        </a><br />
        {{ item.variant_name }} / {{ item.variant_sku }}<br />
        {{ item.price | money_format }}
      </p>
    
    </div>
  {% endcartitems %}
</div>
```

## Atributos do objeto

- [`available_quantity`](#available_quantity)
- [`delivery_days`](#delivery_days)
- [`extra`](#extra)
- [`id`](#id)
- [`image_url`](#image_url)
- [`price`](#price)
- [`product_id`](#product_id)
- [`product_name`](#product_name)
- [`product_url`](#product_url)
- [`quantity`](#quantity)
- [`subtotal`](#subtotal)
- [`total`](#total)
- [`updated_at`](#updated_at)
- [`variant_attributes`](#variant_attributes)
- [`variant_min_quantity`](#variant_min_quantity)
- [`variant_name`](#variant_name)
- [`variant_price`](#variant_price)
- [`variant_properties`](#variant_properties)
- [`variant_sku`](#variant_sku)

### available_quantity

Retorna a quantidade de itens disponíveis na loja. Útil para delimitar quantos itens poderão ser adicionados no carrinho.

---

### delivery_days

Retorna o prazo de entrega do item.

---

### extra

Retorna uma lista com os [atributos personalizados](/guias/produtos-personalizados) do produto, se houver.

###### product.liquid
```html
<!-- No formulário de produto: -->

<label for="extra-prop">Gravação para presente:</label>
<input id="extra-prop" name="extra[Gravação para presente]" type="text" />
```

###### cart_popup.liquid
```liquid
<p>
  <a href="{{ item.product_url }}">{{ item.product_name }}</a><br />
  {{ item.variant_sku }}
</p>

{% if item.extra.length %}
  <p>
    {% for extra in item.extra %}
      <strong>{{ extra.name }}:</strong><br />
      {{ extra.value }}
    {% endfor %}
  </p>
{% endif %}
```
---

### id

Retorna o ID do item do carrinho (diferente do ID do produto).

---

### image_url

Retorna a URL da imagem da variante adicionada ao carrinho. Se a variante adicionada ao carrinho não possuir uma imagem relacionada, retorna a imagem do produto.

---

### price

Retorna o preço unitário do produto.

---

### product_id

Retorna o ID do produto.

---

### product_name

Retorna o nome do produto.

---

### product_url

Retorna a URL da página do produto.

---

### quantity

Retorna a quantidade desse item adicionada ao carrinho.

---

### subtotal

Retorna o subtotal do item — o preço vezes a quantidade.

---

### total

Retorna o total do item — o preço vezes a quantidade, com os descontos promocionais.

###### cart_popup.liquid

```liquid
{{ item.product_name }}
{{ item.quantity | append: 'x' }} {{ item.price | money_format }}

Subtotal: {{ item.subtotal | money_format }}
Total: {{ item.total | money_format }}
```

---

### updated_at

Retorna a data absoluta de quando o produto foi adicionado ao carrinho ou a quantidade foi alterada. A exibição dessa informação pode ser controlada através do `date`.

---

### variant_attributes

TODO

---

### variant_min_quantity

Retorna a quantidade mínima da variante que deve ser adicionada ao carrinho.

---

### variant_name

Retorna o nome da variante adicionada ao carrinho.

###### cart_popup.liquid

```liquid
{{ item.product_name }}<br />
{{ item.variant_name }}
```

```
Camiseta Verão
Tamanho G, Cor Amarela
```

---

### variant_price

Retorna o preço unitário da variante adicionada ao carrinho.

---

### variant_properties

Retorna um objeto com as propriedades da variante.

- **variant_properties.property1**
- **variant_properties.property2**
- **variant_properties.property3**

Cada propriedade possui três informações:

- _name_: o nome da propriedade (ex. “Tamanho”, “Cor”);
- _value_: o valor da propriedade (ex. "G", "Amarelo");
- _defining_: retorna `true` ou `false`.

###### cart_popup.liquid

```liquid
<p>
  {{ item.product_name }}<br />
  {{ item.variant_name }} / {{ item.variant_sku }}
</p>

<ul>
  <li>{{ item.variant_properties.property1.name }}: {{ item.variant_properties.property1.value }}</li>
  <li>{{ item.variant_properties.property2.name }}: {{ item.variant_properties.property2.value }}</li>
```

```
Camiseta Verão
Estampada M / CVB-M

- Tamanho: M
- Cor: Amarela
```

---

### variant_sku

Retorna o SKU da variante adicionada ao carrinho.

{% endraw %}
