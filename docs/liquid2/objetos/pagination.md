---
layout: default
title: pagination
parent: Objetos
grand_parent: Liquid 2
---

# pagination
{: .no_toc }

## Conteúdos
{: .no_toc .text-delta }

1. TOC
{:toc}

---

{% raw %}

O objeto `{{ pagination }}` é usado nos templates de [tag](/setup/templates/tag.liquid) e [busca](/setup/templates/search.liquid) para renderizar links de navegação entre páginas de uma lista de produtos.

###### Exemplo

```liquid

{% load_products q:params.q per_page: 15 page:params.p %}

{% for product in products %}
  {% include "partials/components/_product_block.liquid %}
{% else %}
  <p>Não há produtos que correspondem à sua pesquisa.</p>
{% endfor %}

{% if pagination.total_pages > 1 %}

  <form method="get">
  
    {% unless pagination.prev_url == current_url %}
      <a href="{{ pagination.prev_url }}" rel="prev">« Anterior</a>
    {% endunless %}
    
    <span>
      Página
      <input 
        type="number" 
        name="p" 
        max="{{ pagination.total_pages }}" 
        value="{{ params.q | default: pagination.current_page }}" /> 
        
      de {{ pagination.total_pages }}
    </span>
    
    {% unless pagination.next_url == current_url %}
      <a href="{{ pagination.next_url }}" rel="next">Próxima »</a>
    {% endunless %}
  
  </form>

{% endif %}

```

## Atributos do objeto

- [`count`](#count)
- [`current_page`](#current_page)
- [`next_url`](#next_url)
- [`pages`](#pages)
- [`prev_url`](#prev_url)
- [`total_pages`](#total_pages)

### count

Retorna a contagem total de produtos na lista.

###### \_pagination.liquid

```liquid
Você está vendo {{ params.per_page }} de {{ pagination.count }} produtos.
```

```
Você está vendo 20 de 45 produtos.
```

---

### current_page

Retorna o número da página sendo visualizado no momento.

###### \_pagination.liquid

```liquid
Página {{ pagination.current_page }} de {{ pagination.total_pages }}.
```

```
Página 2 de 2.
```

---

### next_url

Retorna a URL da próxima página da lista.

###### \_pagination.liquid

```liquid
{% if current_url == pagination.next_url %}
  <a href="{{ pagination.next_url }}" rel="next">Próxima página</a>
{% else %}
  <span class="no-more-pages">Próxima página</span>
{% endif %}
```

```html
<a href="https://demo.vnda.com.br/camisetas/?p=3" rel="next">Próxima página</a>
```

---

### pages

Retorna uma lista de URLs para cada página.

Cada item da lista possui:

- _url_: a URL para a página.
- _number_: o número da página.

###### \_pagination.liquid

```liquid
{% for page in pagination.pages %}
  <a href="{{ page.url }}" 
    {% if page.url == current_url %}class="current-page"{% endunless %}>
    [{{ page.number }}]
  </a>
{% endfor %}
```

```html
<a href="https://demo.vnda.com.br/camisetas/?p=1">1</a>
<a href="https://demo.vnda.com.br/camisetas/?p=2" class="current-page">2</a>
<a href="https://demo.vnda.com.br/camisetas/?p=3">3</a>
```

---

### prev_url

Retorna a URL da página anterior da lista.

###### \_pagination.liquid

```liquid
{% if current_url == pagination.prev_url %}
  <a href="{{ pagination.prev_url }}" rel="prev">Página anterior</a>
{% else %}
  <span class="no-more-pages">Página anterior</span>
  {% endif %}
```

```html
<a href="https://demo.vnda.com.br/camisetas/?p=1">Página anterior</a>
```

---

### total_pages

Retorna o número total de páginas da lista.

###### \_pagination.liquid

```liquid
{% if pagination.total_pages > 0 %}
<!-- Vamos ter que criar uma paginação! -->
{% else %}
<!-- Não precisamos criar uma paginação… -->
{% endif %}
```

```html
<!-- Vamos ter que criar uma paginação! -->
```

{% endraw %}
