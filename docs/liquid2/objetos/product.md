---
layout: default
title: product
parent: Objetos
grand_parent: Liquid 2
---

# product
{: .no_toc }

## Conteúdos
{: .no_toc .text-delta }

1. TOC
{:toc}

---

{% raw %}

O objeto `{{ product }}` é disponível na [página de produto](/setup/templates/product.liquid) e através da tag [`{% load_products %}`]. O objeto possui todas as informações disponíveis para cada produto registrado através da plataforma.

## Atributos do objeto

- [`active`](#active)
- [`available`](#available)
- [`category_tags`](#category_types)
- [`description`](#description)
- [`discount_id`](#discount_id)
- [`id`](#id)
- [`image_url`](#image_url)
- [`installments`](#installments)
- [`min_quantity`](#min_quantity)
- [`name`](#name)
- [`on_sale`](#on_sale)
- [`price`](#price)
- [`rating`](#rating)
- [`reference`](#reference)
- [`sale_price`](#sale_price)
- [`slug`](#slug)
- [`tag_names`](#tag_names)
- [`updated_at`](#updated_at)
- [`url`](#url)
- [`variants`](#variants)

### active

Retorna `true` ou `false`, caso o produto esteja ativo ou não.

---

### available

Retorna `true` ou `false`, caso o produto esteja disponível para compra.

###### product.liquid

```liquid
{% if product.available %}
  <p>{{ product.price | money_format }}</p>
{% else %}
  <p class="product-unavailable">Produto indisponível.</p>
{% endif %}
```

```html
<p class="product-available">Produto indisponível.</p>
```

---

### category_tags

Retorna uma lista de tags em forma de objetos. Cada tag possui as seguintes informações:

- _tag_type_: o tipo de tag
- _name_: o nome identificador da tag
- _title_: o título de exibição da tag.

###### product.liquid

```liquid
{% for tag in category_tags %}
<a href="{{ tag.name | prepend: '/' }}" rel="tag">{{ tag.tag_type }}: {{ tag.title }}</a>
{% endfor %}
```

```html
<a href="/linho" rel="tag">Material: Linho</a>
<a href="/blusas" rel="tag">Categoria: Blusas</a>
<a href="/estilo" rel="tag">Estilo: Leve</a>
```

---

### description

Retorna o texto de descrição do produto, em HTML.

Você pode definir seções diferentes para a descrição, e controlá-los com [filtros](/liquid2/filtros):

###### product.liquid

```liquid
{% assign product_description_sections = product.description | split: "<hr/>" %}

{% for description_section in product_description_sections %}
<section>
  {{ description_section }}
</section>
{% endfor %}
```

```html
<section>
  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec orci libero, blandit vitae tellus ut, rhoncus dapibus purus. Donec ullamcorper vehicula ligula.</p>
  <p>Nec cursus tortor interdum a. Sed vel lacinia sem. Fusce at velit ac urna pulvinar facilisis a ut ante. Curabitur pulvinar lacus neque, ac venenatis ante lobortis non.</p>
</section>

<section>
  <p>Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam ac laoreet ligula, quis imperdiet ante. Nullam auctor ipsum at tellus elementum bibendum.</p>
</section>
```

---

### discount_id

Retorna o ID da promoção aplicada ao produto.

---

### id

Retorna o ID do produto.

---

### image_url

Retorna a URL da imagem principal do produto.

###### product.liquid

```liquid
<figure>
  <picture>
    <source srcset="{{ product.image_url | resize: '600x' }}" media="(max-width: 600px)" />
    <source srcset="{{ product.image_url | resize: '800x' }}" media="(max-width: 800px)" />
    <img src="{{ product.image_url }} alt="{{ product.name }}" />
  </picture>
</figure>
```

---

### images

Retorna uma lista de URLs das imagens do produto, e os SKUs de variantes relacionadas em cada imagem.

Cada imagem possui as seguintes informações:

- _sku_: retorna o SKU da variante relacionada à imagem
- _url_: retorna a URL da imagem

Se uma imagem for relacionada à duas SKUs diferentes, haverá um registro duplicado para a mesma URL. Você pode evitar que isso aconteça filtrando a lista com o filtro `uniq`.

###### \_images.liquid

```liquid
{% assign product_image_urls = product.images | map: "url" | uniq %}
```

---

### installments

Retorna uma lista com o número e o valor para parcelamento. Útil para montar uma tabela de parcelamentos. Para exibir a opção de parcelamentos, veja a tag [`{% installments %}`](/liquid2/tags/loja#installments) 

###### product.liquid

```liquid
<table>
  <thead>
    <tr>
      <th>Parcela</th>
      <th>Valor</th>
    </tr>
  </thead>
  <tbody>
    {% for installment in product.installments %}
      <tr>
        <td>{{ forloop.index }}</td>
        <td>{{ installment | money_format }}</td>
      </tr>
    {% endfor %}
  </tbody>
</table>
```

```html
<table>
  <thead>
    <tr>
      <th>Parcela</th>
      <th>Valor</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td>R$ 78,90</td>
    </tr>
    <tr>
      <td>2</td>
      <td>R$ 78,90</td>
      </tr>
      <tr>
      <td>3</td>
      <td>R$ 78,90</td>
      </tr>
      <tr>
      <td>4</td>
      <td>R$ 78,90</td>
      </tr>
      <tr>
      <td>5</td>
      <td>R$ 42,90</td>
      </tr>
  </tbody>
  </table>
```

---

### min_quantity

Retorna a quantidade mínima para compra do produto.

---

### name

Retorna o nome do produto.

---

### on_sale

Retorna `true` se o produto está em promoção.

###### \_product_block.liquid

```liquid
{% if product.on_sale %}
  <p>De: {{ product.price | money_format }}</p>
  <p>Por: {{ product.sale_price | money_format }}</p>
{% else %}
  <p>Preço: {{ product.price | money_format }}</p>
{% endif %}
```

```html
<p>De: R$ 99,90</p>
<p>Por: R$ 85,90</p>
```

---

### price

Retorna o preço do produto.

---

### rating

Retorna um objeto com a avaliação do produto feita por clientes. `rating` possui dois atributos:

- _rating_: a média das avaliações
- _votes_: o número de votos

###### product.liquid

```liquid
Avaliação dos clientes: <strong>{{ product.rating.rating | replace: '.', ',' }}</strong> ({{ product.rating.votes }})
```

```html
Avaliação dos clientes: <strong>4,75</strong> (12)
```

---

### reference

Retorna a referência do produto.

---

### sale_price

Retorna o preço promocional do produto.

###### \_product_block.liquid

```liquid
{% if product.on_sale %}
  <p>De: {{ product.price | money_format }}</p>
  <p>Por: {{ product.sale_price | money_format }}</p>
{% else %}
  <p>Preço: {{ product.price | money_format }}</p>
{% endif %}
```

```html
<p>De: R$ 99,90</p>
<p>Por: R$ 85,90</p>
```

---

### slug

Retorna o caminho do produto.

###### Exemplo

```liquid
{{ product.name }}
{{ product.slug }}
```

```
Camiseta Verão
camiseta-verao
```

---

### tag_names

Retorna o nome das tags relacionadas ao produto.

###### Exemplo

```liquid
<!-- product.tag_names = ["camiseta", "verao", "amarelo", "promo"] -->
Tags: {{ product.tag_names | join: ", " }}.
```

```
Tags: camiseta, verão, amarelo, promo.
```

---

### updated_at

Retorna a data absoluta da última atualização do produto. A exibição desse atributo pode ser controlada através do filtro `date`.

---

### url

Retorna a URL do produto.

---

### variants

Retorna uma lista de objetos com as variantes dos produtos. [Veja a documentaçào do objeto `{{ variant }}`](/liquid2/objetos/variant) para mais informações.

{% endraw %}
