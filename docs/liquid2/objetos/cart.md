---
layout: default
title: cart
parent: Objetos
grand_parent: Liquid 2
---

# cart
{: .no_toc }

## Conteúdos
{: .no_toc .text-delta }

1. TOC
{:toc}

---

{% raw %}

O objeto `{{ cart }}` retorna as informações do carrinho de compras de um cliente.

## Atributos do objeto

- [`agent`](#agent)
- [`billing_address_id`](#billing_address_id)
- [`channel`](#channel)
- [`client_id`](#client_id)
- [`coupon_code`](#coupon_code)
- [`discount`](#discount)
- [`discount_price`](#discount_price)
- [`extra`](#extra)
- [`id`](#id)
- [`installments`](#installments)
- [`items`](#items)
- [`items_count`](#items_count)
- [`shipping_address_id`](#shipping_address_id)
- [`shipping_method`](#shipping_method)
- [`subtotal`](#subtotal)
- [`token`](#token)
- [`total`](#total)
- [`total_for_deposit`](#total_for_deposit)
- [`total_for_slip`](#total_for_slip)
- [`updated_at`](#updated_at)

### agent

TODO

---

### billing_address_id

Retorna o ID do endereço de cobrança do cliente, se ele estiver logado.

---

### channel

TODO

---

### client_id

Retorna o ID do cliente, se ele estiver logado.

---

### coupon_code

Retorna o código promocional que está sendo aplicado no carrinho.

---

### discount

Retorna o desconto que está sendo aplicado no carrinho.

---

### discount_price

Retorna o valor do desconto que está sendo aplicado no carrinho.

###### cart_popup.liquid

```liquid
<p>
  Subtotal: {{ cart.subtotal | money_format }}
</p>
{% if cart.discount_price %}
<p>
  Desconto: {{ cart.discount_price | money_format }}
</p>
{% endif %}
<p>
  Total: {{ cart.total | money_format }}
</p>
```

```
Subtotal: R$ 99,50
Desconto: R$ 9,95
Total: R$ 89,55
```

---

### extra

TODO

---

### id

Retorna o ID atribuído ao carrinho de compras.

---

### installments

Retorna as parcelas possíveis para o pagamento do carrinho.

---

### items

Retorna um vetor de produtos no carrinho. Veja a documentação do objeto [`{{ item }}`](./item) para mais informações.

---

### items_count

Retorna o número de itens no carrinho de compras.

###### cart_popup.liquid

```liquid
{% if cart.items_count > 0 %}
  {% if cart.items_count > 1 %}
    {% assign cart_count_text = "itens" %}
  {% else %}
    {% assign cart_count_Text == "item" %}
  {% endif %}
  
  <p>Você tem {{ cart.items_count }} {{ cart_count_text }} no carrinho.</p>
{% else %}
  <p>Seu carrinho está vazio.</p>
{% endif %}
```

```
Você tem 3 itens no carrinho.
```

---

### shipping_address_id

Retorna o ID do endereço de entrega do cliente, se ele estiver logado.

---

### shipping_method

Retorna o método de entrega selecionado pelo cliente.

---

### subtotal

Retorna o subtotal do carrinho (sem juros ou descontos aplicados).

---

### token

TODO

---

### total

Retorna o valor total do carrinho de compras.

###### cart_popup.liquid

```liquid
<p>
  Total: {{ cart.total | money_format }}
</p>
```

```
Total: R$ 99,50
```

---

### total_for_deposit

Retorna o valor total do carrinho caso o pagamento seja feito por depósito.


###### cart_popup.liquid

```liquid
<p>
  Total: {{ cart.total | money_format }}
</p>

{% if cart.total_for_deposit < cart.total %}
<p>
  Ou {{ cart.total_for_depois | money_format }} pagando por depósito.
</p>
{% endif %}

```

```
Total: R$ 99,50

Ou R$ 89,55 pagando por depósito.
```

---

### total_for_slip

Retorna o valor total do carrinho caso o pagamento seja feito por boleto bancário.

```liquid
<p>
  Total: {{ cart.total | money_format }}
</p>

{% if cart.total_for_deposit < cart.total %}
<p>
  Ou {{ cart.total_for_depois | money_format }} no boleto.
</p>
{% endif %}

```

```
Total: R$ 99,50

Ou R$ 89,55 pagando no boleto.
```

---

### updated_at

Retorna a data absoluta em que o carrinho foi modificado pela última vez. Você pode personalizar a exibição dessa informação através do filtro `date`.

{% endraw %}
