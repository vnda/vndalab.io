---
layout: default
title: aggregations
parent: Objetos
grand_parent: Liquid 2
---

# aggregations
{: .no_toc }

O objeto {% raw %}`{{ aggregations }}`{% endraw %} contém informações sobre como uma listagem de produtos pode ser filtrada, como o menor preço de produto em uma tag ou uma lista de tags, separadas por tipo, dos produtos exibidos na lista.

O {% raw %}`{{ aggregations }}`{% endraw %} pode ser usado para criar filtros de listagem de produto nos templates [`tag.liquid`]({%- link docs/liquid2/templates/tag.md -%}) e [`search.liquid`]({%- link docs/liquid2/templates/search.md -%}).

***

## Atributos do objeto
{: .no_toc .text-delta }

1. TOC
{:toc}

***

## Atributos do objeto
{: .no_toc }

### max_prize e min_prize

Retorna o menor e o maior preço dentre os produtos da listagem atual.

Considere que uma tag possua três camisetas, `Camiseta A` custa R$ 39,90 `Camiseta B` custa R$ 59,90 e `Camiseta C` custa R$ 40,00.

{% raw %}
```liquid
Menor preço: {{ aggregations.min_prize }}
Maior preço: {{ aggregations.max_prize }}
```
{% endraw %}

```
Menor preço: R$ 39,90
Maior preço: R$ 59,90
```

***

### properties

Retorna um array com os atributos existentes nos produtos da lista atual.

Considere que o primeiro atributo de produto numa loja é usado para variação de tamanhos e o segundo para cor; e que a lista atual possua:

- uma camisa com tamanhos `P` e `M` e cor `amarelo`
- uma outra camisa com tamanhos `P` e `G` e as cores `amarelo` e `vermelho`:

<div class="code-example" markdown="1">
```
property1 = [ "P", "M", "G" ]
property2 = [ "amarelo", "vermelho" ]
```
</div>
{% raw %}
```liquid
{{ aggregations.properties }}
```
{% endraw %}
O {% raw %}`{{ aggregations.properties }}`{% endraw %} combina valores iguais para ser possível criar filtros que selecionem todos os produtos com determinado tamanho:

#### \_filters.liquid
{: .no_toc .text-delta }

{% raw %}
```liquid
<form method="get">
  {% for tamanho in aggregations.properties.property1 %}
    <input id="tamanho_{{ forloop.index }}" name="tamanhos[]" value="{{ tamanho.value }}"  />
    <label for="tamanho_{{ forloop.index }}">{{ tamanho.value }}</label> 
  {% endfor %}
</form>
```
{% endraw %}

Lembre-se de adicionar o parâmetro `tamanhos[]` à tag {% raw %}[`{% load_products %}`]({%- link docs/liquid2/tags/load_products.md -%}){% endraw %} no template:

#### tag.liquid
{: .no_toc .text-delta }

{% raw %}
```liquid
{% load_products tag:tag.name property1_values:params.tamanhos  %}
```
{% endraw %}

***

### types

Retorna arrays de tags organizadas por tipos.

Considere que:

- o produto "Camisa P e M" possui uma tags `algodao` e `viscose` com o tipo `material` 
- o produto "Camisa P e G" possui a tag `viscose` com o tipo `material`, a tag e `verao` com o tipo `colecao`:

<div class="code-example" markdown="1">
```
material = [ "algodao", "viscose ]
colecao = [ "verao" ]
```
</div>
{% raw %}
```liquid
{{ aggregations.types }}
```
{% endraw %}

Assim, é possível criar um filtro de materiais.

#### \_filters.liquid
{: .no_toc .text-delta }

{% raw %}
```liquid
<form method="get">
  {% for material in aggregations.types.material %}
    <input id="material_{{ forloop.index }}" name="materiais[]" value="{{ material.name }}"  />
    <label for="material_{{ forloop.index }}">{{ material.title }}</label> 
  {% endfor %}
</form>
```
{% endraw %}

Lembre-se de adicionar o parâmetro `materiais[]` à tag {% raw %}[`{% load_products %}`]({%- link docs/liquid2/tags/load_products.md -%}){% endraw %} no template que você quer oferecer o filtro:

#### search.liquid
{: .no_toc .text-delta }

{% raw %}
```liquid
{% load_products q:params.q material_tags:params.materiais %}
```
{% endraw %}
