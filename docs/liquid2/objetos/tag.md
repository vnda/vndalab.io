---
layout: default
title: tag
parent: Objetos
grand_parent: Liquid 2
---

# tag
{: .no_toc }

O objeto {% raw %}`{{ tag }}`{% endraw %} retorna as informações da tag atual no [template de tag]({%- link docs/liquid2/templates/tag.md -%}).

**Nota:** anteriormente, essa documentação se referia ao objeto {% raw %}`{{ tag }}`{% endraw %} retornado nas tags {% raw %}`{% tag %}`{% endraw %} e {% raw %}`{% tags %}`{% endraw %}. Essas tags entraram em desuso e foram substituídas pelas tags [{% raw %}`{% load_tag %}` e `{% load_tags %}`{% endraw %}]({%- link docs/liquid2/tags/load_tags.md -%}).

***

## Atributos do objeto
{: .no_toc }

* TOC
{:toc}

***

### description

Retorna o texto de descrição da tag, em HTML.

***

### id

Retorna o ID da tag.

***

### image_url

Retorna a URL da imagem da tag.

***

### name

Retorna o nome identificador da tag. Útil para definir mudanças de comportamento dependendo de uma tag específica.

#### tag.liquid
{: .no_toc .text-delta }

<div class="code-example" markdown="1">
```html
<a href="#guia-de-medidas" data-toggle-popup>Guia de medidas</a>
```
</div>
{% raw %}
```liquid
{% if tag.name == "camiseta" %}
  <a href="#guia-de-medidas" data-toggle-popup>Guia de medidas</a>
{% endif %}
```
{% endraw %}

***

### products_count

Retorna a contagem de produtos com a tag específica.

***

### subtitle

Retorna o subtítulo da tag.

***

### title

Retorna o título legíval da tag.

***

### type

Retorna o tipo de tag. Útil para definir layouts específicos para um tipo de tag.

{% raw %}
```liquid
{% if tag.type == "colecao" %}
  {% include "partials/tag/layout_colecao" %}
{% else %}
  <!-- Estrutura padrão de tags -->
{% endif %}
```
{% endraw %}

***

### updated_at

Retorna a data absoluta da última alteração da tag.

