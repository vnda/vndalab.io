---
layout: default
title: template
parent: Objetos
grand_parent: Liquid 2
---

# template
{: .no_toc }


O objeto `{% raw %}{{ template }}{% endraw %}` retorna o nome do template renderizado na página.

| Valor do objeto | Página               |
|-----------------|----------------------|
| `home`          | Início               |
| `product`       | Produto              |
| `tag`           | Tag                  |
| `search`        | Resultados de busca  |
| `page`          | Página personalizada |
| `account`       | Conta de Usuário     |
| `login`         | Login/Criar conta    |


O objeto `{% raw %}{{ template }}{% endraw %}` para controlar blocos de código que devem aparecer apenas em determinados templates.

###### layout.liquid
{: .no_toc }

{% raw %}
```liquid
{% if template == "home" %}
  {% include "partials/common/header_home" %}
{% else %}
  {% include "partials/common/header" %}
{% endif %}
```
{% endraw %}
