---
layout: default
title: sort_options
parent: Objetos
grand_parent: Liquid 2
---

# sort_options
{: .no_toc }

## Conteúdos
{: .no_toc .text-delta }

1. TOC
{:toc}

---

{% raw %}

O objeto `{{ sort_options }}` possui uma lista de opções para ordenar as listas de produto nos templates de [tag](/setup/templates/tag.liquid) e de [resultados de busca](/setup/templates/search.liquid). Você pode percorrer esse objeto com um [loop `for`](/liquid2/tags/iteração#for) para criar um `<select>` com as opções.

**Valores disponíveis no `{{ sort_options }}`**

| value         | label         |
|---------------|---------------|
|               | Padrão        |
| newest        | Mais recentes |
| oldest        | Mais antigos  |
| lowest_price  | Menor preço   |
| highest_price | Maior preço   |
  
###### tag.liquid

```liquid
<select name="sort_by">
  {% for option in sort_options %}
    <option value="{{ option.value }}" 
      {% if params.sort_by == option.value %}selected{% endif %}>
      {{ option.label }}
    </option>
  {% endfor %}
</select>
```

```html
<select name="sort_by">
  <option value="" selected>Padrão</label>
  <option value="newest" selected>Mais recentes</label>
  <option value="oldest" selected>Mais antigos</label>
  <option value="lowest_price" selected>Menor preço</label>
  <option value="highest_price" selected>Maior preço</label>
</select>
```

Lembre-se de adicionar o `name` do `<select>` como parâmetro na [tag `{% load_products %}`](/liquid2/tags/loja#load_products):
  
```liquid
{% load_product tag:tag.name sort:params.sort_by %}
```

{% endraw %}
