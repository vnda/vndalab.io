---
layout: default
title: client
parent: Objetos
grand_parent: Liquid 2
---

# client
{: .no_toc }

## Conteúdos
{: .no_toc .text-delta }

1. TOC
{:toc}

---

{% raw %}

O objeto `{{ client }}` contem informações sobre um cliente que está logado com uma conta de cliente.

###### Exemplo

```liquid
{% if client %}
  Olá, {{ client.first_name }}.
{% else %}
  Olá, visitante!
{% endif %}
```

## Atributos do objeto

- [`auth_token`](#auth_token)
- [`birthdate`](#birthdate)
- [`cnpj`](#cnpj)
- [`confirmed_orders_count`](#confirmed_orders_count)
- [`cpf`](#cpf)
- [`email`](#email)
- [`facebook_uid`](#facebook_uid)
- [`first_name`](#first_name)
- [`gender`](#gender)
- [`id`](#id)
- [`ie`](#ie)
- [`last_confirmed_order_at`](#last_confirmed_order_at)
- [`last_name`](#last_name)
- [`liked_facebook_page`](#liked_facebook_page)
- [`phone`](#phone)
- [`phone_area`](#phone_area)
- [`recent_address`](#recent_address)
- [`tags`](#tags)
- [`updated_at`](#updated_at)

### auth_token

Retorna o token de autenticação da sessão do cliente.

---

### birthdate

Retorna a data de nascimento do cliente, se informado.

---

### cnpj

Retorna o número do CNPJ do cliente, se informado.

---

### confirmed_orders_count

Retorna o número de compras relacionadas ao cliente que foram confirmadas.

---

### cpf

Retorna o número do CPF do cliente, se informado.

---

### email

Retorna o endereço de email do cliente, se informado.

---

### facebook_uid

Retorna o ID de usuário do Facebook se o cliente criou uma conta através da [integração da loja com o Facebook](/setup/integrações/facebook).

---

### first_name

Retorna o nome do cliente.

---

### gender

Retorna o gênero do cliente, se informado.

---

### id

Retorna o ID do cliente.

---

### ie

TODO

---

### last_confirmed_order_at

Se o cliente já fez um pedido na loja, retorna a data absoluta do último pedido que foi confirmado.

---

### last_name

Retorna o sobrenome do cliente.

---

### liked_facebook_page

Retorna `true` se o cliente curtiu a página do Facebook que foi [integrada à loja](/setup/integrações/facebook).

---

### phone

Retorna o número de telefone bruto do cliente, sem DDD.

---

### phone_area

Retorna o DDD do telefone do cliente.

---

### recent_address

Objeto que retorna o endereço mais recente usado pelo cliente em uma compra.

**recent_address.city**

Retorna a cidade informada na última compra realizada pelo cliente.

**recent_address.company_name**

Retorna o nome da empresa informado na última compra realizada pelo cliente.

**recent_address.complement**

Retorna o complemento do endereço informado na última compra realizada pelo cliente.

**recent_address.documents.cnpj**

Retorna o número do CNPJ informado na última compra realizada pelo cliente.

**recent_address.documents.cpf**

Retorna o número do CPF informado na última compra realizada pelo cliente.

**recent_address.email**

Retorna o endereço de email informado na última compra realizada pelo cliente.

**recent_address.first_name**

Retorna o nome informado na última compra realizada pelo cliente.

**recent_address.first_phone**

Retorna o número de telefone bruto, sem DDD, informado na última compra realizada pelo cliente.

**recent_address.first_phone_area**

Retorna o DDD do número de telefone informado na última compra realizada pelo cliente.

**recent_address.id**

Retorna o ID do endereço informado na última compra realizada pelo cliente.

**recent_address.last_name**

Retorna o sobrenome informado na última compra realizada pelo cliente.

**recent_address.neighborhood**

Retorna o bairro informado na última compra realizada pelo cliente.

**recent_address.reference**

Retorna a referência informada na última compra realizada pelo cliente.

**recent_address.second_phone**

Retorna o número de telefone opcional, sem DDD, informado na última compra realizada pelo cliente.

**recent_address.second_phone_area**

Retorna o DDD do número de telefone opcional informado na última compra realizada pelo cliente.

**recent_address.state**

Retorna a sigla do estado informada na última compra realizada pelo cliente.

**recent_address.street_name**

Retorna o nome da rua ou logradouro informado na última compra realizada pelo cliente.

**recent_address.street_number**

Retorna o número do endereço informado na última compra realizada pelo cliente.

**recent_address.zip**

Retorna o CEP do endereço informado na última compra realizada pelo cliente.

---

### tags

Retorna as tags relacionadas a esse cliente. Útil para identificar grupos de clientes através da plataforma.

---

### updated_at

Retorna a data absoluta da última modificação de dados ou pedidos do cliente. A exibição dessa informação pode ser controlada pelo filtro `date`.

{% endraw %}
