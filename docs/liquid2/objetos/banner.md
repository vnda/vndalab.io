---
layout: default
title: banner
parent: Objetos
grand_parent: Liquid 2
---

# banner
{: .no_toc }

## Conteúdos
{: .no_toc .text-delta }

1. TOC
{:toc}

---

{% raw %}

O objeto `{{ banner }}` contem as informações salvas em um banner através do painel. Banners são blocos de conteúdo dinâmicos que podem ser listados por posição usando a tag `{% load_banners %}`.

###### home.liquid
```liquid
{% banners tag: "home-destaque" %}
  <div class="banner-destaque">
    <a href="{{ banner.url }}" {% if banner.external %}target="_blank"{% endif %}>
      <img src="{{ banner.file_url | resize: '600x' }}" alt="{{ banner.title }}" /> 
    </a>

    <div>
      {% unless banner.subtitle == blank %}
        <h3><a href="{{ banner.url }}" {% if banner.external %}target="_blank"{% endif %}>{{ banner.subtitle }}</a></h3>  
      {% endunless %}

      {{ banner.description }}
    </div>
  </div>
{% endbanners %}
```

## Atributos do objeto

- [`color`](#color)
- [`description`](#description)
- [`external`](#external)
- [`file_url`](#file_url)
- (`subtitle`)(#subtitle)
- [`title`](#title)
- [`url`](#url)

### color

Se informado na plataforma, retornará o valor hexadecimal de uma cor, para ser aplicado de acordo com o comportamento previsto pelo desenvolvedor.

---

### description

Retorna um bloco de texto HTML com a descrição de um banner.

---

### external

Retorna um valor booleano `true`/`false` que define se a URL do banner deve ser aberta em uma nova aba.

---

### file_url

Retorna a URL da imagem relacionada ao banner.

---

### subtitle

Retorna o subtítulo do banner. É um campo opcional ao criar um banner através da plataforma, recomenda-se usá-lo ao invés de `title` para exibir o título de um banner.

---

### title

Retorna o título do banner. O título é um campo obrigatório ao criar um banner através da plataforma, então recomenda-se não usar ele como identificação de onde esse banner se localiza no layout da loja, e não no front.

---

### url

Retorna a URL de destido do banner.

{% endraw %}
