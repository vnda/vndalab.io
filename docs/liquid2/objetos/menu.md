---
layout: default
title: menu
parent: Objetos
grand_parent: Liquid 2
---

# menu
{: .no_toc }

## Conteúdos
{: .no_toc .text-delta }

1. TOC
{:toc}

---

{% raw %}

O objeto `{{ menu }}` é usado dentro da tag `{% load_menus %}` para exibir itens de menu cadastrado pelos gestores da loja através da plataforma.

###### Exemplo
```liquid
<ul>
  {% menus position:principal %}
    <li>
      <a href="{{ menu.url }}"
        {% unless menu.tooltip == blank %}class="{{ menu.tooltip }}"{% endunless %} 
        {% if menu.external %}target="_blank"{% endif %}>
        
        {% if menu.image_url %}
          <img src="{{ menu.image_url }}" alt="{{ menu.title }}" />
        {% endif %}
        
        {{ menu.title }}
      </a>
      
      {% if menu.children %}
        <ul>
        
          {% for submenu in menu.children %}
            <li>
              <a href="{{ submenu.url }}"
                {% unless submenu.tooltip == blank %}class="{{ submenu.tooltip }}"{% endunless %}
                {% if submenu.external %}target="_blank"{% endif %}>
                {{ submenu.title }}
              </a>
            </li>
          {% endfor %}
        
        </ul>
      {% endif %}
    </li>
  {% endmenus %}
</ul>
```

## Atributos do objeto

- [`children`](#children)
- [`external`](#external)
- [`image_url`](#image_url)
- [`items_count`](#items_count)
- [`title`](#title)
- [`tooltip`](#tooltip)
- [`url`](#url)

### children

Retorna uma lista de submenus do menu atual. Esses submenus possuem os mesmos atributos do objeto `{{ menu }}` e podem ser percorridos por um [loop `for`](/liquid2/tags/iteração#for).

###### Exemplo
```liquid
{% if menu.children %}
  Esse menu possui {{ items_count }} submenus:
  
  <ul>
    {% for submenu in menu.children %}
      <li>
        <a href="{{ submenu.url }}">
          {{ submenu.title }}
        </a>
        
        {% if submenu.children %}
          <ul>
            {% for childmenu in submenu.children %}
              <!-- ... -->
            {% endfor %}
          </ul>
        {% endif %}
      </li>
    {% endfor %}
  </ul>
{% endif %}
```

---

### external

Retorna `true` caso o gestor tenha optado por esse link abrir em uma nova janela, ou `false` caso o gestor não tenha marcado a opção.

---

### image_url

Se o menu atual aponta para uma tag e essa tag possuir uma imagem, esse atributo retornará a URL dessa imagem.

###### Exemplo

```liquid
{% if menu.image_url %}
  <a href="{{ menu.url }}">
    <img src="{{ menu.image_url }}" alt="{{ menu.title }}" />
  </a>
{% else %}
  <a href="{{ menu.url }}">
    <span>{{ menu.title }}</span>
  </a>
{% endif %}
```

---

### items_count

Retorna o número de submenus que o menu atual possui.

###### Exemplo

```liquid
{% if menu.items_count > 0 %}
  {% assign has_submenu = true %}
{% else %}
  {% assign has_submenu = false %}
{% endif %}

<li {% if has_submenu %}class="has-submenu"{% endif %}>
  {{ menu.title }}  

  {% if has_submenu %}
    <ul>
      <!-- ... -->
    </ul>
  {% endif %}
</li>
```

### title

Retorna o título do menu.

---

### tooltip

Retorna o valor informado no campo `tooltip` na plataforma. Você pode usar esse campo para dinamizar classes ou cores de itens de submenu.

###### Exemplo

```liquid
<li {% if menu.tooltip == "promo" %}style="color: red"{% endif %}>
  <!-- ... -->
</li>
```

---

### url

Retorna a URL do menu.

{% endraw %}
