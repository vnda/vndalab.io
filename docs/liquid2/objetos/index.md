---
layout: default
title: Objetos
parent: Liquid 2
nav_order: 3
has_children: true
---

# Objetos
{: .no_toc }

Objetos contém atributos que são usados para exibir conteúdo dinâmico em uma página. 

Por exemplo, o objeto {% raw %}`{{ produto }}`{% endraw %} contém um atributo `name` que pode ser usado para exibir o nome de um produto. **Objetos** podem ser chamados de **variáveis** também, já que para usá-los nós os colocamos entre {% raw %}`{{ }}`{% endraw %}:

{% raw %}
```liquid
{{ product.name }}
```
{% endraw %}

Você pode exibi-lo no console do navegador da seguinte forma:

{% raw %}
```liquid
<script>console.info("product", {{ product | json }});</script>
```
{% endraw %}

Não se esqueça de remover esse _snippet_ quando terminar de verificar as propriedades de um objeto.

***

## Nesse artigo
{: .no_toc .text-delta }

1. TOC
{:toc}

***

## cart

O objeto `{% raw %}{{ cart }}{% endraw %}` retorna as informações do carrinho de compras de um cliente.

- [Atributos cart]({% link docs/liquid2/objetos/cart.md %})

## client

O objeto `{% raw %}{{ client }}{% endraw %}` contem informações sobre um cliente que está logado com uma conta de cliente.

- [Atributos client]({% link docs/liquid2/objetos/client.md %})

## cart_url

O objeto `{% raw %}{{ cart_url }}{% endraw %}` retorna a url do carrinho.

## cart_item_url

O objeto `{% raw %}{{ cart_url }}{% endraw %}` retorna a url para adicionar um item no carrinho.

## current_shop

O objeto `{% raw %}{{ current_shop }}{% endraw %}` dá acesso às informações da loja.

## current_url

O objeto `{% raw %}{{ current_url }}{% endraw %}` retorna a url atual.

## home_url

O objeto `{% raw %}{{ home_url }}{% endraw %}` retorna a url da home.

## language

O objeto `{% raw %}{{ language }}{% endraw %}` retorna a lingua atual, podendo ser: `pt-BR`, `en` ou `es`.

## page

O objeto `{% raw %}{{ page }}{% endraw %}` é usado no [template de páginas personalizadas](/setup/templates/page.liquid) para exibir o conteúdo de uma página criada através da plataforma da Vnda.

- [Atributos page]({% link docs/liquid2/objetos/page.md %})

## product

O objeto `{% raw %}{{ product }}{% endraw %}` é disponível na [página de produto](/setup/templates/product.liquid) e através da tag [`{% raw %}{% load_products %}{% endraw %}`]. O objeto possui todas as informações disponíveis para cada produto registrado através da plataforma.

- [Atributos product]({% link docs/liquid2/objetos/product.md %})

## store
{: .d-inline-block}

Descontinuado
{: .label .label-red }

Descontinuado, utilize `{% raw %}{{ current_shop }}{% endraw %}`.

## tag (home)

Quando utilizado na home, o objeto `{% raw %}{{ tag }}{% endraw %}` retorna a tag `capa`.

## tag (tag)

O objeto {% raw %}`{{ tag }}`{% endraw %} retorna as informações da tag atual no [template de tag]({%- link docs/liquid2/templates/tag.md -%}).

## template

O objeto `{% raw %}{{ template }}{% endraw %}` retorna o nome do template renderizado na página.
