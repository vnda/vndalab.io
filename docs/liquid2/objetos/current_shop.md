---
layout: default
title: current_shop
parent: Objetos
grand_parent: Liquid 2
---

# current_shop
{: .no_toc }

## Conteúdos
{: .no_toc .text-delta }

1. TOC
{:toc}

---

{% raw %}

O objeto `{{ current_shop }}` dá acesso às informações da loja.

###### Exemplo

```liquid
<h3>{{ current_shop.name }}</h3>

<p>
  {{ current_shop.street_name }}, {{ current_shop.street_number }}<br />
  {{ current_shop.neighborhood }}. {{ current_shop.city }} / {{ current_shop.sate_name }}<br />
  {{ current_shop.zip }}
</p>

<ul>
  <li><a href="mailto:{{ current_shop.email }}">{{ current_shop.email }}</a></li>
  <li><a href="tel:{{ current_shop.phone }}">{{ current_shop.phone }}</a></li>
</ul>
```

```html
<h3>Nome da Loja</h3>

<p>
  Rua de Exemplo, 42<br />
  Bairro de Exemplo. Cidade / ES<br />
  11111-111
</p>

<ul>
  <li>
    <a href="mailto:email@exemplo.com">email@exemplo.com</a>
  </li>
  <li>
    <a href="tel:+55119999999">+55119999999</a>
  </li>
</ul>
```

## Atributos do objeto

- [`active`](#active)
- [`city`](#city)
- [`complement`](#complement)
- [`created_at`](#created_at)
- [`email`](#email)
- [`host`](#host)
- [`logo_url`](#logo_url)
- [`name`](#name)
- [`neighborhood`](#neighborhood)
- [`phone`](#phone)
- [`settings`](#settings)
- [`state_name`](#state_name)
- [`street_name`](#street_name)
- [`street_number`](#street_number)
- [`subdomain`](#subdomain)
- [`updated_at`](#updated_at)
- [`zip`](#zip)

### active

Retorna `true` se a loja está ativa.

---

### city

Retorna a cidade cadastrada no endereço da loja.

---

### complement

Retorna o complemento do endereço da loja.

---

### created_at

Retorna a data em que o ambiente da loja foi criado.

---

### email

Retorna o email de contato da loja.

---

### host

Retorna o URL da loja.

---

### logo_url

Retorna o URL da imagem cadastrada como logo da loja na plataforma.

---

### name

Retorna o nome da loja

---

### neighborhood

Retorna o bairro cadastrado no endereço da loja.

---

### phone

Retorna o número de telefone da loja.

---

### settings

Retorna um objeto com as configurações da loja.

**TODO:** documentar o objeto de configurações.

---

### state_name

Retorna a sigla do estado cadastrado o endereço da loja.

---

### street_name

Retorna a rua cadastrada como endereço da loja.

---

### street_number

Retorna o número do endereço da loja.

---

### subdomain

Retorna o nome do domínio da loja.

---

### updated_at

Retorna a data em que as configurações da loja foram atualizadas.

---

### zip

Retorna o CEP do endereço da loja.

{% endraw %}
