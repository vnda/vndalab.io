---
layout: default
title: page
parent: Objetos
grand_parent: Liquid 2
---

# page
{: .no_toc }

## Conteúdos
{: .no_toc .text-delta }

1. TOC
{:toc}

---

{% raw %}

O objeto `{{ page }}` é usado no [template de páginas personalizadas](/setup/templates/page.liquid) para exibir o conteúdo de uma página criada através da plataforma da Vnda.

###### page.liquid

```liquid
<article>
  <header>
    <h1>{{ page.title }}</h1>
  </header>
  
  <div class="content">
    {{ page.body }}
  </div>
</article>
```

```html
<article>
  <header>
    <h1>Título da página</h1>
  </header>
  
  <div class="content">
    <p>Lorem ipsum dolor sit amet.</p>
  </div>
  </article>
```

## Atributos da página

- [`body`](#body)
- [`description`](#description)
- [`full_url`](#full_url)
- [`id`](#id)
- [`slug`](#slug)
- [`title`](#title)
- [`updated_at`](#updated_at)

### body

Retorna o conteúdo da página em HTML.

---

### description

Retorna o texto de descrição da página.

---

### id

Retorna o ID da página, gerado pela plataforma.

---

### slug

Retorna o caminho da página.

---

### title

Retorna o título da página.

---

### updated_at

Retorna a data absoluta da última alteração da página. A exibição desse atributo pode ser controlada através do filtro `date`.

{% endraw %}
