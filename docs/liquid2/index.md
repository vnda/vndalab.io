---
layout: default
title: Liquid 2
nav_order: 5
has_children: true
---

# Liquid
{: .no_toc }

Liquid é uma linguagem de template criada pelo Shopify e escrita em Ruby. Ela está disponível como um [projeto de código aberto](https://github.com/Shopify/liquid) no GitHub e é usada por muitos projetos e empresas de software diferentes, inclusive a Vnda.

Uma linguagem de templates permite reutilizar o conteúdo estático que define o layout de um site enquanto preenche a página dinâmicamente com os dados de uma loja da Vnda. O conteúdo estático é escrito em HTML, e os conteúdos dinâmicos são escritos em Liquid. Os elementos Liquid em um arquivo servem como “espaços reservados”: quando o código no arquivo é compilado pela plataforma e enviado para o navegador, o Liquid é substituído pelos dados da loja.

## Versões


- Liquid v2.6.3
{: .d-inline-block}

Suportada
{: .label .label-yellow }

Possuímos suporte, contudo muitos recursos que estão disponíveis no Liquid v4 não estão disponíveis nessa versão.

- [Liquid v4.0.3](https://github.com/Shopify/liquid2/tree/1feaa6381300d56e2c71b49ad8fee0d4b625147b)
{: .d-inline-block}

Atual
{: .label .label-green }

Essa é a versão atual e a que está recebendo os últimos recursos implementados pela plataforma.
