---
layout: default
title: Assets
parent: Liquid 2
nav_order: 6
---

# Assets
{: .no_toc }

## Tags de assets

### head

Essa é uma tag cumulativa. Seu conteúdo fica armazenado na variável `{% raw %}{{ content_for_head }}{% endraw %}`, que fica dentro do `<head>` no `layout.liquid`.
Exemplo de uso:

{% raw %}
```liquid
{% head %}
  <link rel="stylesheet" href="{{ 'style.css' | stylesheet_path }}" />
{% endhead %}
```
{% endraw %}

### body

Essa é uma tag cumulativa. Seu conteúdo fica armazenado na variável `{% raw %}{{ content_for_body }}{% endraw %}`, que fica no final do `<body>` no `layout.liquid`.
Exemplo de uso:

{% raw %}
```liquid
{% body %}
  <script src="{{ 'script.js' | javascript_path }}"></script>
{% endbody %}
```
{% endraw %}

## Filtros de assets

### image_path

Filtro de imagens. Considera o caminho a partir de `/images/`.
Exemplo de uso:

{% raw %}
```liquid
<img src="{{ 'image.png' | image_path }}" />
```
{% endraw %}

### favicon_path

Filtro de favicon. Considera o caminho a partir de `/`.
Exemplo de uso:

{% raw %}
```liquid
<link rel="shortcut icon" href="{{ 'favicon.ico' | favicon_path }}" />
```
{% endraw %}

### stylesheet_path

Filtro de estilos. Considera o caminho a partir de `/stylesheets/`.
Exemplo de uso:

{% raw %}
```liquid
<link rel="stylesheet" href="{{ 'style.css' | stylesheet_path }}" />
```
{% endraw %}

### javascript_path

Filtro de scripts. Considera o caminho a partir de `/javascripts/`.
Exemplo de uso:

{% raw %}
```liquid
<script src="{{ 'script.js' | javascript_path }}"></script>
```
{% endraw %}
