---
layout: default
title: Nativos
parent: Filtros
grand_parent: Liquid 2
nav_order: 1
---

# Filtros nativos do Liquid
{: .no_toc }

## Conteúdos
{: .no_toc .text-delta }

1. TOC
{:toc}

***

## abs

Retorna o valor absoluto de um número.

<div class="code-example" markdown="1">

{{ -17 | abs }}

</div>
{% raw %}
```liquid
{{ -17 | abs }}
```
{% endraw %}


<div class="code-example" markdown="1">

{{ 4 | abs }}

</div>
{% raw %}
```liquid
{{ 4 | abs }}
```
{% endraw %}

`abs` também funciona em strings que só possuam um número.

<div class="code-example" markdown="1">

{{ "-19.86" | abs }}

</div>
{% raw %}
```liquid
{{ "-19.86" | abs }}
```
{% endraw %}

***

## append

Concatena duas strings e retorna o valor concatenado.


<div class="code-example" markdown="1">

{{ "/minha/url" | append: ".html" }}

</div>
{% raw %}
```liquid
{{ "/minha/url" | append: ".html" }}
```
{% endraw %}

`append` também pode ser usado em variáveis com [`assign`]({%- link docs/liquid2/tags/variaveis.md -%}):

<div class="code-example" markdown="1">

{% assign filename = "/index.html" %}
{{ "vnda.com.br" | append: filename }}

</div>
{% raw %}
```liquid
{% assign filename = "/index.html" %}
{{ "vnda.com.br" | append: filename }}
```
{% endraw %}

***

## at_least

Limita um número a ter um valor mínimo.


<div class="code-example" markdown="1">

{{ 4 | at_least: 5 }}

</div>
{% raw %}
```liquid
{{ 4 | at_least: 5 }}
```
{% endraw %}

<div class="code-example" markdown="1">

{{ 4 | at_least: 3 }}

</div>
{% raw %}
```liquid
{{ 4 | at_least: 3 }}
```
{% endraw %}

***

## at_most

Limita um número a ter um valor máximo.

<div class="code-example" markdown="1">

{{ 4 | at_most: 5 }}

</div>
{% raw %}
```liquid
{{ 4 | at_most: 5 }}
```
{% endraw %}

<div class="code-example" markdown="1">

{{ 4 | at_most: 3 }}

</div>
{% raw %}
```liquid
{{ 4 | at_most: 3 }}
```
{% endraw %}

***

## capitalize

Transforma o primeiro caractere de uma string em maiúscula.

<div class="code-example" markdown="1">

{{ "título" | capitalize }}

</div>
{% raw %}
```liquid
{{ "título" | capitalize }}
```
{% endraw %}

`capitalize` só funciona no primeiro caractere de uma string, então outros caracteres não serão afetados.

<div class="code-example" markdown="1">

{{ "meu título longo" | capitalize }}

</div>
{% raw %}
```liquid
{{ "meu título longo" | capitalize }}
```
{% endraw %}

***

## ceil

Arredonda um número para o número inteiro mais próximo.

<div class="code-example" markdown="1">

{{ 1.2 | ceil }}

</div>
{% raw %}
```liquid
{{ 1.2 | ceil }}
```
{% endraw %}

<div class="code-example" markdown="1">

{{ 2.0 | ceil }}

</div>
{% raw %}
```liquid
{{ 2.0 | ceil }}
```
{% endraw %}

<div class="code-example" markdown="1">

{{ 183.357 | ceil }}

</div>
{% raw %}
```liquid
{{ 183.357 | ceil }}
```
{% endraw %}

O Liquid tenta converter o valor oferecido para um número antes de arredondá-lo.

<div class="code-example" markdown="1">

4

</div>
{% raw %}
```liquid
{{ "3.5" | ceil }}
```
{% endraw %}

***

## date

Converte um timestamp em um formato de data e hora legível. A sintaxe do formato é a mesma de [`strftime`](http://strftime.net/).

<div class="code-example" markdown="1">

Publicado em {{ "now" | date: "%d/%m/%Y às %H:%M" }}.

</div>
{% raw %}
```liquid

Publicado em {{ post.date | date: "%d/%m/%Y às %H:%M" }}

```
{% endraw %}

***

## default

Define um valor padrão caso o valor de um objeto não exista.

<div class="code-example" markdown="1">

Idioma: {{ lang | default: 'en-US' }}

</div>
{% raw %}
```liquid

Idioma: {{ lang | default: 'en-US' }}

```
{% endraw %}

Definindo um valor para o objeto, o valor de `default` será ignorado:

<div class="code-example" markdown="1">

{% assign lang = "pt-BR" %}

Idioma: {{ lang | default: 'en-US' }}

</div>
{% raw %}
```liquid

{% assign lang = "pt-BR" %}

Idioma: {{ lang | default: 'en-US' }}

```
{% endraw %}

***

## divided_by

Divide um número por outro número.

O resultado é arredondado (para baixo) em um número inteiro se o divisor for um número inteiro.

<div class="code-example" markdown="1">

{{ 16 | divided_by: 4 }}

</div>
{% raw %}
```liquid

{{ 16 | divided_by: 4 }}

```
{% endraw %}

<div class="code-example" markdown="1">

{{ 5 | divided_by: 3 }}

</div>
{% raw %}
```liquid

{{ 5 | divided_by: 3 }}

```
{% endraw %}

### Controlando o arredondamento
{: .no_toc }

`divided_by` produz um resultado do mesmo tipo do divisor — se você divide um número inteiro, o resultado será um número inteiro. Se você divide por um número racional (um número com um decimal), o resultado será um número racional.

Por exemplo, esse é um divisor com um número inteiro:

<div class="code-example" markdown="1">

{{ 20 | divided_by: 7 }}

</div>
{% raw %}
```liquid

{{ 20 | divided_by: 7 }}

```
{% endraw %} 

E aqui está um número racional:

<div class="code-example" markdown="1">

{{ 20 | divided_by: 7.0 }}

</div>
{% raw %}
```liquid

{{ 20 | divided_by: 7.0 }}

```
{% endraw %}

***

## downcase

Transforma todos os caracteres de uma string em minúscula.

<div class="code-example" markdown="1">

{{ "Vnda - Tecnologia em Ecommerce" | downcase }}

</div>
{% raw %}
```liquid

{{ "Vnda – Tecnologia em Ecommerce" | downcase }}

```
{% endraw %}

<div class="code-example" markdown="1">

{{ "maçãs" | downcase }}

</div>
{% raw %}
```liquid

{{ "maçãs" | downcase }}

```
{% endraw %}

***

## escape

Escapa uma string substituindo os caracteres por sequências de entidades (para que a string possa ser usada em um URL, por exemplo). Não muda strings que não têm nada para escapar.

<div class="code-example" markdown="1">

```
{{ "Você já leu 'James & O Pêssego Gigante'" | escape }}
```

</div>
{% raw %}
```liquid

{{ "Você já leu 'James & O Pêssego Gigante'" | escape }}

```

{% endraw %}

<div class="code-example" markdown="1">

```
{{ "Vnda - Tecnologia em Ecommerce" | escape }}
```

</div>
{% raw %}

```liquid

{{ "Vnda - Tecnologia em Ecommerce" | escape }}

```

{% endraw %}

***

## first

Retorna o primeiro item de um vetor.

<div class="code-example" markdown="1">

{{ "Diga aonde você vai, que eu vou varrendo" | split: " " | first }}

</div>
{% raw %}
```liquid

{{ "Diga aonde você vai, que eu vou varrendo" | split: " " | first }}

```
{% endraw %}

<div class="code-example" markdown="1">

{% assign my_array = "zebra, polvo, girafa, tigre" | split: ", " %}

{{ my_array.first }}

</div>
{% raw %}
```liquid

{% assign my_array = "zebra, polvo, girafa, tigre" | split: ", " %}

{{ my_array.first }}

```
{% endraw %}

Você também pode usar o `first` quando precisar usar o filtro dentro de uma tag:

<div class="code-example" markdown="1">

{% if my_array.first == "zebra" %}
  Essa é uma {{ my_array.first }}
{% endif %}

</div>
{% raw %}
```liquid

{% if my_array.first == "zebra" %}
  Essa é uma {{ my_array.first }}!
{% endif %}

```
{% endraw %}

***

## floor

Arredonda um valor para baixo, próximo ao menor número inteiro.

<div class="code-example" markdown="1">

{{ 1.2 | floor }}

</div>
{% raw %}
```liquid

{{ 1.2 | floor }}

```
{% endraw %}

<div class="code-example" markdown="1">

{{ 2.0 | floor }}

</div>
{% raw %}
```liquid

{{ 2.0 | floor }}

```
{% endraw %}

<div class="code-example" markdown="1">

{{ 183.357 | floor }}

</div>
{% raw %}
```liquid

{{ 183.357 | floor }}

```
{% endraw %}

O Liquid tentará converter o valor de entrada para um número antes do filtro ser aplicado.

<div class="code-example" markdown="1">

{{ "3.5" | floor }}

</div>
{% raw %}
```liquid

{{ "3.5" | floor }}

```
{% endraw %}

***

## join

Combina os itens de um array em uma string usando o argumento como um separador.

<div class="code-example" markdown="1">

{% assign beatles = "John, Paul, George, Ringo" | split: ", " %}

{{ beatles | join: " e "}}

</div>
{% raw %}
```liquid

{% assign beatles = "John, Paul, George, Ringo" | split: ", " %}

{{ beatles | join: " e "}}

```
{% endraw %}

***

## last

Retorna o último item de um vetor.

<div class="code-example" markdown="1">

{{ "Diga aonde você vai, que eu vou varrendo" | split: " " | last }}

</div>
{% raw %}
```liquid

{{ "Diga aonde você vai, que eu vou varrendo" | split: " " | last }}

```
{% endraw %}

<div class="code-example" markdown="1">

{% assign my_array = "zebra, polvo, girafa, tigre" | split: ", " %}

{{ my_array.last }}

</div>
{% raw %}
```liquid

{% assign my_array = "zebra, polvo, girafa, tigre" | split: ", " %}

{{ my_array.last }}

```
{% endraw %}

Você também pode usar o `last` quando precisar usar o filtro dentro de uma tag:

<div class="code-example" markdown="1">

{% if my_array.last == "tigre" %}
  Esse é um {{ my_array.last }}!
{% endif %}

</div>
{% raw %}
```liquid

{% if my_array.last == "tigre" %}
  Esse é um {{ my_array.last }}!
{% endif %}

```
{% endraw %}

***

## map

Cria um array de valores extraindo valores de uma propriedade de outro objeto.

Nesse exemplo, considere que o objeto `site.pages` possui todos os metadados de um website. Usando `assign` com o filtro `map` cria uma variável que contém apenas os valores das propriedades `category` em todo o objeto `site.pages` (o filtro [`compact`](#compact) remove os valores vazios de páginas que não possuem categorias):

<div class="code-example" markdown="1">

\- negócios

\- celebridades

\- estilo de vida

\- esportes

\- tecnologia

</div>
{% raw %}
```liquid

{% assign all_categories = site.pages | map: "category" | compact %}

{% for item in all_categories %}
- {{ item }}
{% endfor %}

```
{% endraw %}

***

## minus

Subtrai um número de outro número.

<div class="code-example" markdown="1">

{{ 4 | minus: 2 }}

</div>
{% raw %}
```liquid

{{ 4 | minus: 2 }}

```
{% endraw %}

<div class="code-example" markdown="1">

{{ 16 | minus: 4 }}

</div>
{% raw %}
```liquid

{{ 16 | minus: 4 }}

```
{% endraw %}

<div class="code-example" markdown="1">

{{ 183.357 | minus: 12 }}

</div>
{% raw %}
```liquid

{{ 183.357 | minus: 12 }}

```
{% endraw %}

***

## modulo

Retorna o resto de uma divisão.

<div class="code-example" markdown="1">

{{ 3 | modulo: 2 }}

</div>
{% raw %}
```liquid

{{ 3 | modulo: 2 }}

```
{% endraw %}

<div class="code-example" markdown="1">

{{ 24 | modulo: 7 }}

</div>
{% raw %}
```liquid

{{ 24 | modulo: 7 }}

```
{% endraw %}

<div class="code-example" markdown="1">

{{ 183.357 | modulo: 12 }}

</div>
{% raw %}

```liquid

{{ 183.357 | modulo: 12 }}

```

{% endraw %}

***

## plus

Soma um número com outro número.

<div class="code-example" markdown="1">

{{ 4 | plus: 2 }}

</div>
{% raw %}

```liquid

{{ 4 | plus: 2 }}

```

{% endraw %}

<div class="code-example" markdown="1">
  
{{ 16 | plus: 4 }}

</div>
{% raw %}

```liquid

{{ 16 | plus: 4 }}

```

{% endraw %}

<div class="code-example" markdown="1">
  
{{ 183.357 | plus: 12 }}

</div>
{% raw %}
```liquid

{{ 183.357 | plus: 12 }}

```
{% endraw %}

***

## prepend

Adiciona a string especificada no início de outra string.

<div class="code-example" markdown="1">
  
{{ "maçãs, laranjas e bananas" | prepend: "Algumas frutas: " }}

</div>
{% raw %}
```liquid

{{ "maçãs, laranjas e bananas" | prepend: "Algumas frutas: " }}

```
{% endraw %}

`prepend` também pode ser usado com variáveis:

<div class="code-example" markdown="1">
  
{% assign url = "vnda.com.br" %}

{{ '/index.html' | prepend: url }}

</div>
{% raw %}
```liquid

{% assign url = "vnda.com.br" %}

{{ '/index.html' | prepend: url }}

```
{% endraw %}

***

## remove

Remove todas as ocorrências da substring especificada em uma string.

<div class="code-example" markdown="1">
  
{{ "Metrô, diâmetro, metrônomo, centímetro" | remove: "metro" }}

</div>
{% raw %}
```liquid

{{ "Metrô, diâmetro, metrônomo, centímetro" | remove: "metro" }}

```
{% endraw %}

***

## replace

Substitui todas as ocorrências do primeiro argumento em uma string pelo segundo argumento.

<div class="code-example" markdown="1">
  
{{ "Há um quarto em frente e um quarto ao lado." | replace: "um quarto", "uma sala" }}

</div>
{% raw %}
```liquid

{{ "Há um quarto em frente e um quarto ao lado." | replace: "um quarto", "uma sala" }}

```
{% endraw %}

***

## reverse

Inverte a ordem dos itens de um vetor. `reverse` não pode inverter uma string.

<div class="code-example" markdown="1">
  
{% assign my_array = "maçãs, laranjas, pêras, abóboras" | split: ", " %}

{{ my_array | reverse | join: ", " }}

</div>
{% raw %}
```liquid

{% assign my_array = "maçãs, laranjas, pêras, abóboras" | split: ", " %}

{{ my_array | reverse | join: ", " }}

```
{% endraw %}

Para inverter uma string, é preciso primeiro separar ela em um vetor, inverter e uní-lo de novo.

<div class="code-example" markdown="1">
  
{{ "Diga aonde você vai, que eu vou varrendo." | split: "" | reverse | join: "" }}

</div>
{% raw %}
```liquid

{{ "Diga aonde você vai, que eu vou varrendo." | split: "" | reverse | join: "" }}

```
{% endraw %}

***

## round

Arredonda um número para o número inteiro mais próximo.

<div class="code-example" markdown="1">
  
{{ 1.2 | round }}

</div>
{% raw %}
```liquid

{{ 1.2 | round }}

```
{% endraw %}

<div class="code-example" markdown="1">
  
{{ 2.7 | round }}

</div>
{% raw %}
```liquid

{{ 2.7 | round }}

```
{% endraw %}

Se um número é passado por um argumento, o valor é arredondado para esse número de casas decimais.

<div class="code-example" markdown="1">
  
{{ 183.357 | round: 2 }}

</div>
{% raw %}
```liquid

{{ 183.357 | round: 2 }}

```
{% endraw %}

***

## size

Retorna o número de caracteres em uma string ou o número de itens em um vetor.

Remove todos os espaços em branco (tabs, espaços e novas linhas) do lado esquerdo de uma string. Não afeta os espaços entre palavras.

<div class="code-example" markdown="1">

{{ "Diga onde você vai que eu vou varrendo." | size }}

</div>
{% raw %}
```liquid

{{ "Diga onde você vai que eu vou varrendo." | size }}

```
{% endraw %}

<div class="code-example" markdown="1">

{% assign my_array = "maçãs, larangas, pêras, pêssegos" | split: ", " %}

{{ my_array.size }}

</div>
{% raw %}
```liquid

{% assign my_array = "maçãs, larangas, pêras, pêssegos" | split: ", " %}

{{ my_array.size }}

```
{% endraw %}

Você também pode usar o `size` como propriedade para usá-lo em uma tag:

{% raw %}
```liquid

{% if my_array.size >= 4 %}
  Várias frutas!
{% endif %}

```
{% endraw %}

***

## sort

Ordena os itens em um vetor em uma ordenação respeitando maiúsculas e minúsculas.

<div class="code-example" markdown="1">

{% assign my_array = "zebra, polvo, girafa, Tartaruga Marinha" | split: ", " %}

{{ my_array | sort | join: ", " }}

</div>
{% raw %}
```liquid

{% assign my_array = "zebra, polvo, girafa, Tartaruga Marinha" | split: ", " %}

{{ my_array | sort | join: ", " }}
```
{% endraw %}

Um argumento (opcional) especifica qual propriedade dos itens do array serão usados para a ordenação:

{% raw %}
```liquid

{% assign products_by_price = products | sort: "price" %}

{% for product in products_by_price %}
  {{ product.name }}
{% endfor %}

```
{% endraw %}

***

## split

Divide uma string em um array usando o argumento como separador. `split` é usado geralmente para converter itens separados por vírgulas de uma string em um array.

<div class="code-example" markdown="1">

{% assign beatles = "John, Paul, George, Ringo" | split: ", " %}

{% for member in beatles %}
  {{ beatles }}
{% endfor %}

</div>
{% raw %}
```liquid

{% assign beatles = "John, Paul, George, Ringo" | split: ", " %}

{% for member in beatles %}
  {{ member }}
{% endfor %}

```
{% endraw %}


***

## strip

Remove todos os espaços em branco (tabs, espaços e novas linhas) de ambos os lados de uma string. Isso não afeta os espaços entre palavras:

<div class="code-example" markdown="1">

```
{{ "                 Tanto espaço para se divertir!            " | strip }}
```
</div>
{% raw %}
```liquid

{{ "                 Tanto espaço para se divertir!            " | strip }}

```
{% endraw %}

***

## strip_html

Remove todas as tags HTML de uma string.

<div class="code-example" markdown="1">

{{ "<em>Você</em> já leu <strong>Dom Casmurro</strong>?" | strip_html }}

</div>
{% raw %}
```liquid

{{ "<em>Você</em> já leu <strong>Dom Casmurro</strong>?" | strip_html }}

```
{% endraw %}

***

## strip_newlines

Remove quaisquer novas linhas (`\n`) de uma string.

<div class="code-example" markdown="1">

{{ string_with_newlines | strip_newlines }}

</div>
{% raw %}
```liquid

{% capture string_with_newlines %}
Olá,
Mundo!
{% endcapture %}

{{ string_with_newlines | strip_newlines }}

```
{% endraw %}

***

## times

Multiplica um número por outro número.

<div class="code-example" markdown="1">

{{ 3 | times: 2 }}

</div>
{% raw %}
```liquid

{{ 3 | times: 2 }}

```
{% endraw %}

<div class="code-example" markdown="1">

{{ 24 | times: 7 }}

</div>
{% raw %}
```liquid

{{ 24 | times: 7 }}

```
{% endraw %}

<div class="code-example" markdown="1">

{{ 183.357 | times: 12 }}

</div>
{% raw %}
```liquid

{{ 183.357 | times: 12 }}

```
{% endraw %}

***

## truncate

Diminui uma string para o número de caracteres passados como um parâmetro. Se o número especificado é menor que o número de caracteres de uma string, reticências (…) serão adicionadas ao final e são incluídas na contagem de caracteres.

<div class="code-example" markdown="1">

{{ "Diga onde você vai, que eu vou varrendo." | truncate: 21 }}

</div>
{% raw %}
```liquid

{{ "Diga onde você vai, que eu vou varrendo." | truncate: 21 }}

```
{% endraw %}

### Reticências personalizadas
{: .no_toc }

`truncate` aceita um segundo parâmetro opcional que especifica a sequência de caracteres que devem ser adicionados à string truncada. Por padrão são elipses (…), mas você pode especificar uma sequência diferente.

O tamanho do segundo parâmetro conta contra o número de caracteres especificados no primeiro parâmetro. Por exemplo, se você quer truncar uma string em exatos 10 caracteres, e usa reticências de 3 caracteres (\.\.\.), use `13` como o primeiro parâmetro de `truncate`:

<div class="code-example" markdown="1">

{{ "Diga onde você vai, que eu vou varrendo." | truncate: 21, ", e tal" }}

</div>
{% raw %}
```liquid

{{ "Diga onde você vai, que eu vou varrendo." | truncate: 21, ", e tal" }}

```
{% endraw %}

### Sem reticências
{: .no_toc }

Você pode truncar uma string para o número de caracteres especificado passando uma string vazia como segundo parâmetro:

<div class="code-example" markdown="1">

{{ "Diga onde você vai, que eu vou varrendo." | truncate: 21, "" }}

</div>
{% raw %}
```liquid

{{ "Diga onde você vai, que eu vou varrendo." | truncate: 21, "" }}

```
{% endraw %}

***

## truncatewords

Diminui uma string para o número de palavras passados como um parâmetro. Se o número especificado é menor que o número de palavras de uma string, reticências (…) serão adicionadas ao final e são incluídas na contagem de caracteres (as regras de reticências são as mesmas de [`truncate`](#truncate)).


<div class="code-example" markdown="1">

{{ "Diga onde você vai, que eu vou varrendo." | truncatewords: 4 }}

</div>
{% raw %}
```liquid

{{ "Diga onde você vai, que eu vou varrendo." | truncatewords: 4 }}

```
{% endraw %}

***

## uniq

Remove itens duplicados de um vetor.

<div class="code-example" markdown="1">

{% assign my_array = "formigas, insetos, abelhas, insetos, formigas" | split: ", " %}

{{ my_array | uniq | join: ", " }}

</div>
{% raw %}
```liquid

{% assign my_array = "formigas, insetos, abelhas, insetos, formigas" | split: ", " %}

{{ my_array | uniq | join: ", " }}

```
{% endraw %}

***

## upcase

Transforma todos os caracteres de uma string em minúscula.

<div class="code-example" markdown="1">

{{ "Vnda – Tecnologia em Ecommerce" | upcase }}

</div>
{% raw %}
```liquid

{{ "Vnda – Tecnologia em Ecommerce" | upcase }}

```
{% endraw %}

<div class="code-example" markdown="1">

{{ "MAÇÃS" | upcase }}

</div>
{% raw %}
```liquid

{{ "MAÇÃS" | upcase }}

```
{% endraw %}

***

## url_decode

Decodifica uma string que foi codificada como uma URL ou através de [`url_encode`](#url_encode).

<div class="code-example" markdown="1">

{{ "%27Pare%21%27+disse+Chico" | url_decode }}

</div>
{% raw %}
```liquid

{{ "%27Pare%21%27+disse+Chico" | url_decode }}

```
{% endraw %}

***

## url_encode

Converte caracteres de uma string para caracteres compatíveis com URLs:

<div class="code-example" markdown="1">

{{ "ajuda@vnda.com.br" | url_encode }}

</div>
{% raw %}
```liquid

{{ "ajuda@vnda.com.br" | url_encode }}

```
{% endraw %}
