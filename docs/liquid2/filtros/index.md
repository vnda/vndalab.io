---
layout: default
title: Filtros
parent: Liquid 2
nav_order: 4
has_children: true
---

# Filtros Vnda
{: .no_toc }

Para os filtros nativos oferecidos pelo Liquid, confira [esse artigo]({%- link docs/liquid2/filtros/nativos.md -%}).

## Conteúdos
{: .no_toc .text-delta }

1. TOC
{:toc}

***

## apply_discount e installments

O installments retorna o valor da parcela do produto conforme a quantidade máxima de parcelas. Já o apply_discount é usado em conjunto para considerar o preço promocional do produto caso tenha promoção. Exemplo de uso comum:

{% raw %}
```liquid
{% if product.installments.size > 1 %}
  <p class="parcels">
    {% if product.on_sale %}
      ou {{ product.installments.size }}x de {{ product | apply_discount | installments | money_format }}
    {% else %}
      ou {{ product.installments.size }}x de {{ product.price | installments | money_format }}
    {% endif %}
  </p>
{% endif %}
```
{% endraw %}

## client_price

Esse filtro é utilizado para criar promoções para clientes categorizados por alguma tag de cliente.

{% raw %}
```liquid
{% assign client_price = product | client_price %}
{% assign product_price = product.price %}
{% if client_price != product_price %}
  {{ product | client_price | money_format }}
  {{ product_price | money_format }}
{% else %}
  {{ product | client_price | money_format }}
{% endif %}
```
{% endraw %}

## json
{: .d-inline-block}

Filtro utilizado para passar passar um objeto liquid no formato objeto json

{% raw %}

```liquid
  {% capture javascripts %}
    <script>
      variants_products.push({{ product.variants | json }});
    </script>
  {% endcapture %}
```

{% endraw %}

## group_images_by_url

Agrupa as imagens do produto pela url. Exemplo de uso:

{% raw %}
```liquid
{% assign product_images = product.images | group_images_by_url %}

{% for image in product_images %}
  {{ image }}
{% endfor %}
```
{% endraw %}

## max_with_interest e max_without_interest

Retorna as parcelas do preço do produto com e sem juros (baseado nos métodos de pagamento da loja). Exemplo de uso:

{% raw %}
```liquid
{% assign installments_without_insterest = product.installments | max_without_interest %}
{% assign installments_with_insterest = product.installments | max_with_interest %}

{{ installments_without_insterest.number }}x de {{ installments_without_insterest.price | money_format }} sem juros
ou {{ installments_with_insterest.number }}x de {{ installments_with_insterest.price | money_format }} com juros
```
{% endraw %}

## money_format

Formata um valor para dinheiro com R$.

{% raw %}
```liquid
{{ product.price | money_format }}
```
{% endraw %}

## plural

Aplica plural ou singular a partir de um número.

{% raw %}
```liquid
{{ tag.products_count | plural: "item", "itens" }}
```
{% endraw %}

## resize

Redimensiona uma imagem cadastrada no admin. Exemplos de uso:

{% raw %}
```liquid
<img src="{{ image.url | resize: "50x50" }}" />
<img src="{{ image.url | resize: "50x" }}" />
<img src="{{ image.url | resize: "x50" }}" />
```
{% endraw %}
