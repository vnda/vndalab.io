---
layout: default
title: Pasta liquids
parent: Estrutura Padrão
grand_parent: Ambiente
nav_order: 2
---

# Pasta liquids

Essa documentação foi migrada e reestruturada para nosso novo portal de documentações. Você pode acessá-la [aqui](https://developers.vnda.com.br/docs/liquids).