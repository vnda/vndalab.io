---
layout: default
title: Estrutura Padrão
parent: Ambiente
nav_order: 3
has_children: true
---

# Estrutura Padrão

Essa documentação foi migrada e reestruturada para nosso novo portal de documentações. Você pode acessá-la [aqui](https://developers.vnda.com.br/docs/estrutura-organizacional-dos-arquivos-1).