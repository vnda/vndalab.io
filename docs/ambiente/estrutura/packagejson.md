---
layout: default
title: package.json
parent: Estrutura Padrão
grand_parent: Ambiente
nav_order: 4
---

# package.json

Essa documentação foi migrada e reestruturada para nosso novo portal de documentações. Você pode acessá-la [aqui](https://developers.vnda.com.br/docs/arquivo-packagejson).