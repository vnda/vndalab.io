---
layout: default
title: Pasta assets
parent: Estrutura Padrão
grand_parent: Ambiente
nav_order: 1
---

# Pasta assets

Essa documentação foi migrada e reestruturada para nosso novo portal de documentações. Você pode acessá-la [aqui](https://developers.vnda.com.br/docs/pasta-assets).