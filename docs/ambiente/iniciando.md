---
layout: default
title: Iniciando um projeto
parent: Ambiente
nav_order: 2
---

# Iniciando um projeto

Essa documentação foi migrada e reestruturada para nosso novo portal de documentações. Você pode acessá-la [aqui](https://developers.vnda.com.br/docs/iniciar-customizacao).