---
layout: default
title: home.liquid
parent: Templates
grand_parent: Liquid 4
nav_order: 2
---

# home.liquid

Essa documentação foi migrada e reestruturada para nosso novo portal de documentações. Você pode acessá-la [aqui](https://developers.vnda.com.br/docs/homeliquid).