---
layout: default
title: page.liquid
parent: Templates
grand_parent: Liquid 4
nav_order: 9
---

# page.liquid

Essa documentação foi migrada e reestruturada para nosso novo portal de documentações. Você pode acessá-la [aqui](https://developers.vnda.com.br/docs/pageliquid).