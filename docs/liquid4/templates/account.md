---
layout: default
title: account.liquid
parent: Templates
grand_parent: Liquid 4
nav_order: 6
---

# account.liquid

Essa documentação foi migrada e reestruturada para nosso novo portal de documentações. Você pode acessá-la [aqui](https://developers.vnda.com.br/docs/accountliquid).