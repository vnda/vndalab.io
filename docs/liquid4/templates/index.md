---
layout: default
title: Templates
parent: Liquid 4
nav_order: 5
has_children: true
---

# Templates
{: .no_toc }

As lojas da Vnda são feitas de arquivos de template Liquid, cada uma servindo a um propósito específico. Por exemplo, `tag.liquid` é usada para exibir uma coleção de produtos, enquanto `product.liquid` é usada para mostrar os detalhes de um produto só.
