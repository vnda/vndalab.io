---
layout: default
title: layout.liquid
parent: Templates
grand_parent: Liquid 4
nav_order: 1
---

# layout.liquid

Essa documentação foi migrada e reestruturada para nosso novo portal de documentações. Você pode acessá-la [aqui](https://developers.vnda.com.br/docs/layoutliquid).