---
layout: default
title: Tipos de dados
parent: Básico
grand_parent: Liquid 4
nav_order: 3
---

# Tipos de dados

Essa documentação foi migrada e reestruturada para nosso novo portal de documentações. Você pode acessá-la [aqui](https://developers.vnda.com.br/docs/tipos-de-dados-e-opera%C3%A7%C3%B5es#tipos-de-dados).