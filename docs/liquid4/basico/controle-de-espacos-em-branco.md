---
layout: default
title: Controle de espaços em branco
parent: Básico
grand_parent: Liquid 4
nav_order: 4
---

# Controle de espaços em branco

Essa documentação foi migrada e reestruturada para nosso novo portal de documentações. Você pode acessá-la [aqui](https://developers.vnda.com.br/docs/tipos-de-dados-e-opera%C3%A7%C3%B5es#espa%C3%A7os-em-branco).
