---
layout: default
title: Básico
parent: Liquid 4
nav_order: 1
has_children: true
---

# Noções Básicas

Essa documentação foi migrada e reestruturada para nosso novo portal de documentações. Você pode acessá-la [aqui](https://developers.vnda.com.br/docs/no%C3%A7%C3%B5es-b%C3%A1sicas).