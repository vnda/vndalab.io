---
layout: default
title: Verdadeiro e falso
parent: Básico
grand_parent: Liquid 4
nav_order: 2
---

# Verdadeiro e falso

Essa documentação foi migrada e reestruturada para nosso novo portal de documentações. Você pode acessá-la [aqui](https://developers.vnda.com.br/docs/tipos-de-dados-e-opera%C3%A7%C3%B5es#verdadeiro-e-falso).