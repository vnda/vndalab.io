---
layout: default
title: render
parent: Tags
grand_parent: Liquid 4
nav_order: 6
---

# render
{: .no_toc .d-inline-block}

## Neste artigo:
{: .no_toc .text-delta }

1. TOC
{:toc}

***

Na versão 4 do Liquid, a tag `{% raw %}{% include %}{% endraw %}` foi descontinuada. Para substituí-la foi criada a tag `{% raw %}{% render %}{% endraw %}`.
Exemplo de uso:

{% raw %}
```liquid
{% render "partials/common/footer" %}
```
{% endraw %}

Observe que você não precisa escrever a extensão `.liquid` do arquivo.

Quando um **render** é renderizado, o código dentro dele não tem acesso automático às variáveis atribuídas usando tags de variável no template pai do **render**. Da mesma forma, as variáveis atribuídas dentro do snippet não podem ser acessadas pelo código fora do snippet. Esse encapsulamento aumenta o desempenho e ajuda a tornar o código do template mais fácil de entender e manter.


## Passando variáveis para um render

Variáveis atribuídas usando tags de variáveis podem ser passadas para um render listando-as como parâmetros na tag de renderização:

{% raw %}
```liquid
{% assign bar = "string" %}
{% render "partials/common/footer", foo: bar %}
```
{% endraw %}

## Parâmetro with

Um único objeto pode ser passado para um snippet usando os parâmetros `with` e `as`:

{% raw %}
```liquid
{% assign featured_product = all_products["product_handle"] %}
{% render "product" with featured_product as product %}
```
{% endraw %}

No exemplo acima, a variável `product` no render conterá o valor de `feature_product` no template pai.

## Parâmetro for

Um render pode ser renderizado uma vez para cada valor de um objeto enumerável usando os parâmetros `for` e `as`:

{% raw %}
```liquid
{% assign variants = product.variants %}
{% render "variant" for variants as variant %}
```
{% endraw %}

No exemplo acima, o render será renderizado uma vez para cada variante do produto, e a variável `variant` conterá um objeto de variante do produto dentro do render.

Ao usar o parâmetro `for`, o objeto `forloop` pode ser acessado no render.
