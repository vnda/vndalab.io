---
layout: default
title: Loja
parent: Tags
grand_parent: Liquid 4
nav_order: 6
---

# Tags de Loja

Essa documentação foi migrada e reestruturada para nosso novo portal de documentações. Você pode acessá-la [aqui](https://developers.vnda.com.br/docs/tags-1).
