---
layout: default
title: Variáveis
parent: Tags
grand_parent: Liquid 4
nav_order: 5
---

# Tags de variáveis

Essas tags são nativas do Liquid. Mais detalhes na [documentação oficial](https://shopify.github.io/liquid4/tags/variable/).

## assign
{: .d-inline-block}

Tag de variável sem corpo.

## capture
{: .d-inline-block}

Tag de variável com corpo.

## increment
{: .d-inline-block}

Tag de variável para incrementar números.

## decrement
{: .d-inline-block}

Tag de variável para diminuir números.
