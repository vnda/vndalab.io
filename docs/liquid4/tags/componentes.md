---
layout: default
title: Componentes
parent: Tags
grand_parent: Liquid 4
nav_order: 1
---


# Componentes

Essa documentação foi migrada e reestruturada para nosso novo portal de documentações. Você pode acessá-la [aqui](https://developers.vnda.com.br/docs/componentes-em-react).