---
layout: default
title: Tags
parent: Liquid 4
nav_order: 2
has_children: true
---

# Tags
{: .no_toc }

***


Tags operam a lógica que diz o que os templates devem fazer. Tags são demarcadas com caracteres {% raw %}`{% %}`{% endraw %}.

Algumas tags, como [`for`]({%- link docs/liquid4/tags/iteracao.md -%}#for) e [`cycle`]({%- link docs/liquid4/tags/iteracao.md -%}}#cycle) podem receber parâmetros. Outras, como [`assign`]({%- link docs/liquid4/tags/variaveis.md -%}}#assign), podem receber filtros.
