---
layout: default
title: load_menus
parent: Tags
grand_parent: Liquid 4
---

# load_menus

Essa documentação foi migrada e reestruturada para nosso novo portal de documentações. Você pode acessá-la [aqui](https://developers.vnda.com.br/docs/load_menus).