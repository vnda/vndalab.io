---
layout: default
title: load_customizations
parent: Tags
grand_parent: Liquid 4
nav_order: 7
---

# load_customizations

Essa documentação foi migrada e reestruturada para nosso novo portal de documentações. Você pode acessá-la [aqui](https://developers.vnda.com.br/docs/load_customizations).