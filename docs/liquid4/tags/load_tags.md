---
layout: default
title: "load_tags e load_tag"
parent: Tags
grand_parent: Liquid 4
nav_order: 8
---

# load_tags e load_tag

Essa documentação foi migrada e reestruturada para nosso novo portal de documentações. Você pode acessá-la [aqui](https://developers.vnda.com.br/docs/load_tag-e-load_tags).