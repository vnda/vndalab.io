---
layout: default
title: Filtros
parent: Liquid 4
nav_order: 4
has_children: true
---

# Filtros Vnda

Essa documentação foi migrada e reestruturada para nosso novo portal de documentações. Você pode acessá-la [aqui](https://developers.vnda.com.br/docs/filtros-1).