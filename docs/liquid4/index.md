---
layout: default
title: Liquid 4
nav_order: 4
has_children: true
---

# Liquid
{: .no_toc }

Essa documentação foi migrada e reestruturada para nosso novo portal de documentações. Você pode acessá-la [aqui](https://developers.vnda.com.br/docs/funcionamento-do-liquid).

## Versões

- Liquid v2.6.3
{: .d-inline-block}

Suportada
{: .label .label-yellow }

Possuímos suporte, contudo muitos recursos que estão disponíveis no Liquid v4 não estão disponíveis nessa versão.

- [Liquid v4.0.3](https://github.com/Shopify/liquid4/tree/1feaa6381300d56e2c71b49ad8fee0d4b625147b)
{: .d-inline-block}

Atual
{: .label .label-green }

Essa é a versão atual e a que está recebendo os últimos recursos implementados pela plataforma.
