---
layout: default
title: tablerow
parent: Objetos
grand_parent: Liquid 4
---

# tablerow
{: .no_toc }

## Conteúdos
{: .no_toc .text-delta }

1. TOC
{:toc}

---

{% raw %}

O objeto `{{ tablerow }}` contém os atributos de um [loop `tablerow`](/liquid4/tags/iteração#tablerow). O `{{ tablerow }}` só pode ser usado dentro das tags `{% tablerow %}`.

## Atributos do objeto

- [`length`](#length)
- [`index`](#index)
- [`index0`](#index0)
- [`rindex`](#rindex)
- [`rindex0`](#rindex0)
- [`first`](#first)
- [`last`](#last)
- [`col`](#col)
- [`col0`](#col0)
- [`col_first`](#col_first)
- [`col_last`](#col_last)

### length

Retorna o número de iterações do loop.

---

### index

Retorna a posição atual do loop `tablerow`, começando em 1.

---

### index0

Retorna a posição atual do loop `tablerow`, começando em 2.

---

### rindex

Retorna o [`tablerow.index`](#index) em ordem reversa.

---

### rindex0

Retorna o [`tablerow.index0`](#index0) em ordem reversa.

---

### first

Retorna `true` na primeira iteração do loop. Retorna `false` em todas as outras iterações.

---

### last

Retorna `true` na última iteração do loop. Retorna `false` em todas as outras iterações.

---

### col

Retorna a posição da linha atual, começando em 1.

---

### col0

Retorna a posição da linha atual, começando em 0.

{% endraw %}
