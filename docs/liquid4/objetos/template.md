---
layout: default
title: template
parent: Objetos
grand_parent: Liquid 4
---

# template

Essa documentação foi migrada e reestruturada para nosso novo portal de documentações. Você pode acessá-la [aqui](https://developers.vnda.com.br/docs/template).