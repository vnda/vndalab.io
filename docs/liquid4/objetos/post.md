---
layout: default
title: post
parent: Objetos
grand_parent: Liquid 4
search_exclude: true
---

# post
{: .no_toc }

## Conteúdos
{: .no_toc .text-delta }

1. TOC
{:toc}

---

{% raw %}

O objeto `{{ post }}` é usado dentro de uma tag `{% load_media %}` para exibir o conteúdo de uma integração de loja, como o Cockpit.

###### media/blog_post.liquid

```liquid
{% media from:"blog" username:"blogdaloja.blog.com" api_key:"blog_app_api_key" %}
  <article class="tumblr-{{ post.type }}">
    <header>
      <h1>
        <a href="{{ post.url }}" rel="bookmark">
          {{ post.title }}
        </a>
      </h1>
      
      <p>
        <time datetime="{{ post.timestamp }}">{{ post.timestamp | date: "%d/%m/%Y" }}</time>
      </p>
    </header>
    
    {% if post.image %}
      <figure>
        <img src="{{ post.image }}" alt="{{ post.title }}" />
      </figure>
    {% endif %}
  
    <div class="content">
      {{ post.body }}
    </div>
  </article>
{% endmedia %}
```

## Atributos do objeto

- [`body`](#body)
- [`image`](#image)
- [`permalink`](#permalink)
- [`summary`](#summary)
- [`tags`](#tags)
- [`timestamp`](#timestamp)
- [`title`](#title)
- [`type`](#type)
- [`url`](#url)

### body

Retorna o corpo do post, em HTML.

---

### image

Retorna a URL da imagem de destaque.

---

### permalink

Retorna o link permanente do post no Tumblr.

---

### summary

Retorna o resumo do post.

---

### tags

Retorna uma lista de tags relacionadas ao post. Você pode controlar a exibição usando filtros de array.

---

### timestamp

Retorna a data absoluta de publicação do post. Você pode controlar a exibição usando o `date`.

---

### title

Retorna o título do post.

---

### type

Retorna o formato do post. Útil para controlar a exibição dos formatos do Tumblr.

###### tumblr_post.liquid

```liquid
{% if post.type == "image" %}
  <figure class="tumblr-{{ post.type }}">
    <a href="{{ post.url }}" rel="bookmark">
      <img src="{{ post.image }}" alt="{{ post.title }}" />
    </a>
  </figure>
{% else %}
  <!-- Estrutura normal de um post -->
{% endif %}
```

---

### url

Retorna a URL do post dentro da loja.

{% endraw %}
