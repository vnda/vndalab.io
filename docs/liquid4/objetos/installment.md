---
layout: default
title: installment
parent: Objetos
grand_parent: Liquid 4
search_exclude: true
---

# installment
{: .no_toc }

## Conteúdos
{: .no_toc .text-delta }

1. TOC
{:toc}

---

{% raw %}

O objeto `{{ installment }}` é usado dentro da tag [`{% installments %}`](/liquid4/tags/loja#installments) para exibir informações de parcelamento de um produto.

###### product.liquid

```liquid
{% installments product_id: product.id %}
  {% if forloop.last %}
    <p>em até 
      <span>{{ installment.number }}x</span> 
      de <strong>{{ installment.price | money_format }}</strong>
      {% unless installment.interest %}(sem juros){% endunless %}</p>
  {% endif %}
{% endinstallments %}
```

```html
<p>em até <span>6x</span> de <strong>R$ 499,83</strong> (sem juros)</p>
```

## Atributos do objeto

- [`interest`](#interest)
- [`interest_rate`](#interest_rate)
- [`number`](#number)
- [`price`](#price)
- [`total`](#total)

### interest

Retorna `true` se a opção de parcelamento inclui juros. Retorna `false` se não inclui juros.

---

### interest_rate

Retorna o valor da taxa de juros aplicado sobre o valor do produto. Retorna 0 se a parcela não inclui juros.

---

### number

Retorna o número de parcelas da opção de parcelamento. Você pode pegar o maior número de parcelas usando [`forloop.last`](/liquid4/objetos/forloop#last).

---

### price

Retorna o preço da parcela da opção de parcelamento.

---

### total

Retorna o valor total a ser pago nessa opção de parcelamento. Útil quando houver juros aplicados.

{% endraw %}
