---
layout: default
title: video
parent: Objetos
grand_parent: Liquid 4
---

# video
{: .no_toc }

## Conteúdos
{: .no_toc .text-delta }

1. TOC
{:toc}

---

{% raw %}

O objeto `{{ video }}` é usado com a tag `{% load_videos %}` para exibir os vídeos de um produto na página de produto.

###### product.liquid

```liquid
{% videos product_id: product.id %}
  <div class="video-wrapper">
    <iframe src="{{ video.embed_url }} width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
  </div>
  
  {% if video.url contains "vimeo" %}
    {% assign video_provider = "Vimeo" %}
  {% elsif video.url contains "youtube" or video.url contains "youtu.be" %}
    {% assign video_provider = "YouTube" %}
  {% enduf %}
  
  <p>Assista esse vídeo no <a href="{{ video.url }}">{{ video_provider }}</a>.</p>
{% endvideos %}
```

## Atributos do objeto

- [`embed_url`](#embed_url)
- [`thumbnail_url`](#thumbnail_url)
- [`updated_at`](#updated_at)
- [`url`](#url)


### embed_url 

Retorna a URL para incorporar o vídeo em uma página através de uma tag HTML `<iframe>`.
  
---

### thumbnail_url

Retorna a URL da imagem de prévia do vídeo.

---

### updated_at

Retorna a data absoluta da última modificação do vídeo na plataforma da Vnda.

---

### url

Retorna a URL da página do vídeo no provedor (Vimeo ou YouTube).

{% endraw %}
