---
layout: default
title: current_shop
parent: Objetos
grand_parent: Liquid 4
---

# current_shop

Essa documentação foi migrada e reestruturada para nosso novo portal de documentações. Você pode acessá-la [aqui](https://developers.vnda.com.br/docs/current_shop).