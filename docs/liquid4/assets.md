---
layout: default
title: Assets
parent: Liquid 4
nav_order: 6
---

# Assets

## Tags de assets

Essa documentação foi migrada e reestruturada para nosso novo portal de documentações. Você pode acessá-la [aqui](https://developers.vnda.com.br/docs/tags-de-assets).

## Filtros de assets

### image_path

Essa documentação foi migrada e reestruturada para nosso novo portal de documentações. Você pode acessá-la [aqui](https://developers.vnda.com.br/docs/filtros-1#filtros-de-assets).
