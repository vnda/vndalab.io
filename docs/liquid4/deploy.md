---
layout: default
title: Processo de deploy
parent: Liquid 4
nav_order: 7
---

# Processo de deploy

Essa documentação foi migrada e reestruturada para nosso novo portal de documentações. Você pode acessá-la [aqui](https://developers.vnda.com.br/docs/iniciar-customizacao#production).

