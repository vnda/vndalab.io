---
layout: default
title: Migrando da v2 para a v4
parent: Liquid 4
---

# Migrando da v2.6.3 para a v4.0.3

Para migrar a versão do Liquid primeiramente precisamos solicitar para a equipe de back-end a migração da linguagem. Depois, precisamos atualizar as tags, objetos e filtros, conforme abaixo:

- include => render
- capture css_above / capture css_page => head
- capture javascripts / capture js_page => body

***

- store => current_shop

***

- json => to_json

***

- cartitems => cart.items
- banners => load_banners
- menus => load_menus
- tag => load_tag
- tags => load_tags
- credits => load_credits
- load_products => pode precisar do getparam (ver doc)
- media => load_media
- urlfor => facebook_connect_url
- videos => load_videos
- bandeiras de pagamento => load_payment_icons
