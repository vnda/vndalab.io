---
layout: default
title: Glossário
nav_order: 8
has_children: false
---

# Glossário
{: .no_toc }

## Conteúdos
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Admin:
Seção gerencial do canal ecommerce, acessível adicionando o caminho /admin no endereço da loja.

## Adquirentes:
Empresas que fazem a liquidação financeira das transações realizadas através de cartão de crédito e débito, como Cielo, Stone, etc.

## Ativação:
Ações e recursos relacionados a gerar visitas no ecommerce.  

## Atributos:
Possíveis variações entre produtos, pode variar entre diversas lojas, sendo os mais comuns 'cor' e 'tamanho'. Cada versão dos produtos com atributos específicos, tratamos como 'variantes'. 

## Canal Ecommerce: 
Conjunto recursos, principalmente 'interface' e 'admin', que permitem a operação do varejo online. 

## Carrinhos: 
Registro de consumidor que tomou a ação de adicionar itens ao 'carrinho de compras' sem ter concluído o pedido. A listagem de 'Carrinhos' está disponível no Admin. 

## Categoria: 
Uma categoria consiste num agrupamento qualquer de produtos, que gerencia-se na Vnda em por 'tags' e gera 'listagens' de produtos. 

## Checkout: 
Consiste nas etapas entre abertura de 'carrinho' e conclusão do 'pedido', suas etapas contemplam 'dados pessoas', 'entrega' e 'pagamento'. 

## Cliente: 
São os negócios e marcas atendidos pela Vnda.  

## Consumidor: 
São as pessoas, física ou jurídica, que consomem em canais ecommerce operados pela Vnda. 

## Conversão: 
Indicador de performance em vendas calculado pela razão entre pedidos e visitas no mesmo período no ecommerce. 

## Credenciamento: 
Processo administrativo de filiação em determinado parceiro, geralmente usado em adquirentes. 

## Customização: 
Alteração de algum fluxo ou processo para atendimento de cliente. (Diferente de 'personalização).

## Engajamento: 
entendemos engajamento como as ações associadas no canal ecommerce para gerar intenção de compra. 

## Estilo: 
Consiste em regras de design que em conjunto consistem no estilo a ser seguido, composto principalmente por: paleta de cores, fontes e texturas. 

## Etiquetas: 
São arquivos prontos para serem impressos associados ao processo de expedição e entrega dos pedidos. 

## Flag: 
Sinalizações em produtos que indicam algum destaque do produto como 'promoção' ou 'lançamento', podem ser texto ou imagem. 

## Formulários: 
Recursos nativos da internet que atuam no envio de emails automáticos. 

## Funcionalidades: 
Um fluxo ou processo que utilize recurso ou recursos do software, consistem em funcionalidades. 

## Interface: 
Combinação de arquivos de 'front-end', HTML/CSS/JS, que criam a estrutura das telas. Seu resultado é a frete de loja do ecommerce. 

## Layout: 
Arquivo visual de imagem criado para demonstrar o resultado futuro da tela. 

## Mídias: 
Formato de veiculação de arquivos (imagem, foto ou video) disponibilizados para utilização como conteúdo na Interface do ecommerce, o mais comum são os banners.
Notificações

## Página: 
São telas de conteúdo usadas tipicamente usadas e 'Sobre a marca', 

## Pedido Cancelado: 
Status dos pedidos que por qualquer razão não serão faturados nem expedidos, por qualquer razão pagamento, falta de produto, indicação da cliente ou outra. 

## Pedido Confirmado (ou Aprovado): 
Status que identifica os pedidos que passaram pela verificação de pagamento e outras, caso existentes, estando prontos para faturamento e expedição. 

## Pedido Recebido (ou Pendente): 
Status do pedido enquanto ele foi 'concluído' pelo 'consumidor' e ainda não foi 'confirmado' ou 'cancelado'. 

## Personalização: 
Edição de estilo e conteúdos para adequação para cliente ou objetivo específico, sem alterar nenhum recurso da plataforma (ver customização). 

## Plataforma: 
Combinação funcionalidades disponíveis no Admin e Interface que consistem na solução completa.  

## Production: 
Ambiente em que o canal ecommerce está ativo, antes desse ambiente, termos o ambiente de 'Staging'. 

## Recursos: 
Regras programadas no sistema da plataforma de ecommerce que viabilizam de forma isolada ou combinada funcionalidades. 

## Relacionamento: 
Ações diversas realizadas com a base de clientes direcionada para realização de recompra ou recorrência. 

## Setup: 
Projeto com objetivo de lançamento de um ecommerce, iniciando-se com uma etapa de Briefing. Pode ser uma ecommerce novo ou a troca de interface de um ecommerce existente. 

## Staging: 
Ambiente de desenvolvimento ou homologação do ecommerce, em que ocorre o desenvolvimento do site e cadastros antes do lançamento. Acessível pelo subdomínio de vnda.com.br.

## Tag: 
Agrupamento diversos de produtos ou listagem de produtos, usados na gestão do canal ecommerce e navegação também. 

## Tela: 
Aplicação do layout na interface, não deve ser confundido com 'página'. 

## Usuário: 
Credenciais de acesso ao Admin. 
