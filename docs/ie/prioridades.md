---
layout: page
title: Prioridades
parent: Fluxo de Trabalho
grand_parent: Interface Experience
nav_order: 4
---

# Prioridades

Prioridades são demandas que atingem a pauta com um grau maior de importância no tempo de entrega. Esses cards podem “quebrar” a pauta de desenvolvimento, fazendo outros cards serem pausados para que as prioridades sejam atendidas.

É importante salientar que, embora as prioridades possam fazer outros cards serem pausados para que elas sejam atendidas, a prioridade em si não pode ser pausada. O desenvolvedor se envolve com ela por todo o período de desenvolvimento, até entregar uma correção ou implementação nova.

Prioridades são definidas e reportadas pelo responsável pela organização da pauta no atendimento de Interface Experience, que pode definir entre dois níveis de prioridade, identificadas por etiquetas no card da solicitação:

***

## 🆘 Urgente

Solicitações que precisam ser desenvolvidas assim que reportadas pelo responsável pela pauta. O desenvolvedor deve parar o desenvolvimento para atendê-la, e esse card não pode ser interrompido por outra prioridade.

Solicitações consideradas Urgentes são:

- Erros que impedem a compra.
- Erros que enviam informações incorretas ao carrinho.
- Erros que impedem seleção de atributos.
- Erros que mostram informações incorretas de estoque.
- Erros que mostram valores errados na página de produto.
- Quebra grave de layout (home, produto ou listas quebradas, que não permitem concluir uma compra).
- Erros de publicação.

***

## ⚠️ Importante

Solicitações que precisam ser atendidas assim que um desenvolvedor finalizar uma tarefa. Esse card não pode ser interrompido por outra prioridade.

Solicitações consideradas Importantes são:

- Demandas de lojas Etail.
- Quebra grave de layout (home, produto ou listas quebradas, mas que não impedem de concluir uma compra).
- Cards publicados que voltaram com modificações (**Voltou (Hello Again)**).
