---
layout: page
title: Registro de entregas
parent: Fluxo de Trabalho
grand_parent: Interface Experience
nav_order: 3
---

# Registro de entregas
{: .no_toc}

Quando um card é movido da coluna **Doing** para **Review** na [pauta]({% link docs/ie/fluxo-de-trabalho.md %}), ele deve possuir um registro da entrega, que lista as modificações de cadastro e de arquivos demandadas pelo card.

Vamos explicar cada item que deve ser registrado a seguir:

1. TOC
  {:toc}
 
***

### Modelo do registro

O registro de entrega deve adicionado como um comentário no card da solicitação no Trello, seguindo esse modelo:

```markdown
## Checklist {Versão}

***

### Cadastros

- [{Página da wiki}]({URL})

***

### Exemplos para teste

- [{Página de teste (Home/Tag/Produto/Institucional/etc.)}]({URL})

*** 

### Commits relacionados

- [{Hash do commit}](https://gitlab.com/vnda/setup/{repositório do projeto}/commit/{hash do commit})
```

Textos entre `{Chaves}` denota, um modelo. Por exemplo, substitua `{Versão}` por `1.0`, `2.0`, etc.

***
 
### Versão do Checklist

O registro da entrega deve deixar claro sobre qual checklist especificamente aquela entrega se refere, por isso é importante começar [indicando a versão]({% link docs/ie/fluxo-de-trabalho.md %}#checklists-de-solicitações) (ou, em checklists não versionados, o nome do checklist).

***

### Cadastros

Sempre que um card demanda alteração de cadastro na loja (como novas posições de banners ou de menus) ou uma alteração na lógica atual de como o cadastro da loja é feito, é preciso documentar esse novo cadastro na wiki do projeto no GitLab seguindo [esse padrão de documentação](https://gitlab.com/vnda/setup/ie-wiki-init).

No registro de entrega devem ficar os links para as páginas específicas da Wiki com as novas documentações relacionadas à essa solicitação.

***

### Exemplos para testes em staging

Para ajudar os colegas de atendimento a avaliarem um card com mais velocidade, é recomendado deixar links para páginas em que a solicitação ou a nova implementação foram desenvolvidas.

***

### Commits relacionados

Para a segurança da equipe, todas as modificações feitas em projetos que estão no GitLab podem ser identificadas por commits. É recomendado que o desenvolvedor liste o hash do commit — o identificador que o GitLab retorna quando um commit é enviado para o repositório — para que, em caso de problemas com o deploy, a equipe consiga identificar qualquer problema que possa ocorrer mais rapidamente.

***

## Commits

É recomendado indicar a versão do checklist e um link para o card da demanda relacionada, ao comitar um código no repositório dos projetos. É possível fazer isso adicionando duas quebras de linha à mensagem de commit (`\` + nova linha no Terminal).

Um commit ideal descreve a alteração principal do commit e então indica a demanda do Trello, seguindo o padrão:

```
descrição do que foi alterado

checklist 1.0 @ https://trello.com/c/exemplo
```
