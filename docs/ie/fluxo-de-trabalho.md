---
layout: page
title: Fluxo de Trabalho
parent: Interface Experience
nav_order: 2
has_children: true
---

# Fluxo de Trabalho
{: .no_toc}

O fluxo de trabalho é como chamamos o processo das demandas organizadas pelo atendimento do time de Interface Experience na pauta. O fluxo consiste, geralmente, em levantar requisitos de uma demanda, desenvolvê-los no ambiente de staging da loja, enviá-los para a revisão e, depois de aprovados, publicá-los no ambiente de produção.

Mas vamos por partes:

## Nesse artigo
{: .no_toc .text-delta }

1. TOC
  {:toc}


***

## A pauta

A pauta do time de IE é organizada pelo atendimento do time através do [quadro no Trello](https://trello.com/b/89FR4xCu). Esse quadro possui certas integrações com a pauta centralizada dos squads de front-end da Vnda no Airtable.

***

### Visão geral das etapas

Cada coluna na pauta do Trello representa um status no processo de realização das demandas do time de Interface Experience:

1. **Modelos**  
  Lista de controle interno, armazena [os modelos utilizados]({% link docs/ie/cards-modelo.md %}) pelo atendimento do time para criar os cards de demandas.
2. **Layout XD – Pauta UX Design**  
  Lista de demandas pendentes do time de design.
3. **Layout XD Pronto**  
  Demandas de design já concluídas, que ainda precisam ser pautadas para iniciar o desenvolvimento.
4. **Controle de revisões maiores**  
  Lista de controle interno que centraliza demandas divididas em vários cards na pauta.
5. **Aguardando — Back**  
  Demandas que têm alguma pendência do time de produto.
6. **Aguardando — Atendimento**  
  Demandas que têm alguma pendência do atendimento do time.
7. **To Do**  
  A lista principal da pauta de desenvolvimento. As demandas são organizadas nessa lista pela equipe de atendimento do IE, e devem ser realizadas pela equipe de desenvolvimento na ordem em que são postas na pauta considerando [a ordem de prioridades]({% link docs/ie/prioridades.md %}). Idealmente, as demandas chegam nessa etapa de acordo com os [cards modelo]({% link docs/ie/cards-modelo.md %}).
8. **Voltou (hello again)**  
  Demandas que foram realizadas, mas ainda não foram aprovadas. Elas seguem [a organização de prioridades]({% link docs/ie/prioridades.md %}), e devem ser tocadas na primeira oportunidade possível pelo desenvolvedor responsável.
9. **Doing**  
  Demandas que estão sendo desenvolvidas.
10. **Review/Staging**  
  Demandas que já foram desenvolvidas, e estão sendo avaliadas pelo atendimento.
11. **Aguardando Aprovação**  
  Demandas aprovadas internamente, mas aguardando alguma aprovação específica do cliente.
12. **Review - Hellice**  
  Demandas realizadas por desenvolvedores externos que aguardam a avaliação de um desenvolvedor interno.
13. **Checked**  
  Demandas aprovadas que já podem ser publicadas.
14. **Deployed**  
  Demandas publicadas.
15. **Arquivar**  
  Demandas que a publicação já foi testada e aprovada pelo atendimento de IE.
16. **Reuniões**  
  Lista de controle interno com o registro das reuniões do time.

***

## O processo

O fluxo de trabalho do time de IE é, geralmente, operado ao redor do Trello, em que demandas são vistas como _cards_, e as etapas de desenvolvimento são _colunas_. O desenvolvedor ingressa no card que ele vai realizar, e o move entre as colunas de acordo com qual etapa do processo aquela demanda em específico se encontra. Cards podem possuir _etiquetas_ que indicam algum status específico da demanda — por exemplo, se existe alguma solicitação em paralelo sendo executada, ou se a demanda [é uma prioridade]({% link docs/ie/prioridades.md %}).

A coluna **Pautar** é organizada pelo atendimento do time de CS, que organiza os cards em ordem de prioridade de cima para baixo. Cards mais acima devem ser desenvolvidos primeiro. No geral, o desenvolvedor se ocupa com o desenvolvimento de uma solicitação e, assim que concluída, move o card para a coluna **Review**. Só então o desenvolvedor deve voltar à coluna Pautar para ingressar em um novo card. Esse comportamento difere, porém, quando um card etiquetado como [Urgente]({% link docs/ie/prioridades.md %}) entra na pauta.

É importante que você sempre _ingresse_ no card que você vai desenvolver. Assim o Trello vai sempre notificar os envolvidos no card sobre possíveis atualizações — se o card for aprovado, por exemplo; ou se precisar de novas alterações.

### Etiquetas

Cards podem podem ter situações específicas que precisam de atenção do desenvolvedor e do atendimento. É responsabilidade de ambos manter as etiquetas de um card atualizadas de acordo com essas situações.

Uma etiqueta representa uma observação que não seja específica de uma etapa. Por exemplo, uma solicitação de prioridade não é prioridade apenas durante o desenvolvimento — mas desde o momento que ele entra na pauta até o momento que ele foi publicado. Nesse caso, a etiqueta de prioridade se mantém no card até sua publicação e validação.

Outras etiquetas podem ser mais circunstanciais — um card que precise de alguma observação da plataforma pode ser indicado com a etiqueta **Back-end**, por exemplo.

Cards também são “atalhos” visuais para situações que precisam ser observadas. Quando um card possui uma etiqueta de **Atenção! Outro card em andamento** na fila de Deploy, ele indica que esse card não pode ser publicado até o momento que essa etiqueta ser removida do card pelo atendimento. Da mesma forma, um desenvolvedor deve indicar que um card requer alterações de cadastro para ser publicado usando a etiqueta **Cadastros atualizados**.

### Checklists de solicitações

Um card possui as solicitações relacionadas em um Checklist 1.0. Esses são os itens que precisam ser desenvolvidos/investigados para que o card possa ser considerado concluído pelo desenvolvedor para ser movido para a coluna **Review**.

Quando um checklist é concluído, mas o atendimento adiciona mais solicitações são adicionadas, o card é movido pelo atendimento para a coluna **Voltou (Hello again)** , e os checklists já existentes no card não devem ser alterados. Sempre é adicionado um novo Checklist a partir da última versão (eg. 2.0, 2.1, dependendo do tipo de demanda adicional), seguindo a lógica:

- Versão menor (1.1, 2.1, 3.2, etc.): problemas ou comportamentos inesperados causados pela solicitação inicial.
- Versão maior (2.0, 3.0, 4.0, etc.): novas solicitações não relacionadas com a solicitação inicial, mas que precisam ser realizadas para que o card possa ser publicado.

***

### Prioridades

Nós chamamos de **prioridades** as demandas que requerem uma atenção especial do desenvolvedor. Existem dois tipos de prioridade que chegam à pauta:

* **Urgente**, demandas referentes à um problema sério em uma loja. Essas demandas precisam ser tocadas imediatamente.
* **Importante**, solicitações que precisam ser executadas na primeira oportunidade possível, assim que algum desenvolvedor estiver livre.

Apenas o responsável pela pauta de IE pode marcar uma demanda como prioridade, seguindo [essas condições]({% link docs/ie/prioridades.md %}).

***

### Checklists automáticos

Quando um card é movido para determinadas colunas, o Trello adiciona um checklist automático com tarefas que devem ser cumpridas. Essas tarefas geralmente se referem à organização e registro da demanda no card — como a pontuação, a data de início e de fim do desenvolvimento, o registro das orientações de cadastro.

Atualmente existem três checklists automáticos ativos no fluxo de trabalho:

* **Fluxo Atendimento/Dev:** tarefas relacionadas ao processo de desenvolvimento de um card.
* **Review Front/Hellice:** requisitos técnicos que devem ser cumpridos para que um card desenvolvido externamente possa ser aprovado.
* **Fluxo Deploy:** tarefas relacionadas ao processo de publicação da demanda no card; e requisitos do que deve ser testado assim que a demanda for publicada.

Você pode conferir cada um dos itens desses checklists automáticos [aqui]({% link docs/ie/checklists-automaticos.md %}).

***

### Entregas

Para que um card possa ser movido para a coluna **Review**, ele deve concluir o checklist automático [Fluxo Atendimento/Devs]({% link docs/ie/checklists-automaticos.md %}#fluxo-atendimentodevs), que consiste em tarefas relacionadas ao registro do que foi modificado na loja (arquivos, páginas, e cadastros).

A entrega de um card para avaliação também inclui o registro dessas modificações, que é realizada seguindo o [padrão de entregas que você pode conferir aqui]({% link docs/ie/registro-de-entregas.md %}). Esse registro é feito para que todo o time tenha uma visão geral das alterações que a solicitação demanda, e deve incluir:

- As novas orientações de cadastro, caso a solicitação envolva mudanças de cadastro na loja.  
  Essas orientações devem ser registradas na wiki do repositório da loja seguindo [esse padrão](https://gitlab.com/vnda/setup/ie-wiki-init).
- Links para testes da modificação no ambiente de staging da loja.
- Lista dos commits relacionados à modificação.

*** 

## Publicação

Assim que uma demanda for aprovada e movida para a coluna **Checked**, ela pode ser publicada no ambiente de produção da loja. O processo de publicação está documentado internamente [aqui](https://gitlab.com/vnda/setup/build/-/wikis/Deploy).

O deploy, como chamamos, envolve atualizar o ambiente de produção com as novas versões dos arquivos modificados para a realização de cada demanda. É uma operação que requer atenção especial, e por isso é acompanhado por um [checklist automático]({% link docs/ie/checklists-automaticos.md %}#fluxo-deploy) para que o desenvolvedor não esqueça de cada tarefa.

O time de IE faz a publicação dos cards aprovados na **primeira hora da manhã**, para que haja tempo para que possíveis bugs sejam identificados e resolvidos. É recomendado que os deploys aconteçam sempre entre às 9h e às 11h. Cards não-urgentes aprovados depois desse horário são publicados na manhã seguinte.

Assim que as etapas de atualização de arquivos tenha sido concluída e a modificação tenha sido propagada, o desenvolvedor deve testar a nova funcionalidade ou a correção, e então fazer um teste do processo de compra da loja, seguindo a ordem:

```markdown
Home ~> Tag ~> Busca ~ Produto ~> Carrinho
```

Ao acessar essas páginas e conseguir adicionar um produto no carrinho, o deploy foi concluído. O desenvolvedor deve mover o card para a coluna **Deployed**.

***

## Pauta no Airtable

A pauta de todos os desenvolvedores pode ser visualizada através do Airtable. O Airtable do time de IE é atualizado automaticamente a partir do Trello — não há necessidade de criar registros ou alterá-los no Airtable em si, uma vez que os dados são sincronizados automaticamente.

Caso uma tarefa não seja exibida nessa visualização, é importante avisar o atendimento do time [no canal do Slack](https://vnda.slack.com/archives/C010Y4MH0EM).

***

## Status no Slack

Os integrantes do time de IE usam certos status no Slack para indicar que tipo de tarefa eles estão executando para os outros integrantes:

| Emoji | Tarefa |
| ----- | ------ |
| 🚀 `:rocket:` | Fazendo Deploy |
| 🕵️‍♀️ `:detective:` | Revisando cards |
| 👩‍🚒 `:fireman:` | Demanda urgente |
| ⚠️ `:warning:` | Demanda importante |
| 📝 `:memo:` | Documentando |
