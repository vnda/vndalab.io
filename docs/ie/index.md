---
layout: page
title: Interface Experience
nav_order: 7
has_children: true
---

# Interface Experience

Depois que uma loja finaliza o processo de [setup e onboarding]({% link docs/setup/index.md %}), ela passa a ser responsabilidade da equipe de Interface Experience (IE) da Vnda. O time de desenvolvedores front-end do IE é responsável pela manutenção/resolução de bugs e pela implementação de novos recursos nessas lojas.

A pauta do time é definida através de demandas levantadas pelos clientes e organizadas, por ordem de prioridade, pelo atendimento de Interface Experience. É papel do desenvolvedor investigar a solicitação — que pode ser um bug ou um novo recurso —, desenvolver e documentar a modificação.

O Dev, como é chamado o time de desenvolvedores front-end do IE, segue os mesmos princípios de desenvolvimento do [time de Setup]({% link docs/setup/index.md %}), prezando por um código legível, organizado e eficiente; a documentação do desenvolvimento; e por um resultado de qualidade que respeite a identidade da loja. Esses princípios nos ajudam a oferecer uma manutenção mais certeira e ágil para as lojas, atingir bons índices de performance e tornar a vida útil do código mais longa.

Se você está começando agora no time, dê uma conferida na [visão geral sobre nossas responsabilidades]({% link docs/ie/visao-geral.md %}) e em [como funciona o nosso fluxo de trabalho]({% link docs/ie/fluxo-de-trabalho.md %}). Se você está com dúvida sobre algum detalhe, confira os artigos sobre [os checklists automáticos]({% link docs/ie/checklists-automaticos.md %}) que usamos, o [registro de entregas]({% link docs/ie/registro-de-entregas.md %}), e alguns [links úteis]({% link docs/ie/links-uteis.md %}).
