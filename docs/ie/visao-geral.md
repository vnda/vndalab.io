---
layout: page
title: Visão Geral
parent: Interface Experience
nav_order: 1
---

# Visão Geral
{: .no_toc }

Em linhas gerais, o time de IE cuida das lojas ativas da Vnda que não estão mais em processo de criação — que acontece nas etapas de [setup e onboarding]({% link docs/setup/index.md %}).

Isso significa em fazer a manutenção da loja, resolvendo bugs que podem interferir na experiência do site; no desenvolvimento de novos recursos específicos, como blogs, páginas personalizadas e campanhas; e na atualização de código de acordo com a evolução da Plataforma da empresa.

Essas demandas são reportadas ou requisitadas pelos clientes através dos canais de ajuda e comunicação da Vnda, e o atendimento do time de IE realiza a triagem e a organização delas na pauta de acordo com [a prioridade]({% link docs/ie/prioridades.md %}), o tempo de espera e a complexidade.

## Neste artigo:
{: .no_toc .text-delta }

1. TOC
  {:toc}

Cada desenvolvedor do time se vincula a uma dessas solicitações, desenvolvendo ou investigando a solução no ambiente de staging da loja. Assim que realizada, a solicitação é revisada pelo cliente e/ou pelo atendimento do time de CS. Se a solicitação for aprovada, o desenvolvedor pode publicá-la no ambiente de produção da loja. Se ela retornar com mais revisões, o desenvolvedor deve retomar o desenvolvimento para finalizá-la, enviando-a para revisão depois.

No canal do time no Slack, `#interface-experience`, as equipes de desenvolvimento e de atendimento se reúnem para tirar dúvidas, definir ou alertar de prioridades e conversar sobre a pauta. O time usa um quadro no Trello para organizar e visualizar o andamento das demandas de desenvolvimento; e o Airtable para o registro de pontuação e de datas dessas demandas.

Todo esse processo é acompanhado e reportado através da [pauta do time]({% link docs/ie/fluxo-de-trabalho.md %}#a-pauta), um quadro no Trello com etapas definidas. Essas etapas possuem algumas atividades em comum — como o registro da pontuação da demanda, por exemplo — que são controladas por [checklists automáticos]({% link docs/ie/checklists-automaticos.md %}). As demandas de desenvolvimento devem sempre ser acompanhadas pela [documentação do que foi desenvolvido]({% link docs/ie/registro-de-entregas.md %}).

***

## Responsabilidades do atendimento do time

Em relação às demandas de desenvolvimento, a equipe de atendimento do IE é responsável por:

- Organizar a pauta usando [os modelos]({% link docs/ie/cards-modelo.md %}).
- Organizar e alertar prioridades [de acordo com os parâmetros de prioridade]({% link docs/ie/prioridades.md %}).
- Perguntar e reportar dúvidas de implementação.
- Avaliar as demandas desenvolvidas.
- Verificar se existem outras demandas da mesma loja em andamento.

***

## Responsabilidades dos desenvolvedores do time

Como os desenvolvedores do time de Interface Experience são responsáveis por manter e aperfeiçoar o código das lojas da Vnda conforme o tempo, suas responsabilidades incluem:

- Desenvolver as demandas  
  (Sempre tendo em mente nossos princípios de desenvolvimento de código limpo, organizado e modularizado).
- Atualização de código em desuso pela Plataforma, como tags e objetos desatualizados.
  - Migrar os ambientes de _staging_ desatualizados para o novo padrão.
- Documentação e atualização do cadastro de novas implementações, de acordo [com os padrões de documentação](https://gitlab.com/vnda/setup/ie-wiki-init).
- Encontrar soluções de implementação junto com a equipe de atendimento.
- Publicar as demandas no ambiente de produção das lojas.
- Testar novas implementações que foram publicadas.

Para realizar essas tarefas, é preciso manter o [fluxo de trabalho]({% link docs/ie/fluxo-de-trabalho.md %}) do time organizado através das seguintes responsabilidades:

- Manter a pauta atualizada, de acordo com as tarefas realizadas e em andamento.
- Utilizar os [checklists automáticos]({% link docs/ie/checklists-automaticos.md %}) corretamente.
- Registrar as modificações de cada demanda, usando [o modelo de registro]({% link docs/ie/registro-de-entregas.md %}) do time.
- Identificar e evitar fazer publicações de demandas simultâneas.

Nós vamos entrar em detalhes sobre essas responsabilidades enquanto falamos [do fluxo de trabalho]({% link docs/ie/fluxo-de-trabalho.md %}).
