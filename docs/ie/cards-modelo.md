---
layout: page
title: Cards modelo
parent: Fluxo de Trabalho
grand_parent: Interface Experience
nav_order: 1
---

# Cards modelo

As demandas possuem um conjunto de categorias específicas, como **correção de bug** ou **alteração de layout**, que geralmente precisam de um mínimo de informações básicas para que o desenvolvedor comece a trabalhar nelas.

Essas informações são alinhadas e centralizadas no que chamamos de **cards modelo**, cards que podem ser duplicados pelo atendimento do time de IE para levar as solicitações dos clientes para a pauta. É recomendado que todos os cards cheguem na coluna **To Do** respeitando o card modelo específico da sua solicitação.

Em comum em todos os tipos de solicitações são as informações da loja (URL do ambiente em staging e URL do ambiente em produção), que precisam estar em todos os cards da pauta.

Já existem cards modelo para os seguintes tipos de solicitação:

* [Bug/Correção](https://trello.com/c/1T1cUwpC) 
  Quando há um problema na loja que precisa ser resolvido.
* [Alterações de layout](https://trello.com/c/yHQ0fzvT)  
  Quando algum elemento ou conteúdo da loja deve ser alterado.
* [Implementações](https://trello.com/c/SFfOqoLL)  
  Quando um novo recurso precisa ser desenvolvido para a loja.
* [Content](https://trello.com/c/s81sW0I2)  
  Quando uma loja deseja implementar o [Vnda Content]({% link docs/recursos/integracoes/cockpit.md %}).
* [B2B / Checkout](https://trello.com/c/Un7V50ce) e [B2B / Catálogo](https://trello.com/c/UOqhRrAe)  
  Quando uma loja deseja implementar os recursos de venda B2B.
* [Documentações](https://trello.com/c/lQRvdFHa)  
  Quando é preciso atualizar a documentação de uma loja para o [padrão](https://gitlab.com/vnda/setup/ie-wiki-init).
