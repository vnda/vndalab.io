---
layout: page
title: Links úteis
parent: Interface Experience
nav_order: 3
---

# Links úteis

Aqui vai uma relação de links que o time sempre acha bom ter em mãos:

* [O canal do time no Slack](https://vnda.slack.com/archives/C010Y4MH0EM)
* [O quadro da pauta no Trello](https://trello.com/b/89FR4xCu)
* [O registro da pauta no Airtable](https://airtable.com/tblPffXHKLljb9SqL/viwTtimWWbfkR3FsQ?blocks=hide).
* [O repositório da estrutura padrão](https://gitlab.com/vnda/setup/estrutura_padrao)  
  Inclui as melhores práticas de desenvolvimento e o novo padrão de registro de cadastros.
* [O repositório da `build`](https://gitlab.com/vnda/setup/build)  
  Nosso novo framework de desenvolvimento.
* [A documentação do script de deploy](https://gitlab.com/vnda/setup/build/-/wikis/deploy)  
  Nossa ferramenta de publicação de alterações.
* [O repositório do script de migração de lojas](https://gitlab.com/vnda/setup/migrate)  
  Para lojas que ainda não estão no GitLab.
* [O repositório do padrão de documentações](https://gitlab.com/vnda/setup/ie-wiki-init)  
  Para atualizar a documentação de uma loja para o padrão do time.
* [A nossa documentação do Liquid]({% link docs/liquid4/index.md %}).
