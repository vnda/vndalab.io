---
layout: page
title: Checklists automáticos
parent: Fluxo de Trabalho
grand_parent: Interface Experience
nav_order: 2
---

# Checklists automáticos
{: .no_toc }

Checklists automáticos são os checklists adicionados ao card quando esse atinge uma determinada [coluna da pauta de Dev]({% link docs/ie/fluxo-de-trabalho.md %}#visão-geral-das-etapas). Esses checklists possuem tarefas internas que precisam ser concluídas pelo desenvolvedor para que o card possa seguir para a próxima etapa.

As tarefas são demarcadas da seguinte forma:

```
[Ferramenta relacionada]: Descrição da tarefa
```

Por exemplo, tarefas que precisam ser realizadas no Trello ou no GitLab incluem o nome da ferramenta (e o campo específico ou página), além da descrição do que precisa ser feito.

Existem três checklists automáticos ativos na pauta de IE atualmente:

1. TOC
  {:toc}

***

### Fluxo Atendimento/Dev

Adicionado ao card quando ele é movido para a coluna **Doing**, esse checklist automático inclui tarefas operacionais, que o desenvolvedor precisa concluir antes de enviar o card para aprovação.

Esse checklist é adicionado novamente toda a vez que um card da coluna **Voltei (hello again)** é movido para **Doing**, porque as tarefas relacionadas à ele — como a atualização do caso ou o registro de entregas — precisam ser revisadas e corrigidas no card.

#### Tarefas do checklist:
{: .no_toc }

* **Trello » Card:** Preencher campo “Data Início” com a data de início.

  O campo personalizado “Data Início” deve ser preenchido com a data que o desenvolvedor começou a realizar as tarefas do checklist.
* **Wiki:** Registrar orientações de cadastro (criar wiki se não houver).

  Quando uma solicitação requer uma alteração de cadastro, essa alteração deve estar documentada [seguindo os padrões de formatação da Vnda](https://gitlab.com/vnda/setup/ie-wiki-init) na wiki do projeto no GitLab.
* **Trello » Comentário:** Registro de entregas.

  Após a conclusão de um checklist, o desenvolvedor deve fazer o [registro da entrega]({%- link docs/ie/registro-de-entregas.md -%}).
  
***

### Revisão Front/Hellice

Adicionado ao card quando ele é movido para a coluna **Review - Hellice**. Esses cards são desenvolvidos externamente, e o desenvolvedor responsável pelas revisões deve avaliar os requisitos desse checklist para poder aprová-lo para entrar na fila do deploy.
  
***

### Fluxo Deploy

Adicionado ao card quando ele é movido para a coluna **Checked**, esse checklist automático lista as tarefas que devem ser relacionadas ao [processo de publicação do card]({% link docs/ie/fluxo-de-trabalho.md %}#publicação) — incluindo os testes que devem ser feitos para considerar o deploy bem sucedido. 

#### Tarefas do checklist
{: .no_toc }

* **Backup:** Fazer backup do diretório `/pages/` do ambiente de produção.

  Algumas lojas possuem um diretório de páginas institucionais estáticas no ambiente de produção. Essa pasta pode ser substituída no deploy, então é de vital importância fazer um backup desse diretório no ambiente de staging antes de executar o script de deploy.
* **build/deploy.sh:** Enviar arquivos para o ambiente de produção (Dropbox/S3)

  A execução da publicação em si, utilizando o comando de deploy da build.
* **Tarefas de teste**

  As alterações e o processo de compra devem ser todos testados e aprovados pelo desenvolvedor após o deploy.
* **Trello » Due:** Preencher campo “Due” com a data de finalização

  O campo personalizado “Due” deve ser preenchido com a data em que o deploy foi realizado.
