---
published: false
---

## Visão Geral

- O que é o time de CS? Quais demandas atendemos?
- O canal no Slack
  - Assuntos de processo
  - Dúvidas técnicas
  - Memez/gifs
- Quais as responsabilidades da equipe de atendimento de CS?
  [https://docs.google.com/document/d/1_yHVQPoNxJdxvgrZQa677aqrJYflXDFN5LY_c4N5xIg/edit#heading=h.1q8j8pus5nl9]
  - Criar e colocar em pauta os records do Airtable
  - Uso de cards modelo
  - Ordenação da pauta
    - Gerenciar cards
  - Organização das prioridades
  - Review do card
    - Verificar outros cards em andamento
  - Esclarecimento de dúvidas de implementação
- Quais as responsabilidades da equipe de front-end de CS?
  - Migrar loja pro GitLab
  - Verificar se existe documentação sobre novas implementações
    - Qualquer coisa pergunta!
  - Desenvolvimento das demandas
    - Código limpo e organizado
    - Atualização de tags em desuso
    - Componentização
    - Documentação e atualização de novas implementações
  - Utilização correta dos checklists automáticos
  - Publicação de cards
    - Não fazer deploy dos itens simultâneos
    - Notificação do deploy
    - Teste do deploy
  - Esclarecimento de dúvidas de implementação
  - Atualização de código pras novas demandas da API
  - Atualização do Airtable

## Fluxo de Trabalho

(Trello Interno)

**A pauta:**

- Onde fica centralizada a pauta
- Visão geral das colunas


**O processo:**

- As notificações
- As etiquetas
- Os cards modelo
- Os checklists automáticos
- Como é feita a triagem de prioridades?
  - Quando começar a atender a demanda **Urgente** após recebimento?
  - Quando começar a atender a demanda **Importante** após recebimento?

**As entregas:**
- O que é o time de CS? Quais demandas atendemos?
- O canal no Slack
  - Assuntos de processo
  - Dúvidas técnicas
  - Memez/gifs
- Quais as responsabilidades da equipe de atendimento de CS?
  [https://docs.google.com/document/d/1_yHVQPoNxJdxvgrZQa677aqrJYflXDFN5LY_c4N5xIg/edit#heading=h.1q8j8pus5nl9]
  - Criar e colocar em pauta os records do Airtable
  - Uso de cards modelo
  - Ordenação da pauta
    - Gerenciar cards
  - Organização das prioridades
  - Review do card
    - Verificar outros cards em andamento
  - Esclarecimento de dúvidas de implementação
- Quais as responsabilidades da equipe de front-end de CS?
  - Migrar loja pro GitLab
  - Verificar se existe documentação sobre novas implementações
    - Qualquer coisa pergunta!
  - Desenvolvimento das demandas
    - Código limpo e organizado
    - Atualização de tags em desuso
    - Componentização
    - Documentação e atualização de novas implementações
  - Utilização correta dos checklists automáticos
  - Publicação de cards
    - Não fazer deploy dos itens simultâneos
    - Notificação do deploy
    - Teste do deploy
  - Esclarecimento de dúvidas de implementação
  - Atualização de código pras novas demandas da API
  - Atualização do Airtable

## Fluxo de Trabalho

(Trello Interno)

**A pauta:**

- Onde fica centralizada a pauta
- Visão geral das colunas


**O processo:**

- As notificações
- As etiquetas
- Os cards modelo
- Os checklists automáticos
- Como é feita a triagem de prioridades?
  - Quando começar a atender a demanda **Urgente** após recebimento?
  - Quando começar a atender a demanda **Importante** após recebimento?

**As entregas:**
- O que é o time de CS? Quais demandas atendemos?
- O canal no Slack
  - Assuntos de processo
  - Dúvidas técnicas
  - Memez/gifs
- Quais as responsabilidades da equipe de atendimento de CS?
  [https://docs.google.com/document/d/1_yHVQPoNxJdxvgrZQa677aqrJYflXDFN5LY_c4N5xIg/edit#heading=h.1q8j8pus5nl9]
  - Criar e colocar em pauta os records do Airtable
  - Uso de cards modelo
  - Ordenação da pauta
    - Gerenciar cards
  - Organização das prioridades
  - Review do card
    - Verificar outros cards em andamento
  - Esclarecimento de dúvidas de implementação
- Quais as responsabilidades da equipe de front-end de CS?
  - Migrar loja pro GitLab
  - Verificar se existe documentação sobre novas implementações
    - Qualquer coisa pergunta!
  - Desenvolvimento das demandas
    - Código limpo e organizado
    - Atualização de tags em desuso
    - Componentização
    - Documentação e atualização de novas implementações
  - Utilização correta dos checklists automáticos
  - Publicação de cards
    - Não fazer deploy dos itens simultâneos
    - Notificação do deploy
    - Teste do deploy
  - Esclarecimento de dúvidas de implementação
  - Atualização de código pras novas demandas da API
  - Atualização do Airtable

## Fluxo de Trabalho

(Trello Interno)

**A pauta:**

- Onde fica centralizada a pauta
- Visão geral das colunas


**O processo:**

- As notificações
- As etiquetas
- Os cards modelo
- Os checklists automáticos
- Como é feita a triagem de prioridades?
  - Quando começar a atender a demanda **Urgente** após recebimento?
  - Quando começar a atender a demanda **Importante** após recebimento?

**As entregas:**
- O que é o time de CS? Quais demandas atendemos?
- O canal no Slack
  - Assuntos de processo
  - Dúvidas técnicas
  - Memez/gifs
- Quais as responsabilidades da equipe de atendimento de CS?
  [https://docs.google.com/document/d/1_yHVQPoNxJdxvgrZQa677aqrJYflXDFN5LY_c4N5xIg/edit#heading=h.1q8j8pus5nl9]
  - Criar e colocar em pauta os records do Airtable
  - Uso de cards modelo
  - Ordenação da pauta
    - Gerenciar cards
  - Organização das prioridades
  - Review do card
    - Verificar outros cards em andamento
  - Esclarecimento de dúvidas de implementação
- Quais as responsabilidades da equipe de front-end de CS?
  - Migrar loja pro GitLab
  - Verificar se existe documentação sobre novas implementações
    - Qualquer coisa pergunta!
  - Desenvolvimento das demandas
    - Código limpo e organizado
    - Atualização de tags em desuso
    - Componentização
    - Documentação e atualização de novas implementações
  - Utilização correta dos checklists automáticos
  - Publicação de cards
    - Não fazer deploy dos itens simultâneos
    - Notificação do deploy
    - Teste do deploy
  - Esclarecimento de dúvidas de implementação
  - Atualização de código pras novas demandas da API
  - Atualização do Airtable

## Fluxo de Trabalho

(Trello Interno)

**A pauta:**

- Onde fica centralizada a pauta
- Visão geral das colunas


**O processo:**

- As notificações
- As etiquetas
- Os cards modelo
- Os checklists automáticos
- Como é feita a triagem de prioridades?
  - Quando começar a atender a demanda **Urgente** após recebimento?
  - Quando começar a atender a demanda **Importante** após recebimento?

**As entregas:**

- O registro de entregas
- Como usamos o Airtable:
-    [Disponibilizar o link da view]
  - Onde fica centralizada a pauta
-    [Atualização de campos]
  - Quais campos o front de CS preenche no Airtable

## Links úteis

- Documentação
- Trello
- Trello interno
- Pauta do Airtable

## Prioridades

## Tipos de Prioridades
---
### :sos: Prioridade alta
Deve ser iniciada assim que reportada
---
- Erro que impede compra
- Erro que envia informações incorretas ao carrinho
- Erro que impede seleção de variants
- Erro que mostra informações incorretas de estoque
- Erro que mostra valor errado no carrinho
- Quebra grave de layout (home, produto ou tag quebradas, que não permitindo concluir uma compra)
- Erros de deploy
---
### :atenção: Prioridade média
Deve ser iniciada na primeira oportunidade possível
---
- Demandas etail
- Quebra grave de layout (home, produto ou tag quebradas, mas que não impedem de concluir uma compra)
- Cards publicados que voltaram com modificações
- Avise-me quando chegar
---
### :papel_e_lápis: Observações
---
- Esses cards não devem ser interrompidos. Assim que um front inicia, ele está alocado neste card e outras demandas urgentes ou importantes precisam ir pra outros fronts, ou ficar na fila de espera.
- Cards para serem considerados prioridade precisam seguir algum dos modelos de padronização de card/checklists, afim de, agilizar o entendimento e efetividade das alterações solicitadas.
