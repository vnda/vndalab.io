---
layout: default
title: Introdução
nav_order: 1
description: "Boas vindas à Wiki do time de front da Vnda!"
permalink: /
---

# Boas vindas ao Vnda docs

A documentação da Vnda para ajudar você a criar novas lojas e a entender o funcionamento das já existentes, utilizando os recursos disponíveis da plataforma e das ferramentas de desenvolvimento que o nosso time de front-end utiliza.

## Cheguei agora, por onde posso começar?

Fique à vontade para explorar a documentação, mas indicamos que comece entendendo o que é um **Setup**. ↓

***

### O que é um Setup?

Nós chamamos de **Setup** o processo de desenvolvimento de cada nova loja na Vnda Ecommerce. O **Setup** de uma loja é composto por várias etapas, que são descritas detalhadamente na seção de [Setup]({% link docs/setup/index.md %}). O **Setup** deve proporcionar uma loja com código **moderno** e **organizado**, que oferece **boa performance** e traduza a **identidade** da loja para o usuário.

***

Agora que você já sabe o que é um **Setup**, confira:

[O que é o Liquid?]({% link docs/liquid4/index.md %}){: .btn .btn-orange }

[Organizando seu ambiente]({% link docs/ambiente/index.md %}){: .btn .btn-orange }
