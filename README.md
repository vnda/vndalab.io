# Vnda | docs

Esse repositório guarda o conteúdo da [Documentação da Vnda](https://docs.vnda.com.br/), um site estático hospedado no GitLab Pages usando [Jekyll](https://jekyllrb.com/).

## Instalação

1. **Windows:** Instale um ambiente de desenvolvimento Ruby.

  Recomendamos instalar o Ruby através do Chocolatey. Veja como instalar ele [nesse tutorial](https://chocolatey.org/install).

  Depois, execute no Prompt de Comando:

  ```
  choco install ruby
  ```

  > *Problemas com a versão do Ruby?*
  >
  > Caso ocorra problemas com a versão Ruby, uma versão testada e indicada seria `ruby 2.5.1p57 (2018-03-29 revision 63029) [x64-mingw32]`.

  Se você já tem um ambiente de desenvolvimento do Ruby, recomendamos que você dê uma olhada [nesse tutorial](https://jekyllrb.com/docs/installation/windows/) para ver se ele está configurado corretamente para o Jekyll.

2. Instale o Bundler:

  ```
  gem install bundler
  ```

3. Vá para o repositório do projeto:

  ```
  cd vnda-docs
  ```

4. Instale as dependências do projeto:

  ```
  bundle install
  ```

  O `Gemfile` vai instalar todas as dependências do projeto no seu sistema. Algumas dependências são exclusivas para o Windows, mas o Bundler deve identificar o que precisa ser instalado na sua máquina dependendo do sistema.

  > *Erro: Unable to load the EventMachine C extension; To use the pure-ruby reactor, require 'em/pure_ruby'*
  >
  > Caso ocorra o erro acima, uma solução existente é desinstalar e instalar novamente o EventMachine através dos comandos `gem uninstall eventmachine` seguido por `gem install eventmachine --platform ruby`.

5. Sirva o site na sua máquina:

  ```
  bin/jekyll serve --livereload
  ```

  O Jekyll irá re-gerar o site automaticamente toda a vez que você atualizar um arquivo Markdown.

6. Acesse sua cópia local do site em <http://localhost:4000>
